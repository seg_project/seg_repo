package View;

import Model.PredefinedSettings;
import Model.RecalculatedRunway;
import static Model.RecalculatedRunway.DIST_TYPE.BLAST;
import static Model.RecalculatedRunway.DIST_TYPE.RESA;
import static Model.RecalculatedRunway.DIST_TYPE.SLOPE;
import Model.Runway;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public abstract class DisplayRunway extends JPanel {

    //These constants represent the proportion of the screen they take up
    //They are NOT proportional to their metered length
    //ie if their length changes in metres, there is no graphical change to the drawings
    protected final Double CLEARWAY_WIDTH = 0.15;
    protected final Double CLEARWAY_HEIGHT = 0.28;

    protected final Double RUNWAY_WIDTH = 0.7;
    protected final Double RUNWAY_HEIGHT = 0.14;

    protected final Double STOPWAY_WIDTH = 0.08;
    protected final Double STOPWAY_HEIGHT = RUNWAY_HEIGHT;

    protected final Double TORA_HEIGHT = 14D / 50D;
    protected final Double ASDA_HEIGHT = 8.5D / 50D;
    protected final Double TODA_HEIGHT = 3D / 50D;
    protected final Double LDA_HEIGHT = 19.5D / 50D;
    protected final Double FIRST_ARROW_HEIGHT = 0.65;
    protected final Double SECOND_ARROW_HEIGHT = 0.75;
    protected final Double THIRD_ARROW_HEIGHT = 0.85;
    public static boolean FACING_LEFT = false;
    public static boolean FACING_RIGHT = true;
    protected final Double arrowheight = 0.015;
    protected final Double ARROW_PADDING = 10.0;

    protected Color BACK_GROUND;
    protected Color CGA;
    protected Color RUNWAY;
    protected Color CLEARWAY;
    protected Color STOPWAY;
    protected Color ARROW;
    protected Color DEFAULT;

    protected Runway runwayToDisplay;
    protected RecalculatedRunway recalRunway;
    protected Boolean facingRight;
    protected int estimatedRunwayLength;
    protected int runwayStart;
    protected int runwayEnd;
    protected int fStopwayEnd;
    protected int sStopwayEnd;
    protected int fClearwayEnd;
    protected int sClearwayEnd;
    protected int fDispThresh;
    protected int sDispThresh;
    protected int obstacleLoc;
    
    private final double ZOOM_IN = 1.1;
    private final double ZOOM_OUT = 0.9;
    private int originalWidth;
    private int originalHeight;
    private int currentWidth;
    private int currentHeight;
    private boolean zoomed = false;
    private JScrollPane pane;
    private int maxZoom = 2500;
    
    protected BufferedImage image;

    public DisplayRunway(Runway r, PredefinedSettings settings) {
        this.RUNWAY = settings.getRunwayColor();
        this.BACK_GROUND = settings.getBack_ground();
        this.CGA = settings.getCGA();
        this.CLEARWAY = settings.getClearway();
        this.STOPWAY = settings.getStopway();
        this.ARROW = settings.getArrow();
        this.DEFAULT = settings.getOther();
        this.runwayToDisplay = r;
        this.facingRight = false;
        this.setFocusable(true);
        if (this.runwayToDisplay.getfTORA() > this.runwayToDisplay.getsTORA()) {
            this.estimatedRunwayLength = this.runwayToDisplay.getfTORA();
        } else {
            this.estimatedRunwayLength = this.runwayToDisplay.getsTORA();
        }
    }
    
    public BufferedImage getImage() {
        return this.image;
    }
    
    public void setPane(JScrollPane p) {
        this.pane = p;
    }
    
    public void zoomIn() {
        zoomed = true;
        if((this.currentHeight > this.maxZoom) || (this.currentWidth > this.maxZoom)) {
            
        } else {
            this.currentHeight = (int) (this.currentHeight * this.ZOOM_IN);
            this.currentWidth = (int) (this.currentWidth * this.ZOOM_IN);
        }
        this.repaint();
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(currentWidth, currentHeight);
    }
    
    public void zoomOut() {
        this.currentHeight = (int) (this.currentHeight * this.ZOOM_OUT);
        this.currentWidth = (int) (this.currentWidth * this.ZOOM_OUT);
        if((this.currentHeight < this.originalHeight) || (this.currentWidth < this.originalWidth)) {
            this.currentHeight = this.originalHeight;
            this.currentWidth = this.originalWidth;
            zoomed = false;
        }
        this.repaint();
    }

    public void changeDirection(boolean facingRight) {
        this.facingRight = facingRight;
    }

    public void updateRecalculatedRunway(RecalculatedRunway recal) {
        this.recalRunway = recal;
    }

    public Boolean isDirectionRight() {
        return this.facingRight;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        if(!zoomed) {
            this.originalHeight = pane.getViewport().getSize().height;
            this.originalWidth = pane.getViewport().getSize().width;
            this.currentWidth = this.originalWidth;
            this.currentHeight = this.originalHeight;
        }
        int width = this.currentWidth;
        int height = this.currentHeight;
        Dimension d = this.getDimension(width, height);
        image = new BufferedImage(d.width, d.height, BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D imageGraphics = image.createGraphics();
        imageGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        this.drawAll(imageGraphics, d.width, d.height);
        g2.setColor(this.BACK_GROUND);
        g2.fillRect(0, 0, width, height);
        g2.drawImage(image, this.rotate(d.width, d.height), null);
        pane.revalidate();
    }

    private void drawAll(Graphics2D g, int width, int height) {
        //Sets the background color for the whole panel, others shapes drawn on top will overwrite some, leaving just the graded area 
        g.setColor(BACK_GROUND);
        g.fillRect(0, 0, width, height);
        g.setColor(DEFAULT);
        g.drawRect(0, 0, width-1, height-1);
        g.setFont(this.getFont(g.getFont(), height));
        drawStatic(g, width, height);
        drawDirectionArrow(g, width, height);
        if (recalRunway != null) {
            if (recalRunway.getObstacleType().equals((RecalculatedRunway.OBS_TYPE.NO_OBSTACLE)) || recalRunway.getObstacleType().equals(RecalculatedRunway.OBS_TYPE.NO_EFFECT)) {
                drawDefaultParameters(g, width, height);
            } else {
                drawObstacle(g, width, height);
                
                drawDistances(g, width, height);
            }
        } else {
            drawDefaultParameters(g, width, height);
        }
    }
    
    private Font getFont(Font orig, int height) {
        int fontHeight = (height * 6 / 200) + 5;
        int size = fontHeight;
        Boolean up = null;
        while(true) {
            Font font = new Font(orig.getName(), orig.getStyle(), size);
            int testHeight = getFontMetrics(font).getHeight();
            if (testHeight < fontHeight && up != Boolean.FALSE) {
                size++;
                up = Boolean.TRUE;
            } else if (testHeight > fontHeight && up != Boolean.TRUE) {
                size--;
                up = Boolean.FALSE;
            } else {
                return font;
            }
        }
    }

    private void drawDistances(Graphics2D g, int width, int height) {
        if (recalRunway.getObstacleType().equals(RecalculatedRunway.OBS_TYPE.OBSTACLE_AT_FIRST)) {
            if (!facingRight) {
                int RESAEnd = obstacleLoc + (int) ((240.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                drawArrow(obstacleLoc, RESAEnd, (int) (SECOND_ARROW_HEIGHT * height), 240, "RESA", g);
                int stripEnd = RESAEnd + (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                drawArrow(RESAEnd, stripEnd, (int) (SECOND_ARROW_HEIGHT * height), "        60m", "        SE", g);
                if (recalRunway.getsLDA() > 0) {
                    drawArrow(sDispThresh, stripEnd, (int) (LDA_HEIGHT * height), recalRunway.getsLDA(), "LDA", g);
                }
                int paramEnd = stripEnd;
                if (recalRunway.getTakeoffOver().equals(RecalculatedRunway.DIST_TYPE.SLOPE)) {
                    int slopeEnd = obstacleLoc + (int) ((recalRunway.getTakeoffOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                    drawArrow(obstacleLoc, slopeEnd, (int) (THIRD_ARROW_HEIGHT * height), recalRunway.getTakeoffOverLength(), "Slope", g);
                    int slopeSE = slopeEnd + (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                    drawArrow(slopeEnd, slopeSE, (int) (THIRD_ARROW_HEIGHT * height), "        60m", "        SE", g);
                    paramEnd = slopeSE;
                }
                if (recalRunway.getsTORA() > 0) {
                    drawArrow(runwayEnd, paramEnd, (int) (TORA_HEIGHT * height), recalRunway.getsTORA(), "TORA", g);
                }
                if (recalRunway.getsTODA() > 0) {
                    drawArrow(runwayEnd, paramEnd, (int) (TODA_HEIGHT * height), recalRunway.getsTODA(), "TODA", g);
                }
                if (recalRunway.getsASDA() > 0) {
                    drawArrow(runwayEnd, paramEnd, (int) (ASDA_HEIGHT * height), recalRunway.getsASDA(), "ASDA", g);
                }
            } else {
                int blastEnd = obstacleLoc + (int) ((recalRunway.getTakeOffAwayLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                String label;
                int start;
                if (recalRunway.getTakeOffAway().equals(RecalculatedRunway.DIST_TYPE.BLAST)) {
                    label = "Blast";
                    start = blastEnd;
                } else {
                    label = "RESA";
                    start = blastEnd + (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                    drawArrow(blastEnd, start, (int) (SECOND_ARROW_HEIGHT * height), "        60m", "        SE", g);
                }
                drawArrow(obstacleLoc, blastEnd, (int) (SECOND_ARROW_HEIGHT * height), recalRunway.getTakeOffAwayLength(), label, g);
                if (recalRunway.getfTORA() > 0) {
                    drawArrow(start, runwayEnd, (int) (TORA_HEIGHT * height), recalRunway.getfTORA(), "TORA", g);
                }
                if (recalRunway.getfTODA() > 0) {
                    drawArrow(start, fClearwayEnd, (int) (TODA_HEIGHT * height), recalRunway.getfTODA(), "TODA", g);
                }
                if (recalRunway.getfASDA() > 0) {
                    drawArrow(start, fStopwayEnd, (int) (ASDA_HEIGHT * height), recalRunway.getfASDA(), "ASDA", g);
                }
                int startofLDA;
                switch (recalRunway.getLandOver()) {
                    case BLAST:
                        startofLDA = obstacleLoc + (int) ((recalRunway.getLandOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                        if (!label.equals("Blast")) {
                            drawArrow(obstacleLoc, startofLDA, (int) (THIRD_ARROW_HEIGHT * height), recalRunway.getLandOverLength(), "Blast", g);
                        }
                        break;
                    case SLOPE:
                        int slopeEnd = obstacleLoc + (int) ((recalRunway.getLandOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                        drawArrow(obstacleLoc, slopeEnd, (int) (THIRD_ARROW_HEIGHT * height), recalRunway.getLandOverLength(), "Slope", g);
                        int slopeSE = slopeEnd + (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                        startofLDA = slopeSE;
                        drawArrow(slopeEnd, slopeSE, (int) (THIRD_ARROW_HEIGHT * height), "        60m", "        SE", g);
                        break;
                    case RESA:
                        int RESAEnd = obstacleLoc + (int) ((240.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                        startofLDA = RESAEnd + (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                        if (!label.equals("RESA")) {
                            drawArrow(obstacleLoc, RESAEnd, (int) (THIRD_ARROW_HEIGHT * height), 240, "RESA", g);
                            drawArrow(RESAEnd, startofLDA, (int) (THIRD_ARROW_HEIGHT * height), "        60m", "        SE", g);
                        }
                        break;
                    default:
                        startofLDA = 0;
                }
                if (recalRunway.getfLDA() > 0) {
                    drawArrow(startofLDA, runwayEnd, (int) (LDA_HEIGHT * height), recalRunway.getfLDA(), "LDA", g);
                }
            }
        } else {
            if (facingRight) {
                int RESAEnd = obstacleLoc - (int) ((240.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                drawArrow(obstacleLoc, RESAEnd, (int) (SECOND_ARROW_HEIGHT * height), 240, "RESA", g);
                int stripEnd = RESAEnd - (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                drawArrow(RESAEnd, stripEnd, (int) (SECOND_ARROW_HEIGHT * height), "60m        ", "SE        ", g);
                if (recalRunway.getfLDA() > 0) {
                    drawArrow(fDispThresh, stripEnd, (int) (LDA_HEIGHT * height), recalRunway.getfLDA(), "LDA", g);
                }
                int paramEnd = stripEnd;
                if (recalRunway.getTakeoffOver().equals(RecalculatedRunway.DIST_TYPE.SLOPE)) {
                    int slopeEnd = obstacleLoc - (int) ((recalRunway.getTakeoffOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                    drawArrow(obstacleLoc, slopeEnd, (int) (THIRD_ARROW_HEIGHT * height), recalRunway.getTakeoffOverLength(), "Slope", g);
                    int slopeSE = slopeEnd - (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                    drawArrow(slopeEnd, slopeSE, (int) (THIRD_ARROW_HEIGHT * height), "60m        ", "SE        ", g);
                    paramEnd = slopeSE;
                }
                if (recalRunway.getfTORA() > 0) {
                    drawArrow(runwayStart, paramEnd, (int) (TORA_HEIGHT * height), recalRunway.getfTORA(), "TORA", g);
                }
                if (recalRunway.getfTODA() > 0) {
                    drawArrow(runwayStart, paramEnd, (int) (TODA_HEIGHT * height), recalRunway.getfTODA(), "TODA", g);
                }
                if (recalRunway.getfASDA() > 0) {
                    drawArrow(runwayStart, paramEnd, (int) (ASDA_HEIGHT * height), recalRunway.getfASDA(), "ASDA", g);
                }
            } else {
                int blastEnd = obstacleLoc - (int) ((recalRunway.getTakeOffAwayLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                String label;
                int start;
                if (recalRunway.getTakeOffAway().equals(RecalculatedRunway.DIST_TYPE.BLAST)) {
                    label = "Blast";
                    start = blastEnd;
                } else {
                    label = "RESA";
                    start = blastEnd - (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                    drawArrow(blastEnd, start, (int) (SECOND_ARROW_HEIGHT * height), "60m        ", "SE        ", g);
                }
                drawArrow(obstacleLoc, blastEnd, (int) (SECOND_ARROW_HEIGHT * height), recalRunway.getTakeOffAwayLength(), label, g);
                if (recalRunway.getsTORA() > 0) {
                    drawArrow(start, runwayStart, (int) (TORA_HEIGHT * height), recalRunway.getsTORA(), "TORA", g);
                }
                if (recalRunway.getsTODA() > 0) {
                    drawArrow(start, sClearwayEnd, (int) (TODA_HEIGHT * height), recalRunway.getsTODA(), "TODA", g);
                }
                if (recalRunway.getsASDA() > 0) {
                    drawArrow(start, sStopwayEnd, (int) (ASDA_HEIGHT * height), recalRunway.getsASDA(), "ASDA", g);
                }
                int startofLDA;
                switch (recalRunway.getLandOver()) {
                    case BLAST:
                        startofLDA = obstacleLoc - (int) ((recalRunway.getLandOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                        if (!label.equals("Blast")) {
                            drawArrow(obstacleLoc, startofLDA, (int) (THIRD_ARROW_HEIGHT * height), recalRunway.getLandOverLength(), "Blast", g);
                        }
                        break;
                    case SLOPE:
                        int slopeEnd = obstacleLoc - (int) ((recalRunway.getLandOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                        drawArrow(obstacleLoc, slopeEnd, (int) (THIRD_ARROW_HEIGHT * height), recalRunway.getLandOverLength(), "Slope", g);
                        int slopeSE = slopeEnd - (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                        startofLDA = slopeSE;
                        drawArrow(slopeEnd, slopeSE, (int) (THIRD_ARROW_HEIGHT * height), "60m        ", "SE        ", g);
                        break;
                    case RESA:
                        int RESAEnd = obstacleLoc - (int) ((240.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                        startofLDA = RESAEnd - (int) ((60.0 / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
                        if (!label.equals("RESA")) {
                            drawArrow(obstacleLoc, RESAEnd, (int) (THIRD_ARROW_HEIGHT * height), 240, "RESA", g);
                            drawArrow(RESAEnd, startofLDA, (int) (THIRD_ARROW_HEIGHT * height), "60m        ", "SE        ", g);
                        }
                        break;
                    default:
                        startofLDA = 0;
                }
                if (recalRunway.getsLDA() > 0) {
                    drawArrow(startofLDA, runwayStart, (int) (LDA_HEIGHT * height), recalRunway.getsLDA(), "LDA", g);
                }
            }
        }
    }

    private void drawDirectionArrow(Graphics2D g2, int width, int height) {
        Font normal = g2.getFont();
        Font large = normal.deriveFont((float) (normal.getSize() * 1.5));
        g2.setFont(large);
        //Draws the threshold indicators and directional arrow
        String first = runwayToDisplay.getFirstName();
        if (first.length() == 3) {
            g2.drawString(first.substring(0, 2), (int) width / 5, height * 47 / 50);
            g2.drawString(first.substring(2), ((int) width / 5) + 5, height * 49 / 50);
        } else {
            g2.drawString(runwayToDisplay.getFirstName(), (int) width / 5, height * 49 / 50);
        }
        String second = runwayToDisplay.getSecondName();
        if (second.length() == 3) {
            g2.drawString(second.substring(0, 2), (int) (width * 3.7 / 5), height * 47 / 50);
            g2.drawString(second.substring(2), ((int) (width * 3.7 / 5) + 5), height * 49 / 50);
        } else {
            g2.drawString(runwayToDisplay.getSecondName(), (int) (width * 3.7 / 5), height * 49 / 50);
        }
        g2.setFont(normal);

        //Set the co-ordinates for the points of an arrow
        int[] xpointsArrow;
        int[] ypointsArrow;
        if (facingRight) {
            int[] xpointsArrowCheat = {width * 3 / 10, width * 13 / 20, width * 13 / 20, width * 7 / 10, width * 13 / 20, width * 13 / 20, width * 3 / 10};
            xpointsArrow = xpointsArrowCheat;
            int[] ypointsArrowCheat = {height * 190 / 200, height * 190 / 200, height * 187 / 200, height * 193 / 200, height * 199 / 200, height * 196 / 200, height * 196 / 200};
            ypointsArrow = ypointsArrowCheat;
        } else {
            int[] xpointsArrowCheat = {width * 3 / 10, width * 7 / 20, width * 7 / 20, width * 7 / 10, width * 7 / 10, width * 7 / 20, width * 7 / 20};
            xpointsArrow = xpointsArrowCheat;
            int[] ypointsArrowCheat = {height * 193 / 200, height * 187 / 200, height * 190 / 200, height * 190 / 200, height * 196 / 200, height * 196 / 200, height * 199 / 200};
            ypointsArrow = ypointsArrowCheat;
        }
        GeneralPath arrowline = new GeneralPath(GeneralPath.WIND_NON_ZERO, xpointsArrow.length);
        arrowline.moveTo(xpointsArrow[0], ypointsArrow[0]);
        for (int index = 1; index < xpointsArrow.length; index++) {
            arrowline.lineTo(xpointsArrow[index], ypointsArrow[index]);
        }
        arrowline.lineTo(xpointsArrow[0], ypointsArrow[0]);
        arrowline.closePath();

        //Draw the direction arrow
        //g2.draw(arrowline);
        g2.setColor(ARROW);
        g2.fill(arrowline);
        g2.setColor(DEFAULT);
        //and  draw the text inside on the arrow
        FontMetrics fm = g2.getFontMetrics();
        int arrowStart = width * 3/10;
        int arrowEnd = width * 7/10;
        int arrowMiddle = (arrowStart + arrowEnd) / 2;
        int arrowYMiddle = height * 193 / 200;
        g2.drawString("Land/Take-off direction", arrowMiddle - (fm.stringWidth("Land/Take-off direction") / 2), arrowYMiddle + (fm.getAscent() / 2) - 2);
    }

    private void drawDefaultParameters(Graphics2D g2, int width, int height) {
        if (facingRight) {
            //TORA
            drawArrow(runwayStart, runwayEnd, (int) (TORA_HEIGHT * height), runwayToDisplay.getfTORA(), "TORA", g2);
            //ASDA
            drawArrow(runwayStart, fStopwayEnd, (int) (ASDA_HEIGHT * height), runwayToDisplay.getfASDA(), "ASDA", g2);
            //TODA
            drawArrow(runwayStart, fClearwayEnd, (int) (TODA_HEIGHT * height), runwayToDisplay.getfTODA(), "TODA", g2);
            //LDA
            drawArrow(fDispThresh, runwayEnd, (int) (LDA_HEIGHT * height), runwayToDisplay.getfLDA(), "LDA", g2);
        } else {
            //TORA
            drawArrow(runwayEnd, runwayStart, (int) (TORA_HEIGHT * height), runwayToDisplay.getsTORA(), "TORA", g2);
            //ASDA
            drawArrow(runwayEnd, sStopwayEnd, (int) (ASDA_HEIGHT * height), runwayToDisplay.getsASDA(), "ASDA", g2);
            //TODA
            drawArrow(runwayEnd, sClearwayEnd, (int) (TODA_HEIGHT * height), runwayToDisplay.getsTODA(), "TODA", g2);
            //LDA
            drawArrow(sDispThresh, runwayStart, (int) (LDA_HEIGHT * height), runwayToDisplay.getsLDA(), "LDA", g2);
        }
    }

    protected void drawArrow(int startX, int endX, int Y, int distance, String label, Graphics2D g) {
        final Integer arrowsize = 10; //The length of the arrow head, in pixels (fixed size)

        if (startX > endX) {
            int temp = startX;
            startX = endX;
            endX = temp;
        }

        GeneralPath arrowline = new GeneralPath(GeneralPath.WIND_NON_ZERO, 9);
        arrowline.moveTo(startX, Y);
        arrowline.lineTo(startX + arrowsize, (int) (Y - (getHeight() / 2 * arrowheight)));
        arrowline.lineTo(startX, Y);
        arrowline.lineTo(startX + arrowsize, (int) (Y + (getHeight() / 2 * arrowheight)));
        arrowline.lineTo(startX, Y);

        arrowline.lineTo(endX, Y);
        arrowline.lineTo(endX - arrowsize, (int) (Y - (getHeight() / 2 * arrowheight)));
        arrowline.lineTo(endX, Y);
        arrowline.lineTo(endX - arrowsize, (int) (Y + (getHeight() / 2 * arrowheight)));

        FontMetrics fm = g.getFontMetrics();
        g.draw(arrowline);
        String dist = new Integer(distance).toString() + "m";
        g.drawString(dist, startX + (((endX - startX) / 2) - (fm.stringWidth(dist) / 2)), (int) (Y + fm.getAscent() - 2));
        g.drawString(label, startX + (((endX - startX) / 2) - (fm.stringWidth(label) / 2)), Y);
    }
    
    protected void drawArrow(int start, int end, int Y, String distance, String label, Graphics2D g) {
        final Integer arrowsize = 10; //The length of the arrow head, in pixels (fixed size)

        int startX;
        int endX;
        if (start > end) {
            startX = end;
            endX = start;
        } else {
            startX = start;
            endX = end;
        }

        GeneralPath arrowline = new GeneralPath(GeneralPath.WIND_NON_ZERO, 9);
        arrowline.moveTo(startX, Y);
        arrowline.lineTo(startX + arrowsize, (int) (Y - (getHeight() / 2 * arrowheight)));
        arrowline.lineTo(startX, Y);
        arrowline.lineTo(startX + arrowsize, (int) (Y + (getHeight() / 2 * arrowheight)));
        arrowline.lineTo(startX, Y);

        arrowline.lineTo(endX, Y);
        arrowline.lineTo(endX - arrowsize, (int) (Y - (getHeight() / 2 * arrowheight)));
        arrowline.lineTo(endX, Y);
        arrowline.lineTo(endX - arrowsize, (int) (Y + (getHeight() / 2 * arrowheight)));
        FontMetrics fm = g.getFontMetrics();
        g.draw(arrowline);
        g.drawString(distance, (startX + ((endX - startX) / 2)) - (fm.stringWidth(distance) / 2), (int) (Y + fm.getAscent() - 2));
        g.drawString(label, (startX + ((endX - startX) / 2)) - (fm.stringWidth(label) / 2),Y);
    }

    protected abstract void drawStatic(Graphics2D g, int width, int height);

    protected abstract void drawObstacle(Graphics2D g, int width, int height);
    
    protected abstract Dimension getDimension(int width, int height);
    
    protected abstract AffineTransform rotate(int width, int height);
}
