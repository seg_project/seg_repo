package View;

import Model.Obstacle;
import Model.PredefinedSettings;
import Model.RecalculatedRunway.OBS_TYPE;
import Model.Runway;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;


public class SideOnPanel extends DisplayRunway {
    private final Double SIDE_RUNWAY_HEIGHT = 0.05;

    public SideOnPanel(Runway r, PredefinedSettings settings) {
        super(r, settings);
    }
    
    @Override
    protected void drawObstacle(Graphics2D g, int width, int height) {
        Obstacle obs = runwayToDisplay.getObstacle();
        int obstacleSize = 30;
        int runwayTop = (int) ((height / 2) - ((SIDE_RUNWAY_HEIGHT / 2) * height));
        int y = runwayTop - obstacleSize;
        if(recalRunway.getObstacleType().equals(OBS_TYPE.OBSTACLE_AT_FIRST)) {
            obstacleLoc = fDispThresh + (int) ((new Double(obs.getDistanceFirst()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
            drawArrow(fDispThresh, obstacleLoc, (int) (FIRST_ARROW_HEIGHT * height), Math.abs(obs.getDistanceFirst()), "", g);
            int slopeEnd;
            if(facingRight) {
                slopeEnd = obstacleLoc + (int)((recalRunway.getLandOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                g.drawString("ALS", obstacleLoc, y - 20);
            }
            else {
                slopeEnd = obstacleLoc + (int)((recalRunway.getTakeoffOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                g.drawString("TOCS", obstacleLoc, y - 20);
            }
            int starty = runwayTop - ((obstacleSize*(slopeEnd - runwayStart)) / (slopeEnd - obstacleLoc));
            g.drawLine(runwayStart, starty, slopeEnd, runwayTop);
        }
        else {
            obstacleLoc = sDispThresh - (int) ((new Double(obs.getDistanceSecond()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
            drawArrow(sDispThresh, obstacleLoc, (int) (FIRST_ARROW_HEIGHT * height), Math.abs(obs.getDistanceSecond()), "", g);
            int slopeEnd;
            if(facingRight) {
                slopeEnd = obstacleLoc - (int)((recalRunway.getTakeoffOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                g.drawString("TOCS", obstacleLoc, y - 20);
            }
            else {
                slopeEnd = obstacleLoc - (int)((recalRunway.getLandOverLength() / new Double(estimatedRunwayLength) * RUNWAY_WIDTH * width));
                g.drawString("ALS", obstacleLoc, y - 20);
            }
            int starty = runwayTop - ((obstacleSize*(runwayEnd - slopeEnd)) / (obstacleLoc - slopeEnd));
            g.drawLine(runwayEnd, starty, slopeEnd, runwayTop);
        }
        g.fillOval(obstacleLoc  - (obstacleSize/2), y, obstacleSize, obstacleSize);
    }
    
    @Override
    public void drawStatic(Graphics2D g, int totalLength, int totalHeight){
        //Draw the runway
        Double runwayAbove = ((1 - SIDE_RUNWAY_HEIGHT) / 2) * totalHeight;
        runwayStart = (int) (totalLength * CLEARWAY_WIDTH);
        runwayEnd = runwayStart + (int) (totalLength * RUNWAY_WIDTH);
        Rectangle2D runway = new Rectangle2D.Double(runwayStart, (int) Math.floor(runwayAbove), (int) (totalLength * RUNWAY_WIDTH), (int) (totalHeight * SIDE_RUNWAY_HEIGHT));
        //g.draw(runway);
        g.setColor(RUNWAY);
        g.fill(runway);
        g.setColor(DEFAULT);
        
        //Calculates displacement from top and the draws the clearway on both sides
        double arrowHeight = 0.95 * totalHeight;
        Double clearwayAbove = ((1 - SIDE_RUNWAY_HEIGHT) / 2) * totalHeight;
        fClearwayEnd = runwayEnd;
        sClearwayEnd = runwayStart;
        if(runwayToDisplay.getfClearwayLength() != 0) {
            Rectangle2D clearwayRight = new Rectangle2D.Double(totalLength - (int) (totalLength * CLEARWAY_WIDTH), (int) Math.floor(clearwayAbove), (int) (totalLength * CLEARWAY_WIDTH), (int) (totalHeight * SIDE_RUNWAY_HEIGHT));
            //g.draw(clearwayRight);
            g.setColor(CLEARWAY);
            g.fill(clearwayRight);
            g.setColor(DEFAULT);
            drawArrow(totalLength - (int) (totalLength * CLEARWAY_WIDTH), totalLength, (int) arrowHeight, runwayToDisplay.getfClearwayLength(), "Clearway", g);
            fClearwayEnd = totalLength;
        }
        if(runwayToDisplay.getsClearwayLength() != 0) {
            Rectangle2D clearwayLeft = new Rectangle2D.Double(0, (int) Math.floor(clearwayAbove), (int) (totalLength * CLEARWAY_WIDTH), (int) (totalHeight * SIDE_RUNWAY_HEIGHT));
            //g.draw(clearwayLeft);
            g.setColor(CLEARWAY);
            g.fill(clearwayLeft);
            g.setColor(DEFAULT);
            drawArrow(0, (int) (totalLength * CLEARWAY_WIDTH), (int) arrowHeight, runwayToDisplay.getsClearwayLength(), "Clearway", g);
            sClearwayEnd = 0;
            
        }
        
        //Draw the stopway
        Double stopwayAbove = ((1 - SIDE_RUNWAY_HEIGHT) / 2) * totalHeight;
        fStopwayEnd = runwayEnd;
        sStopwayEnd = runwayStart;
        if(runwayToDisplay.getfStopwayLength() != 0) {
            Rectangle2D stopwayRight = new Rectangle2D.Double(totalLength - (int) (totalLength * CLEARWAY_WIDTH), (int) Math.floor(stopwayAbove), (int) (totalLength * STOPWAY_WIDTH), (int) (totalHeight * SIDE_RUNWAY_HEIGHT));
            //g.draw(stopwayRight);
            g.setColor(STOPWAY);
            g.fill(stopwayRight);
            g.setColor(DEFAULT);
            drawArrow((int) (totalLength * (1 - CLEARWAY_WIDTH)), (int) ((totalLength * (1 - CLEARWAY_WIDTH)) + (totalLength * STOPWAY_WIDTH)), (int) (ARROW_PADDING + stopwayAbove + (totalHeight * SIDE_RUNWAY_HEIGHT) + (totalHeight / 2 * arrowheight)), runwayToDisplay.getfStopwayLength(), "Stopway", g);
            fStopwayEnd = (int) ((totalLength * (1 - CLEARWAY_WIDTH)) + (totalLength * STOPWAY_WIDTH));
        }
        if(runwayToDisplay.getsStopwayLength() != 0) {
            Rectangle2D stopwayLeft = new Rectangle2D.Double((int) (totalLength * (CLEARWAY_WIDTH - STOPWAY_WIDTH)), (int) Math.floor(stopwayAbove), (int) (totalLength * STOPWAY_WIDTH), (int) (totalHeight * SIDE_RUNWAY_HEIGHT));
            //g.draw(stopwayLeft);
            g.setColor(STOPWAY);
            g.fill(stopwayLeft);
            g.setColor(DEFAULT);
            drawArrow((int) (totalLength * (CLEARWAY_WIDTH - STOPWAY_WIDTH)), (int) (totalLength * CLEARWAY_WIDTH), (int) (ARROW_PADDING + stopwayAbove + (totalHeight * SIDE_RUNWAY_HEIGHT) + (totalHeight / 2 * arrowheight)), runwayToDisplay.getsStopwayLength(), "Stopway", g);
            sStopwayEnd = (int) (totalLength * (CLEARWAY_WIDTH - STOPWAY_WIDTH));
        }
        
        //Draw the displaced thresholds
        fDispThresh = runwayStart;
        Integer thresholdOne = (int) ((CLEARWAY_WIDTH + ((new Double(runwayToDisplay.getfDisplacedThreshold()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH)) * totalLength);
        if (runwayToDisplay.getfDisplacedThreshold() > 0) {
            fDispThresh = thresholdOne;
            g.drawLine(thresholdOne, (int) (((1 - SIDE_RUNWAY_HEIGHT) / 2) * totalHeight), thresholdOne, (int) (((SIDE_RUNWAY_HEIGHT + 1) / 2) * totalHeight));
            drawArrow((int) (CLEARWAY_WIDTH * totalLength), thresholdOne, (int) ((((1 - SIDE_RUNWAY_HEIGHT) / 2) * totalHeight) + (totalHeight * SIDE_RUNWAY_HEIGHT) + (totalHeight / 2 * arrowheight) + ARROW_PADDING), runwayToDisplay.getfDisplacedThreshold(), "Thresh", g);
        }
        //Threshold two (right side)
        sDispThresh = runwayEnd;
        Integer thresholdTwo = (int) (totalLength - ((CLEARWAY_WIDTH + ((new Double(runwayToDisplay.getsDisplacedThreshold()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH)) * totalLength));

        if (runwayToDisplay.getsDisplacedThreshold() > 0) {
            sDispThresh = thresholdTwo;
            g.drawLine(thresholdTwo, (int) (((1 - SIDE_RUNWAY_HEIGHT) / 2) * totalHeight), thresholdTwo, (int) (((SIDE_RUNWAY_HEIGHT + 1) / 2) * totalHeight));
            drawArrow(thresholdTwo, (int) (totalLength * (1 - CLEARWAY_WIDTH)), (int) ((((1 - SIDE_RUNWAY_HEIGHT) / 2) * totalHeight) + (totalHeight * SIDE_RUNWAY_HEIGHT) + (totalHeight / 2 * arrowheight) + ARROW_PADDING), runwayToDisplay.getsDisplacedThreshold(), "Thresh", g);
        }
    }
    
    @Override
    public Dimension getDimension(int width, int height) {
        return new Dimension(width, height);
    }
    
    @Override
    public AffineTransform rotate(int width, int height) {
        //do nothing. Side view does not rotate
        return new AffineTransform();
    }
}
