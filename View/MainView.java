package View;

//the main view class is responsible for adding and removing subviews from
import java.util.List;

//the main frame
public class MainView {

    //this method initialise the main frame
    private SubView currentSubView; //the subview which is currently the ContentPane of the main frame
    private MainFrame mainFrame; // the main jframe. the subviews will be the content pane of that frame

    public static void main(String[] args) {
        MainView m = new MainView();
        m.init();
    }

    private MainView() {
        init();
    }
    private static MainView instance;

    public static MainView getInstance() {
        if (instance == null) {
            instance = new MainView();
        }
        return instance;
    }

    public void packMain() {
        this.mainFrame.pack();
    }

    public final void init() {

        mainFrame = new MainFrame();
        mainFrame.setLocationRelativeTo(null); //put the frame in the centre of the screen

//        initialSubView = new StartScreenSubView(); //create the sub view
//        changeSubView(initialSubView); //change the frame with assigning the subview as the content of the frame
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
                mainFrame.setVisible(true);
//            }
//        });
//        changeSubView(new ViewRunwaySubView());
        //the thread.sleep is curently just a way to verify that we
        //can programatically change the subviews in the mainframe
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException ex) {
//        }
//        SubView airportRunway = new AddRunwayPanel();
//        changeSubView(airportRunway);
    }

    //this method accepts a subview and replace with it the current view.
    //it also saves the current view as the lastSubView
    public void changeSubView(SubView sv) {
//        if (currentSubView == null) {
//            currentSubView = sv;
//        }
//        currentSubView.setVisible(false);
//        sv.setPreviousView(currentSubView);
//        currentSubView = sv;
//        mainFrame.getContentPane().removeAll();
//        mainFrame.getContentPane().add(sv);

        if (sv.isResizable()) {
            mainFrame.setResizable(true);
        } else {
            mainFrame.setResizable(false);
        }
        mainFrame.setContentPane(sv);
        mainFrame.setSize(sv.getPreferredSize());
        mainFrame.setPreferredSize(sv.getPreferredSize());
        mainFrame.setMinimumSize(sv.getMinimumSize());
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setSize(sv.getPreferredSize());
        mainFrame.pack();
        sv.setVisible(true);
    }

    public void passDataToSubView(List data) {

    }

    public SubView getCurrentSubView() {
        return this.currentSubView;
    }

    public boolean isLastViewSet() {
        return currentSubView.getPreviousView() != null;
    }

    public void switchOff() {
        this.mainFrame.setVisible(false);
    }

    public void switchOn() {
        this.mainFrame.setVisible(true);
    }

    public MainFrame getMainFrame() {
        return this.mainFrame;
    }

}
