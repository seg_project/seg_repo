package View;

 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
 

public class ToastMessage extends JDialog {
 
    String msg;
    JFrame frame;
    ComponentAdapter ca;
    int type = JOptionPane.ERROR_MESSAGE;
    //You can set the time for how long the dialog will be displayed
    public static final int LENGTH_LONG = 5000;
    public static final int LENGTH_SHORT = 2000;
 
    public ToastMessage(String msg, boolean priority) {
        super(MainFrame.getFrame(), false);
        if(priority) makeHighPrio(msg);
        else makeLowPrio(msg);
    }
    
    public ToastMessage(String msg, boolean priority, int type) {
        super(MainFrame.getFrame(), false);
        this.type = type;
        if(priority) makeHighPrio(msg);
        else makeLowPrio(msg);
    }
	
    private void makeHighPrio(String msg){
        //JOptionPane.showMessageDialog(null, msg, "alert", JOptionPane.ERROR_MESSAGE);
        
        String[] options = {"OK"};
        JOptionPane pane = new JOptionPane(msg, type, JOptionPane.OK_OPTION, null, options);
//        pane.setBackground(Color.white); // Configure
        JDialog dialog = pane.createDialog(null, "alert");
        dialog.show();
    }
    
    private void makeLowPrio(String msg){
        this.msg = "<html> <body style='width: 90%; margin-left: 5px;'> <b> " + msg + "</b>";
        this.frame = MainFrame.getFrame();
        
		ca = new ComponentAdapter(){
                        @Override
			public void componentMoved(ComponentEvent e){
				setLocationPoint();
			}
                        @Override
			public void componentResized(ComponentEvent e){
				setLocationPoint();
			}
		};
		
		final ToastMessage dialog = this;
                
                final Timer fadeTimer = new Timer(100, new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        float current = getOpacity();
                        current -= 0.05;
                        
                        if(current<=0){
                            ((Timer)e.getSource()).stop();
                            dialog.setVisible(false);
                            frame.removeComponentListener(ca);
                            dialog.dispose();
                        }else{
                            setOpacity(current);
                        }
                    }
                });
                
		Timer timer = new Timer(ToastMessage.LENGTH_LONG, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        fadeTimer.start();
                    }
                });
		timer.setRepeats(false);
                fadeTimer.setRepeats(true);
                
		initComponents();
		timer.start();
		dialog.setVisible(true);

		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.toFront();
				frame.repaint();
			}
		});
    }
    
    public void setLocationPoint(){
        setSize(frame.getWidth()/7, frame.getHeight()/6);
        Point location = frame.getLocation();
        Point p = new Point(frame.getWidth() - getWidth() - 20 + (int)location.getX(), frame.getHeight() - getHeight() - 20 + (int)location.getY());
        setLocation(p);// adding toast to frame or use null
    }
 
    private void initComponents() {
        setLayout(new BorderLayout());
        addComponentListener(new ComponentAdapter() {
            // Give the window an rounded rect shape.
            @Override
            public void componentResized(ComponentEvent e) {
                setShape(new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), 30, 30));
            }
        });
		
        frame.addComponentListener(ca);
        setMinimumSize(new Dimension(100, 100));
        setLocationPoint();
        setUndecorated(true);
		
        getContentPane().setBackground(Color.YELLOW);
		
        // Determine what the GraphicsDevice can support.
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        final boolean isTranslucencySupported = gd.isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.TRANSLUCENT);
 
        // Set the window to 50% translucency, if supported.
        if (isTranslucencySupported) setOpacity(1f);
 
        JLabel label = new JLabel();
        label.setForeground(Color.BLACK);
        label.setText(msg);
        label.setFont(getFont(label.getFont(), getHeight()));
        add(label);
    }
    
    private Font getFont(Font orig, int height) {
        int fontHeight = (height * 6 / 40) + 5;
        int size = fontHeight;
        Boolean up = null;
        while(true) {
            Font font = new Font(orig.getName(), orig.getStyle(), size);
            int testHeight = getFontMetrics(font).getHeight();
            if (testHeight < fontHeight && up != Boolean.FALSE) {
                size++;
                up = Boolean.TRUE;
            } else if (testHeight > fontHeight && up != Boolean.TRUE) {
                size--;
                up = Boolean.FALSE;
            } else {
                return font;
            }
        }
    }
}