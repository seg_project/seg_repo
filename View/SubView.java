package View;

import java.util.Collection;
import javax.swing.JPanel;

//classes implementing this interface are the so-called "sub-views"
//the MainView will be adding/removing instances of those classes from the main
//frame
public class SubView extends JPanel {

    private SubView prevSubview;
    //each Collection added to it is a single frame of data - all the date a subview might needs for rendering.

    private String type; //the type of the SubView might be "Main/start screen", "Runway view"
    private boolean resizable = false;
    private MainFrame mainFrame;

    public void setMainFrame(MainFrame m) {
        this.mainFrame = m;
    }

    public MainFrame getMainFrame() {
        return this.mainFrame;
    }

    public SubView() {
        prevSubview = null;
        setMainFrame(MainView.getInstance().getMainFrame());
    }

    public SubView(boolean resize) {
        setMainFrame(MainView.getInstance().getMainFrame());

        this.resizable = resize;
    }

    public boolean isResizable() {
        return this.resizable;
    }

    public void setPreviousView(SubView sv) {
        prevSubview = sv;
    }

    public SubView getPreviousView() {
        return prevSubview;
    }

    public void addData(Collection data) {
        data.addAll(data);
    }

    
}
