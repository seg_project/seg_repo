package View;

import Model.Obstacle;
import Model.PredefinedSettings;
import Model.RecalculatedRunway.OBS_TYPE;
import Model.Runway;
import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;



public class TopDownPanel extends DisplayRunway {
    private final Double CLEARAREA_HEIGHT_ONE = 0.2;
    private final Double CLEARAREA_HEIGHT_TWO = 0.08;
    private Double rotation;
    private Double usedRotation;
    
    public TopDownPanel(Runway toDisplay, PredefinedSettings settings) {
        super(toDisplay, settings);
        rotation = this.getRotation();
        usedRotation = 0.0;
    }
    
    private double getRotation() {
        String first = this.runwayToDisplay.getFirstName().substring(0, 2);
        String second = this.runwayToDisplay.getSecondName().substring(0, 2);
        int firstInt = Integer.parseInt(first);
        int secondInt = Integer.parseInt(second);
        int used;
        if(firstInt < secondInt) {
            used = firstInt;
        } else {
            used = secondInt;
        }
        used = used * 10;
        used = used - 90;
        return Math.toRadians(used);
    }
    
    public void setRotation(boolean b) {
        if(b) {
            usedRotation = rotation;
        }
        else {
            usedRotation = 0.0;
        }
    }
    
    @Override
    protected void drawObstacle(Graphics2D g, int width, int height) {
        Obstacle obs = runwayToDisplay.getObstacle();
        int obstacleSize = height / 15;
        int y = (int) ((height / 2) + ((new Double(obs.getDistanceY()) / 75.0 * -1) * (0.5 - CLEARAREA_HEIGHT_ONE) * height) - (obstacleSize / 2));
        if(recalRunway.getObstacleType().equals(OBS_TYPE.OBSTACLE_AT_FIRST)) {
            obstacleLoc = fDispThresh + (int) ((new Double(obs.getDistanceFirst()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
            drawArrow(fDispThresh, obstacleLoc, (int) (FIRST_ARROW_HEIGHT * height), Math.abs(obs.getDistanceFirst()), "", g);
        } else {
            obstacleLoc = sDispThresh - (int) ((new Double(obs.getDistanceSecond()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH * width);
            drawArrow(sDispThresh, obstacleLoc, (int) (FIRST_ARROW_HEIGHT * height), Math.abs(obs.getDistanceSecond()), "", g);
        }
        g.fillOval(obstacleLoc  - (obstacleSize/2), y, obstacleSize, obstacleSize);
        
    }

    @Override
    protected void drawStatic(Graphics2D g, int totalLength, int totalHeight) {
        //Calculates the top half points of the cleared area
        Double clearwayLength = totalLength * CLEARWAY_WIDTH;
        Double heightOne = totalHeight * CLEARAREA_HEIGHT_ONE;
        Double heightTwo = totalHeight * CLEARAREA_HEIGHT_TWO;
        Double[] xpoints = {0D, clearwayLength, clearwayLength + (totalLength / 10), totalLength - clearwayLength - (totalLength / 10), totalLength - clearwayLength, (double) totalLength};
        Double[] ypoints = {heightOne, heightOne, heightTwo, heightTwo, heightOne, heightOne};

        //Calculates the bottom half points of the cleared area
        Double heightThree = totalHeight - (totalHeight * CLEARAREA_HEIGHT_ONE);
        Double heightFour = totalHeight - (totalHeight * CLEARAREA_HEIGHT_TWO);
        Double[] x2points = {(double) totalLength, totalLength - clearwayLength, totalLength - clearwayLength - (totalLength / 10), clearwayLength + (totalLength / 10), clearwayLength, 0D};
        Double[] y2points = {heightThree, heightThree, heightFour, heightFour, heightThree, heightThree};

        //Draws a custom shape using the cleared area points
        GeneralPath polyline = new GeneralPath(GeneralPath.WIND_NON_ZERO, xpoints.length);
        polyline.moveTo(xpoints[0], ypoints[0]);
        for (int index = 1; index < xpoints.length; index++) {
            polyline.lineTo(xpoints[index], ypoints[index]);
        }
        //polyline.moveTo(x2points[0], y2points[0]);
        for (int index = 0; index < x2points.length; index++) {
            polyline.lineTo(x2points[index], y2points[index]);
        }
        polyline.moveTo(xpoints[0], ypoints[0]);
        polyline.closePath();

        //Colours the cleared area
        g.setColor(CGA);
        g.fill(polyline);

        //Set to black to differtiate objects on the cleared area
        g.setColor(DEFAULT);
        
        //Calculates the runway drawings. It is based on where the left/right clearway stops/starts
        double runwayAbove = ((1 - RUNWAY_HEIGHT) / 2) * totalHeight;
        runwayStart = (int) (totalLength * CLEARWAY_WIDTH);
        runwayEnd = runwayStart + (int) (totalLength * RUNWAY_WIDTH);
        Rectangle2D runway = new Rectangle2D.Double(runwayStart, (int) runwayAbove, (int) (totalLength * RUNWAY_WIDTH), (int) (totalHeight * RUNWAY_HEIGHT));
        
        //g.draw(runway);
        g.setColor(RUNWAY);
        g.fill(runway);
        g.setColor(DEFAULT);
        //Set a dashing line for the centre line
        final float dash1[] = {10.0f};
        final BasicStroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
        g.setStroke(dashed);
        g.drawLine((int) (totalLength * CLEARWAY_WIDTH), (int) Math.floor(runwayAbove + (totalHeight * RUNWAY_HEIGHT / 2)), (int) (totalLength - totalLength * CLEARWAY_WIDTH), (int) (runwayAbove + (totalHeight * RUNWAY_HEIGHT / 2)));
        //Set back to a solid line for the other drawings
        g.setStroke(new BasicStroke(0));

        //Calculates displacement from top and the draws the clearway on both sides
        Double clearwayAbove = ((1 - CLEARWAY_HEIGHT) / 2) * totalHeight;
        double arrowHeight = 0.95 * totalHeight;
        fClearwayEnd = runwayEnd;
        sClearwayEnd = runwayStart;
        if(runwayToDisplay.getfClearwayLength() != 0) {
            Rectangle2D clearwayRight = new Rectangle2D.Double(totalLength - (int) (totalLength * CLEARWAY_WIDTH), (int) Math.floor(clearwayAbove), (int) (totalLength * CLEARWAY_WIDTH), (int) (totalHeight * CLEARWAY_HEIGHT));
            //g.draw(clearwayRight);
            g.setColor(CLEARWAY);
            g.fill(clearwayRight);
            g.setColor(DEFAULT);
            drawArrow(totalLength - (int) (totalLength * CLEARWAY_WIDTH), totalLength, (int) arrowHeight, runwayToDisplay.getfClearwayLength(), "Clearway", g);
            fClearwayEnd = totalLength;
        }
        if(runwayToDisplay.getsClearwayLength() != 0) {
            Rectangle2D clearwayLeft = new Rectangle2D.Double(0, clearwayAbove.intValue(), (int) (totalLength * CLEARWAY_WIDTH), (int) (totalHeight * CLEARWAY_HEIGHT));
            //g.draw(clearwayLeft);
            g.setColor(CLEARWAY);
            g.fill(clearwayLeft);
            g.setColor(DEFAULT);
            drawArrow(0, (int) (totalLength * CLEARWAY_WIDTH), (int) arrowHeight, runwayToDisplay.getsClearwayLength(), "Clearway", g);
            sClearwayEnd = 0;
        }
        
        //Calculate distances and draw the stopway
        Double stopwayAbove = ((1 - STOPWAY_HEIGHT) / 2) * totalHeight;
        fStopwayEnd = runwayEnd;
        sStopwayEnd = runwayStart;
        if(runwayToDisplay.getfStopwayLength() != 0) {
            Rectangle2D stopwayRight = new Rectangle2D.Double(totalLength * (1 - CLEARWAY_WIDTH), stopwayAbove.intValue(), (int) (totalLength * STOPWAY_WIDTH), (int) (totalHeight * STOPWAY_HEIGHT));
            //g.draw(stopwayRight);
            g.setColor(STOPWAY);
            g.fill(stopwayRight);
            g.setColor(DEFAULT);
            drawArrow((int) (totalLength * (1 - CLEARWAY_WIDTH)), (int) ((totalLength * (1 - CLEARWAY_WIDTH)) + (totalLength * STOPWAY_WIDTH)), (int) (ARROW_PADDING + stopwayAbove + (totalHeight * STOPWAY_HEIGHT) + (totalHeight / 2 * arrowheight)), runwayToDisplay.getfStopwayLength(), "Stopway", g);
            fStopwayEnd = (int) ((totalLength * (1 - CLEARWAY_WIDTH)) + (totalLength * STOPWAY_WIDTH));
        }
        if(runwayToDisplay.getsStopwayLength() != 0) {
            Rectangle2D stopwayLeft = new Rectangle2D.Double((int) (totalLength * (CLEARWAY_WIDTH - STOPWAY_WIDTH)), stopwayAbove.intValue(), (int) (totalLength * STOPWAY_WIDTH), (int) (totalHeight * STOPWAY_HEIGHT));
            //g.draw(stopwayLeft);
            g.setColor(STOPWAY);
            g.fill(stopwayLeft);
            g.setColor(DEFAULT);
            drawArrow((int) (totalLength * (CLEARWAY_WIDTH - STOPWAY_WIDTH)), (int) (totalLength * CLEARWAY_WIDTH), (int) (ARROW_PADDING + stopwayAbove + (totalHeight * STOPWAY_HEIGHT) + (totalHeight / 2 * arrowheight)), runwayToDisplay.getsStopwayLength(), "Stopway", g);
            sStopwayEnd = (int) (totalLength * (CLEARWAY_WIDTH - STOPWAY_WIDTH));
        }
        
        //Draw the displaced thresholds
        fDispThresh = runwayStart;
        Integer thresholdOne = (int) ((CLEARWAY_WIDTH + ((new Double(runwayToDisplay.getfDisplacedThreshold()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH)) * totalLength);
        if (runwayToDisplay.getfDisplacedThreshold() > 0) {
            fDispThresh = thresholdOne;
            g.drawLine(thresholdOne, (int) (((1 - RUNWAY_HEIGHT) / 2) * totalHeight), thresholdOne, (int) (((RUNWAY_HEIGHT + 1) / 2) * totalHeight));
            drawArrow((int) (CLEARWAY_WIDTH * totalLength), thresholdOne, (int) ((((1 - STOPWAY_HEIGHT) / 2) * totalHeight) + (totalHeight * STOPWAY_HEIGHT) + (totalHeight / 2 * arrowheight) + ARROW_PADDING), runwayToDisplay.getfDisplacedThreshold(), "Thresh", g);
        }
        //Threshold two (right side)
        sDispThresh = runwayEnd;
        Integer thresholdTwo = (int) (totalLength - ((CLEARWAY_WIDTH + ((new Double(runwayToDisplay.getsDisplacedThreshold()) / new Double(estimatedRunwayLength)) * RUNWAY_WIDTH)) * totalLength));

        if (runwayToDisplay.getsDisplacedThreshold() > 0) {
            sDispThresh = thresholdTwo;
            g.drawLine(thresholdTwo, (int) (((1 - RUNWAY_HEIGHT) / 2) * totalHeight), thresholdTwo, (int) (((RUNWAY_HEIGHT + 1) / 2) * totalHeight));
            drawArrow(thresholdTwo, (int) (totalLength * (1 - CLEARWAY_WIDTH)), (int) ((((1 - STOPWAY_HEIGHT) / 2) * totalHeight) + (totalHeight * STOPWAY_HEIGHT) + (totalHeight / 2 * arrowheight) + ARROW_PADDING), runwayToDisplay.getsDisplacedThreshold(), "Thresh", g);
        }
    }
    
    @Override
    public Dimension getDimension(int width, int height) {
        double prop = new Double(width) / new Double(height);
        if(this.usedRotation == 0.0) {
            return new Dimension(width, height);
        }
        else {
            double x = Math.atan(1.0/prop);
            double positiveAngle = Math.abs(this.usedRotation);
            double diagonal = new Double(height) / Math.sin(x + positiveAngle);
            double newHeight = Math.sqrt((diagonal*diagonal)/(1 + (prop*prop)));
            double newWidth = newHeight * prop;
            return new Dimension((int)newWidth, (int)newHeight);
        }
    }
    
    @Override
    public AffineTransform rotate(int width, int height) {
        AffineTransform at = new AffineTransform();
        at.translate(getWidth()/2, getHeight()/2);
        at.rotate(this.usedRotation);
        at.translate(-width/2, -height/2);
        return at;
    }
}
