/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package View;

import Model.Airport;
import Model.PredefinedSettings;
import Model.RecalculatedRunway;
import Model.Runway;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ViewRunwaySubView extends SubView {

    private boolean isTopView;
    private boolean runwayRotated;
    private TopDownPanel top_panel;
    private SideOnPanel side_panel;
    private JScrollPane topPane;
    private JScrollPane sidePane;
    RecalculatedRunway recR;
    Runway runway;

    public ViewRunwaySubView(Runway r, PredefinedSettings settings) {
        super(true); //resizable
//        getMainFrame().setResizable(false);
        runway = r;
        runwayRotated = false;
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        runway_menu = new javax.swing.JMenu();
        match_compass_btn = new javax.swing.JCheckBoxMenuItem();
        jMenu3 = new javax.swing.JMenu();
        add_obstacle_btn = new javax.swing.JMenuItem();
        import_obstacle_btn = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        remove_obstacle_btn = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        show_info_btn = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        help_btn = new javax.swing.JMenuItem();
        save_runway_as_btn = new javax.swing.JMenuItem();
        print_calculations_btn = new javax.swing.JMenuItem();

        initComponents();

        top_panel = new TopDownPanel(runway, settings); // those two classes should accept a Runway in the constructor and use it for drawing
        side_panel = new SideOnPanel(runway, settings);
        topPane = new JScrollPane(top_panel);
        sidePane = new JScrollPane(side_panel);
        top_panel.setPane(topPane);
        side_panel.setPane(sidePane);
        topPane.setFocusable(true);
        sidePane.setFocusable(true);
        topPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        topPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        sidePane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        sidePane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.getRunwayName().setText(runway.getFirstName() + '/' + r.getSecondName());

        view_panel.add(topPane, "card2");
        view_panel.add(sidePane, "card3");

        /*set the direction to both as the same once*/
        top_panel.changeDirection(TopDownPanel.FACING_RIGHT);
        side_panel.changeDirection(SideOnPanel.FACING_RIGHT);
        this.first_name_threshold.setSelected(true);
        this.asda_original.setText(String.valueOf(runway.getfASDA()));
        this.toda_original.setText(String.valueOf(runway.getfTODA()));
        this.tora_original.setText(String.valueOf(runway.getfTORA()));
        this.lda_original.setText(String.valueOf(runway.getfLDA()));

        this.getFirstNameRadioButton().setText(runway.getFirstName());
        this.getSecondNameRadioButton().setText(runway.getSecondName());

        if (!runway.isObstacleOnRunway()) { //there is not obstacle on the runway
            this.obstacle_params_panel.setEnabled(false);
            this.getRemoveObstacleBtn().setEnabled(false);
            this.getAddObstacleBtn().setEnabled(true);
        } else {
            //set the parameters for the panel with the obstacle
            this.getRemoveObstacleBtn().setEnabled(true);
            this.getAddObstacleBtn().setEnabled(false);

            Integer blast = getBlastDistance();
            if (blast == null) {
                blast = 300;
                JOptionPane.showMessageDialog(null, "The blast distance was set to the default 300");
            }

            recR = runway.recalculateParameters(blast);
            top_panel.updateRecalculatedRunway(recR);
            side_panel.updateRecalculatedRunway(recR);
            this.asda_obstacle.setText(String.valueOf(recR.getfASDA()));
            this.toda_obstacle.setText(String.valueOf(recR.getfTODA()));
            this.tora_obstacle.setText(String.valueOf(recR.getfTORA()));
            this.lda_obstacle.setText(String.valueOf(recR.getfLDA()));

            getRemoveObstacleBtn().setEnabled(true);
        }
        CardLayout cl = (CardLayout) this.view_panel.getLayout();

        cl.show(view_panel, "card2");
        isTopView = true;

        first_name_threshold.setSelected(true);

        this.repaintPanes();
    }
    javax.swing.JMenuItem add_obstacle_btn;
    javax.swing.JMenuItem help_btn;
    javax.swing.JMenuItem import_obstacle_btn;
    javax.swing.JMenu jMenu1;
    javax.swing.JMenu jMenu3;
    javax.swing.JMenu jMenu4;
    javax.swing.JMenuBar jMenuBar1;
    javax.swing.JMenuItem jMenuItem3;
    javax.swing.JPopupMenu.Separator jSeparator1;
    javax.swing.JPopupMenu.Separator jSeparator2;
    javax.swing.JCheckBoxMenuItem match_compass_btn;
    javax.swing.JMenuItem remove_obstacle_btn;
    javax.swing.JMenu runway_menu;
    javax.swing.JMenuItem show_info_btn;

    javax.swing.JMenuItem save_runway_as_btn;
    javax.swing.JMenuItem print_calculations_btn;

    private Airport airport;

    public void showMenu(MainFrame frame) {
//        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        //<editor-fold defaultstate="collapsed" desc="initialise variables">

//</editor-fold>
        jMenu1.setText("File");
        print_calculations_btn.setText("Print calculations");
        print_calculations_btn.setName("print_calculations");
        jMenu1.add(print_calculations_btn);

        jMenuBar1.add(jMenu1);

        runway_menu.setText("Runway");
        match_compass_btn.setSelected(false);
//        match_compass_btn.addActionListener(new );
        match_compass_btn.setText("Rotate to match Compass Heading");
        runway_menu.add(jSeparator1);

        save_runway_as_btn.setText("Save Runway as Image");
        save_runway_as_btn.setName("save_image");
        match_compass_btn.setName("rotate");
        runway_menu.add(match_compass_btn);
        runway_menu.add(save_runway_as_btn);
        jMenuBar1.add(runway_menu);

        jMenu3.setText("Obstacle");
        add_obstacle_btn.setText("Add obstacle");
        add_obstacle_btn.setName("add_obstacle");
        jMenu3.add(add_obstacle_btn);
        import_obstacle_btn.setText("Import obstacle(s) from XML");
        jMenu3.add(import_obstacle_btn);
        import_obstacle_btn.setName("import_obstacle");
        jMenu3.add(jSeparator1);
        remove_obstacle_btn.setText("Remove obstacle");
        jMenu3.add(remove_obstacle_btn);
        remove_obstacle_btn.setName("remove_obstacle");
        jMenu3.add(jSeparator2);

        show_info_btn.setText("Show Obstacle Information");
        jMenu3.add(show_info_btn);
        show_info_btn.setName("show_info_obstacle");

        jMenuBar1.add(jMenu3);

        jMenu4.setText("Help");

        help_btn.setText("Show help!");
        jMenu4.add(help_btn);
        help_btn.setName("help");

        jMenuBar1.add(jMenu4);

        frame.setJMenuBar(jMenuBar1);

    }

    public JMenuItem getPrintCalculationsBtn() {
        return this.print_calculations_btn;
    }

    public JMenuItem getSaveRunwayAsImageBtn() {
        return this.save_runway_as_btn;
    }

    public JMenuItem getImportObstacleBtn() {
        return this.import_obstacle_btn;
    }

    public JScrollPane getTopPane() {
        return this.topPane;
    }

    public JScrollPane getSidePane() {
        return this.sidePane;
    }

    public void zoomIn() {
        if (isTopView) {
            top_panel.zoomIn();
            topPane.revalidate();
        } else {
            side_panel.zoomIn();
            sidePane.revalidate();
        }
    }

    public void zoomOut() {
        if (isTopView) {
            top_panel.zoomOut();
            topPane.revalidate();
        } else {
            side_panel.zoomOut();
            sidePane.revalidate();
        }
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(975, 600);
    }

    public RecalculatedRunway getRecalculatedRunway() {
        return recR;
    }

    public void setRecalculatedRunway(RecalculatedRunway r) {
        this.recR = r;
    }

    public Runway getRunway() {
        return this.runway;
    }

    public JCheckBoxMenuItem getRotateChk() {

        return this.match_compass_btn;
    }

    public void addListenerRotateChk(ActionListener a) {
        this.getRotateChk().addActionListener(a);
    }

    public JButton getBackBtn() {
        return this.back_btn;
    }

    public void addListenerBackBtn(ActionListener a) {
        this.getBackBtn().addActionListener(a);
    }

    public void addListenerRemoveObstacle(ActionListener a) {
        this.getRemoveObstacleBtn().addActionListener(a);
    }

    public void addListenerFirstThresholdButton(ActionListener a) {
        getFirstNameRadioButton().addActionListener(a);
    }

    public void printAllCalculations() {
        String to_be_printed = "";
        String preamble = "";
        String first;
        String second;
        if (this.getRunway().isObstacleOnRunway()) {
            preamble = this.recR.getFirstName() + "/" + this.recR.getSecondName() + "\n";
            first = this.recR.getFirstRecalculation();
            second = this.recR.getSecondRecalculation();
        } else {
            new ToastMessage("Nothing to be printed - there is not obstacle on the runway!", true);
            return;
        }

        to_be_printed = preamble + first + "\n" + second;
        JTextArea txt = new JTextArea(30, 40);
        txt.setVisible(false);
        txt.setText(to_be_printed);
        try {
            txt.print();
        } catch (PrinterException ex) {
            System.err.println(ex);
        }
    }

    public void viewBreakdownCalculations(boolean isObstacle) {
        if (isObstacle) {
            JTextArea txt = new JTextArea(30, 40);
            if (this.getFirstNameRadioButton().isSelected()) {
                txt.setText(this.recR.getFirstRecalculation());
            } else {
                txt.setText(this.recR.getSecondRecalculation());
            }
            JScrollPane scrollTxt = new JScrollPane(txt);

            //JOptionPane.showMessageDialog(null, scrollTxt);
            Object[] options = {"OK", "Print"};
            Integer input = JOptionPane.showOptionDialog(null, scrollTxt,
                    "Calculations", JOptionPane.DEFAULT_OPTION,
                    JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

            if (input == 1) {
                // print
                try {
                    txt.print();
                } catch (PrinterException e) {
                    e.printStackTrace();
                }
            }

        } else {
            JOptionPane.showMessageDialog(null, "There is no obstacle on the runway. Cannot show the breakdown of the calculations");
        }
    }

    public JPanel getObstacleParametersPanel() {
        return this.obstacle_params_panel;
    }

    public void addListenerViewBreakdownCalculations(ActionListener a) {
        this.getCalculationsBreakdownBtn().addActionListener(a);
    }

    public void addListenerSecondThresholdButton(ActionListener a) {
        getSecondNameRadioButton().addActionListener(a);

    }

    public TopDownPanel getTopDownPanel() {
        return this.top_panel;
    }

    public SideOnPanel getSideOnPanel() {
        return this.side_panel;
    }

    public JRadioButton getFirstNameRadioButton() {
        return this.first_name_threshold;
    }

    public JRadioButton getSecondNameRadioButton() {
        return this.second_name_threshold;
    }

    public Integer getBlastDistance() {
        int maxTry = 3;
        int tries = 0;
        Integer result = 0;
        while (true) {
            String inputValue = JOptionPane.showInputDialog(null, "Enter blast distance in metres.\nExample input: 300", 0);
            if (inputValue == null) {
                return null;
            }
            try {
                result = Integer.parseInt(inputValue);
                break;
            } catch (NumberFormatException ex) {
                tries++;
                if (maxTry == tries) {
                    JOptionPane.showMessageDialog(null, "Alert", "Exceeded number of attempts. Default value of 300 is set", JOptionPane.ERROR_MESSAGE);
                    result = 300;
                    break;
                }
            }
        }

        return result;
    }

    public void setRunwayName(String s) {
        this.runway_name_label.setText(s);
    }

    public JLabel getRunwayName() {
        return this.runway_name_label;
    }

    public void setAirport(Airport a) {
        this.airport = a;
    }

    public Airport getAirport() {
        return this.airport;
    }

    public boolean isTopDown() {
        return this.isTopView;
    }

    public JMenuItem getHelpBtn() {
        return this.help_btn;
    }

    public JMenuItem getObstacleInfoBtn() {
        return this.show_info_btn;
    }

    public void swapViews() {
        CardLayout cl = (CardLayout) this.view_panel.getLayout();
        cl.next(view_panel);
        view_panel.repaint();
        isTopView = !isTopView;
        if (isTopView) {
            this.match_compass_btn.setEnabled(true);
            this.match_compass_btn.setSelected(runwayRotated);
        } else {
            runwayRotated = this.match_compass_btn.isSelected();
            this.match_compass_btn.setEnabled(false);
            this.match_compass_btn.setSelected(false);
        }
    }

    public void rotateTopView() {
        this.top_panel.setRotation(this.match_compass_btn.isSelected());
        this.top_panel.repaint();
        if (this.match_compass_btn.isSelected()) {
            new ToastMessage("Compass heading orientation is ON", false);
        } else {
            new ToastMessage("Compass heading orientation is OFF", false);
        }
    }

    public ButtonGroup getThresholdRadioGroup() {
        return this.threshold_radio_group;
    }

    public JButton getSaveImg() {
        return this.save_img_btn;
    }

    public JMenuItem getAddObstacleBtn() {
        return this.add_obstacle_btn;
    }

    public JMenuItem getRemoveObstacleBtn() {
        return this.remove_obstacle_btn;
    }

    public JLabel getOriginalTORA() {
        return this.tora_original;
    }

    public JLabel getOriginalTODA() {
        return this.toda_original;
    }

    public JLabel getOriginalASDA() {
        return this.asda_original;
    }

    public JLabel getOriginalLDA() {
        return this.lda_original;
    }

    public JLabel getObstacleTORA() {
        return this.tora_obstacle;
    }

    public JLabel getObstacleTODA() {
        return this.toda_obstacle;
    }

    public JLabel getObstacleASDA() {
        return this.asda_obstacle;
    }

    public JLabel getObstacleLDA() {
        return this.lda_obstacle;
    }

    public JButton getCalculationsBreakdownBtn() {
        return this.breakdown_calculations_btn;
    }

    public JRadioButton getTopDown() {
        return this.top_down_radio;
    }

    public JRadioButton getSideOn() {
        return this.side_on_radio;
    }

    public void repaintPanes() {
        top_panel.repaint();
        side_panel.repaint();
        view_panel.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        threshold_radio_group = new javax.swing.ButtonGroup();
        views_radio_group = new javax.swing.ButtonGroup();
        jSeparator10 = new javax.swing.JSeparator();
        runway_name_label = new javax.swing.JLabel();
        back_btn = new javax.swing.JButton();
        view_panel = new javax.swing.JPanel();
        breakdown_calculations_btn = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        LDA = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tora_original = new javax.swing.JLabel();
        toda_original = new javax.swing.JLabel();
        asda_original = new javax.swing.JLabel();
        lda_original = new javax.swing.JLabel();
        obstacle_params_panel = new javax.swing.JPanel();
        LDA1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        tora_obstacle = new javax.swing.JLabel();
        toda_obstacle = new javax.swing.JLabel();
        asda_obstacle = new javax.swing.JLabel();
        lda_obstacle = new javax.swing.JLabel();
        first_name_threshold = new javax.swing.JRadioButton();
        second_name_threshold = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        top_down_radio = new javax.swing.JRadioButton();
        side_on_radio = new javax.swing.JRadioButton();
        jSeparator20 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        save_img_btn = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(800, 600));
        setPreferredSize(new java.awt.Dimension(1007, 730));

        runway_name_label.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        runway_name_label.setText("Runway 09L/27R");

        back_btn.setText("Back to Airport");

        view_panel.setBackground(new java.awt.Color(153, 153, 153));
        view_panel.setMinimumSize(new java.awt.Dimension(300, 300));
        view_panel.setLayout(new java.awt.CardLayout());

        breakdown_calculations_btn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        breakdown_calculations_btn.setText("View breakdown of calculations");
        breakdown_calculations_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                breakdown_calculations_btnActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Original runway parameters"));

        LDA.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LDA.setText("LDA:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("ASDA:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("TODA:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("TORA:");

        tora_original.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tora_original.setText("0");

        toda_original.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        toda_original.setText("0");

        asda_original.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        asda_original.setText("0");

        lda_original.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lda_original.setText("0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5)
                    .addComponent(LDA))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lda_original)
                    .addComponent(asda_original)
                    .addComponent(toda_original)
                    .addComponent(tora_original))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tora_original))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toda_original)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(asda_original)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lda_original)
                    .addComponent(LDA))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        obstacle_params_panel.setBorder(javax.swing.BorderFactory.createTitledBorder("With obstacle"));

        LDA1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        LDA1.setText("LDA:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("ASDA:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("TODA:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("TORA:");

        tora_obstacle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tora_obstacle.setText("0");

        toda_obstacle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        toda_obstacle.setText("0");

        asda_obstacle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        asda_obstacle.setText("0");

        lda_obstacle.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lda_obstacle.setText("0");

        javax.swing.GroupLayout obstacle_params_panelLayout = new javax.swing.GroupLayout(obstacle_params_panel);
        obstacle_params_panel.setLayout(obstacle_params_panelLayout);
        obstacle_params_panelLayout.setHorizontalGroup(
            obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(obstacle_params_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(LDA1))
                .addGap(18, 18, 18)
                .addGroup(obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lda_obstacle)
                    .addComponent(asda_obstacle)
                    .addComponent(toda_obstacle)
                    .addComponent(tora_obstacle))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        obstacle_params_panelLayout.setVerticalGroup(
            obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(obstacle_params_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(tora_obstacle))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toda_obstacle)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(asda_obstacle)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(obstacle_params_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lda_obstacle)
                    .addComponent(LDA1))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(obstacle_params_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(obstacle_params_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        threshold_radio_group.add(first_name_threshold);
        first_name_threshold.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        first_name_threshold.setSelected(true);
        first_name_threshold.setText("09L");

        threshold_radio_group.add(second_name_threshold);
        second_name_threshold.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        second_name_threshold.setText("27R");
        second_name_threshold.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                second_name_thresholdActionPerformed(evt);
            }
        });

        jLabel1.setText("Select threshold");

        jLabel8.setText("Runway Views");

        views_radio_group.add(top_down_radio);
        top_down_radio.setSelected(true);
        top_down_radio.setText("Top-down");
        top_down_radio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                top_down_radioActionPerformed(evt);
            }
        });

        views_radio_group.add(side_on_radio);
        side_on_radio.setText("Side-on");
        side_on_radio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                side_on_radioActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("|");

        save_img_btn.setText("Save picture");
        save_img_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save_img_btnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator10, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel8)
                        .addGap(5, 5, 5)
                        .addComponent(top_down_radio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(side_on_radio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator20, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(first_name_threshold)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(second_name_threshold)))
                        .addGap(0, 528, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(runway_name_label)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(back_btn)
                        .addGap(20, 20, 20))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(view_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(breakdown_calculations_btn, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE))
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(save_img_btn)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(runway_name_label)
                    .addComponent(back_btn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(first_name_threshold)
                            .addComponent(second_name_threshold)
                            .addComponent(jLabel1)
                            .addComponent(jLabel8)
                            .addComponent(top_down_radio)
                            .addComponent(side_on_radio)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(breakdown_calculations_btn)
                                .addGap(0, 167, Short.MAX_VALUE))
                            .addComponent(view_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(save_img_btn)
                        .addGap(17, 17, 17))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSeparator20, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void second_name_thresholdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_second_name_thresholdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_second_name_thresholdActionPerformed

    private void save_img_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save_img_btnActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("./"));
        int actionDialog = chooser.showSaveDialog(this);
        if (actionDialog == JFileChooser.APPROVE_OPTION) {
            File fileName = new File(chooser.getSelectedFile() + ".png");
            if (fileName == null) {
                return;
            }
            if (fileName.exists()) {
                actionDialog = JOptionPane.showConfirmDialog(this,
                        "Replace existing file?");
                // may need to check for cancel option as well
                if (actionDialog == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            // okay to write file
            BufferedImage savefile = isTopDown() ? getTopDownPanel().getImage() : getSideOnPanel().getImage();

            try {
                ImageIO.write(savefile, "png", fileName);
                new ToastMessage("The picture was successfully saved", false);
            } catch (IOException ioe) {
                new ToastMessage("Error saving file. Please try again", true);
            }
        }
    }//GEN-LAST:event_save_img_btnActionPerformed

    private void top_down_radioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_top_down_radioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_top_down_radioActionPerformed

    private void side_on_radioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_side_on_radioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_side_on_radioActionPerformed

    private void breakdown_calculations_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_breakdown_calculations_btnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_breakdown_calculations_btnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LDA;
    private javax.swing.JLabel LDA1;
    private javax.swing.JLabel asda_obstacle;
    private javax.swing.JLabel asda_original;
    private javax.swing.JButton back_btn;
    private javax.swing.JButton breakdown_calculations_btn;
    private javax.swing.JRadioButton first_name_threshold;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JLabel lda_obstacle;
    private javax.swing.JLabel lda_original;
    private javax.swing.JPanel obstacle_params_panel;
    private javax.swing.JLabel runway_name_label;
    private javax.swing.JButton save_img_btn;
    private javax.swing.JRadioButton second_name_threshold;
    private javax.swing.JRadioButton side_on_radio;
    private javax.swing.ButtonGroup threshold_radio_group;
    private javax.swing.JLabel toda_obstacle;
    private javax.swing.JLabel toda_original;
    private javax.swing.JRadioButton top_down_radio;
    private javax.swing.JLabel tora_obstacle;
    private javax.swing.JLabel tora_original;
    private javax.swing.JPanel view_panel;
    private javax.swing.ButtonGroup views_radio_group;
    // End of variables declaration//GEN-END:variables

}
