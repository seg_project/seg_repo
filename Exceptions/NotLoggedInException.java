package Exceptions;

/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
public class NotLoggedInException extends Exception {

    public NotLoggedInException(String not_logged_in) {
        super(not_logged_in); //To change body of generated methods, choose Tools | Templates.
    }

}
