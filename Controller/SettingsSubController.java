/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Controller;

import Model.Airport;
import Model.CustomListModel;
import Model.Obstacle;
import Model.PredefinedData;
import View.SettingsSubView;
import View.ToastMessage;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class SettingsSubController extends SubController {

    SettingsSubView view;
    DefaultListModel<Airport> airport_model;
    DefaultListModel<Obstacle> obstacle_model;

    public static final String runway_concrete = "runway_concrete";
    public static final String background = "background";
    public static final String CGA = "CGA";
    public static final String clearway = "clearway";
    public static final String stopway = "stopway";
    public static final String arrow = "arrow";
    public static final String other = "other";

    public SettingsSubController(MainController m) {
        super(m);
        view = new SettingsSubView();
        /*Set the button listeners*/
        JList airports_list = view.getAirportList();
        view.getAddAirportBtn().addActionListener(new AirportBtnListener(airports_list));
//        view.getEditAirportBtn().addActionListener(new AirportBtnListener(airports_list));
        view.getDeleteAirportBtn().addActionListener(new AirportBtnListener(airports_list));
        JList obstacles_list = view.getObstacleList();
        view.getAddObstacleBtn().addActionListener(new ObstacleBtnListener(obstacles_list));
        view.getEditObstacleBtn().addActionListener(new ObstacleBtnListener(obstacles_list));
        view.getDeleteObstacleBtn().addActionListener(new ObstacleBtnListener(obstacles_list));
        view.getBackBtn().addActionListener(new BackBtnListener());

        ColorPicker picker = new ColorPicker();
        view.getRunwayConcretePanel().addMouseListener(picker);
        view.getRunwayConcretePanel().setName(SettingsSubController.runway_concrete);
        view.getRunwayConcretePanel().setBackground(getSettings().getRunwayColor());

        view.getBackgroundPanel().addMouseListener(picker);
        view.getBackgroundPanel().setName(SettingsSubController.background);
        view.getBackgroundPanel().setBackground(getSettings().getBack_ground());

        view.getArrowPanel().addMouseListener(picker);
        view.getArrowPanel().setName(SettingsSubController.arrow);
        view.getArrowPanel().setBackground(getSettings().getArrow());

        view.getCGAPanel().addMouseListener(picker);
        view.getCGAPanel().setName(SettingsSubController.CGA);
        view.getCGAPanel().setBackground(getSettings().getCGA());

        view.getClearwayPanel().addMouseListener(picker);
        view.getClearwayPanel().setName(SettingsSubController.clearway);
        view.getClearwayPanel().setBackground(getSettings().getClearway());

        view.getStopwayPanel().addMouseListener(picker);
        view.getStopwayPanel().setName(SettingsSubController.stopway);
        view.getStopwayPanel().setBackground(getSettings().getStopway());

        view.getOtherPanel().addMouseListener(picker);
        view.getOtherPanel().setName(SettingsSubController.other);
        view.getOtherPanel().setBackground(getSettings().getOther());

        /*populate the lists with airports and Obstacles*/
        airport_model = new CustomListModel<>(airports_list);
        ArrayList<Airport> airports = getMainController().getModel().getOfflineAirports();
//        view.getAirportList().setModel(airport_model);
        for (Airport a : airports) {
            airport_model.addElement(a);
        }

        obstacle_model = new CustomListModel<>(obstacles_list);
        ArrayList<Obstacle> obstacles = getModel().getObstacles();
//        view.getObstacleList().setModel(obstacle_model);
        for (Obstacle a : obstacles) {
            obstacle_model.addElement(a);
        }

//        MainController.setFirstElementSelected(view.getObstacleList());
//        MainController.setFirstElementSelected(view.getAirportList());
        //set the view to visualise data from the PredefinedSettings
        super.setView(view);
        m.getMainView().changeSubView(view);
    }

    class AirportBtnListener implements
            ActionListener {

        JList airport_list;

        public AirportBtnListener(JList a) {
            this.airport_list = a;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton source = (JButton) ae.getSource();

            if (!airport_list.isSelectionEmpty() || source.getName().equals("add")) {
                Airport airport = (Airport) airport_list.getSelectedValue();
                switch (source.getName()) {
                    case "add":
                        ArrayList<Airport> res = PredefinedData.getInstance().getAirportList(true);
                        if (res != null) {
                            getMainController().getModel().addAirports(res);
                            for (Airport a : res) {
                                airport_model.addElement(a);
                                getMainController().getInitialController().view.getPredefinedPanel().setVisible(true);
                                getMainController().getInitialController().getStartScreenAirportListModel().addElement(a);
                            }
                            if (res.size() == 1) {
                                new ToastMessage(res.get(0) + " successfuly added", true);
                            } else {
                                new ToastMessage(res.size() + " airports successfuly added", true);
                            }
                        }
                        break;
                    case "delete":
                        int dialogButton = JOptionPane.YES_NO_CANCEL_OPTION;
                        int user_input = JOptionPane.showConfirmDialog(null, "Would you like to delete the airport XML file as well?", "Delete XML?", dialogButton);

                        if (user_input == JOptionPane.CLOSED_OPTION || user_input == JOptionPane.CANCEL_OPTION) { //if the user closes the prompt button
                            break;
                        }
                        boolean result = getMainController().getModel().removeAirport(airport, user_input == JOptionPane.YES_OPTION);

                        airport_model.removeElement(airport);

                        getMainController().getInitialController().getStartScreenAirportListModel().removeElement(airport); // remove the airport from the home screen as well
                        System.out.println("[" + result + "] deleting airport");
                        new ToastMessage("Airport deleted", false);

                        if (airport_model.isEmpty()) {
                            getMainController().getInitialController().view.getPredefinedPanel().setVisible(false);
                        }
                        break;
                    case "edit":
                        System.out.println("edit");
                        break;
                    default:
                        System.out.println("default");
                }
            }
        }

    }

    class ObstacleBtnListener implements ActionListener {

        public ObstacleBtnListener(JList list) {
            this.obstacle_list = list;
        }
        JList obstacle_list;

        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton source = (JButton) ae.getSource();

            if (!obstacle_list.isSelectionEmpty() || source.getName().equals("add")) {
                Obstacle obstacle = (Obstacle) obstacle_list.getSelectedValue();
                switch (source.getName()) {
                    case "add":
                        ArrayList<Obstacle> res = PredefinedData.getInstance().getObstacleList();
                        if (res != null) {
                            getMainController().getModel().addObstacles(res);
                            for (Obstacle a : res) {
                                obstacle_model.addElement(a);
//                                getMainController().getInitialController().getStartScreenAirportListModel().addElement(a);
                            }
                            if (res.size() == 1) {
                                new ToastMessage(res.get(0) + " obstacle added successfully", true);
                            } else {
                                new ToastMessage(res.size() + " obstacles added successfully", true);
                            }
                        }
                        break;
                    case "delete":
                        int dialogButton = JOptionPane.YES_NO_OPTION;
                        int user_input = JOptionPane.showConfirmDialog(null, "Would you like to delete the obstacle XML file as well?", "Delete XML?", dialogButton);
                        if (user_input == JOptionPane.CLOSED_OPTION || user_input == JOptionPane.CANCEL_OPTION) { //if the user closes the prompt button
                            break;
                        }
                        boolean result = getMainController().getModel().removeObstacle(obstacle, user_input == JOptionPane.YES_OPTION);

                        obstacle_model.removeElement(obstacle);
                        System.out.println("[" + result + "] deleting obstacle");
                        break;
                    case "edit":
                        boolean successfully_rewriten = false;
                        int position_of_old_element = obstacle_model.indexOf(obstacle);
                        System.out.println("edit");
                        ChangeObstacleSubController ref = new ChangeObstacleSubController(mainController, obstacle);
                        if (ref.getObstacle() != null && !(ref.getObstacle().equals(obstacle))) {
                            System.out.println(ref.getObstacle().getName());
                            successfully_rewriten = PredefinedData.getInstance().saveDefaultObstacle(ref.getObstacle(), true);
//                            successfully_rewriten = true;
                        }
                        if (successfully_rewriten) {
                            new ToastMessage("The obstacle was changed and the XML file was updated!", false);
                            obstacle_model.setElementAt(ref.getObstacle(), position_of_old_element);
                        } else {
                            new ToastMessage("The obstacle XML file was not updated!", true);
                        }
                        break;
                    default:
                        System.out.println("default");
                }
            }
        }

    }

    class BackBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            getMainController().getModel().getSettings().saveSettings();  //we save the settings here so we make just one write write to the xml file
            getMainView().changeSubView(getPreviousController().getView());
        }

    }

    class ColorPicker extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            Color result;
            JColorChooser a = null;
            JPanel source = (JPanel) e.getSource();
            switch (source.getName()) {
                case SettingsSubController.runway_concrete:
                    a = new JColorChooser(view.getRunwayConcretePanel().getForeground());
                    JOptionPane.showMessageDialog(null, a);
                    result = a.getColor();
                    view.getRunwayConcretePanel().setBackground(result);
                    getMainController().getModel().getSettings().setRunwayConcrete(result);
                    break;
                case SettingsSubController.background:
                    a = new JColorChooser(view.getBackgroundPanel().getForeground());
                    JOptionPane.showMessageDialog(null, a);
                    result = a.getColor();
                    view.getBackgroundPanel().setBackground(result);
                    getMainController().getModel().getSettings().setBack_ground(result);
                    break;
                case SettingsSubController.CGA:
                    a = new JColorChooser(view.getCGAPanel().getForeground());
                    JOptionPane.showMessageDialog(null, a);
                    result = a.getColor();
                    view.getCGAPanel().setBackground(result);
                    getMainController().getModel().getSettings().setCGA(result);
                    break;
                case SettingsSubController.clearway:
                    a = new JColorChooser(view.getClearwayPanel().getForeground());
                    JOptionPane.showMessageDialog(null, a);
                    result = a.getColor();
                    view.getClearwayPanel().setBackground(result);
                    getMainController().getModel().getSettings().setClearway(result);
                    break;
                case SettingsSubController.stopway:
                    a = new JColorChooser(view.getStopwayPanel().getForeground());
                    JOptionPane.showMessageDialog(null, a);
                    result = a.getColor();
                    view.getStopwayPanel().setBackground(result);
                    getMainController().getModel().getSettings().setStopway(result);
                    break;
                case SettingsSubController.arrow:
                    a = new JColorChooser(view.getArrowPanel().getForeground());
                    JOptionPane.showMessageDialog(null, a);
                    result = a.getColor();
                    view.getArrowPanel().setBackground(result);
                    getMainController().getModel().getSettings().setArrow(result);
                    break;
                case SettingsSubController.other:
                    a = new JColorChooser(view.getOtherPanel().getForeground());
                    JOptionPane.showMessageDialog(null, a);
                    result = a.getColor();
                    view.getOtherPanel().setBackground(result);
                    getMainController().getModel().getSettings().setOther(result);
                    break;
                default:
                    break;
            }
        }
    }

}
