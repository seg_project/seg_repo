/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Controller;

import Exceptions.NotLoggedInException;
import Model.Airport;
import Model.Connection;
import Model.CustomListModel;
import Model.Log;
import Model.Manual;
import Model.PredefinedData;
import Model.PredefinedManual;
import View.StartScreenSubView;
import View.ToastMessage;
import com.thoughtworks.xstream.XStream;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

public class StartScreenSubController extends SubController {

    StartScreenSubView view;
    DefaultListModel<Airport> JListOfflineAirports;
    static String dummy_airport_name = "";
    Airport dummy = new Airport(StartScreenSubController.dummy_airport_name);
    CustomListModel<Airport> JListOnlineAirportsModel;

    public StartScreenSubController(MainController m) {
        super(m);

        view = new StartScreenSubView(Connection.getInstance().isLoggedIn());

        view.getHelpBtn().addActionListener(new ServiceListener());
        view.addListenerCreateAirport(new CreateAirportListener());
        view.addListenerLoadAirport(new LoadAirportListener());
        view.addListenerSettings(new ServiceListener());
        view.getExitBtn().addActionListener(new ServiceListener());
//        view.getPredefinedPanel().setVisible(m.getModel().isAutmaticallyAddedFromXML());
        JListOfflineAirports = new CustomListModel<>(view.getOfflineList());
        view.getOfflineList().addMouseListener(new OpenOfflineClickListener());
//        view.getOfflineList().setModel(JListOfflineAirports);
        dummy = new Airport(StartScreenSubController.dummy_airport_name);
        if (m.getModel().isAutmaticallyAddedFromXML()) {
            for (Airport a : getMainController().getModel().getOfflineAirports()) {
                JListOfflineAirports.addElement(a);
            }
        }

        view.getOpenOfflineBtn().addActionListener(new AirportListsListener());
        view.getDeleteOfflineAirportBtn().addActionListener(new AirportListsListener());

        // - SET THE ONLINE PANEL
        view.getDeleteOnlineBtn().addActionListener(new AirportListsListener());
        view.getDownloadLocallyBtn().addActionListener(new AirportListsListener());
        view.getUploadBtn().addActionListener(new AirportListsListener());
        view.getLoginButtonOnlinePanel().addActionListener(new AirportListsListener());
        view.getOpenOnlineBtn().addActionListener(new AirportListsListener());
        view.getOnlineList().addMouseListener(new OpenOnlineClickListener());
        if (Connection.getInstance().isLoggedIn()) {

            JListOnlineAirportsModel = new CustomListModel<>(view.getOnlineList());
            try {
                ArrayList<Airport> onlineAirports = getModel().getOnlineAirports();
                Collections.sort(onlineAirports); //sort alphabetically
                for (Airport a : onlineAirports) {
                    Log.addMessage(Connection.getInstance().getUserName() + " logged in.");
                    JListOnlineAirportsModel.addElement(a);
                }
                System.err.println(Connection.getInstance().getUserName() + " logged in.");
                startCloudService();
            } catch (NotLoggedInException ex) {
                System.out.println("Cannot load online airports.");
                Log.addMessage("Cannot load online airports.");
            }

        }

        super.setView(view);
        m.getMainView().changeSubView(view); //assign the subview to the mainview

    }

    //this methods starts a thread which will periodically make a request to the server 
    //and retrieve the airports assosiated with the logged in user.
    public final void startCloudService() {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(1);
        scheduledThreadPool.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                ArrayList<Airport> result = null;
                if (Connection.getInstance().isLoggedIn()) {
                    result = Connection.getInstance().getOnlineAirportsReply();
                }
                if (result != null) {
                    for (Airport ar : result) {
                        System.out.println(ar.getName());
                    }
                    System.out.println("====");
                    updateOnlineList(result);
                }
                System.err.println(Calendar.getInstance().get(Calendar.SECOND));
            }
        }, 3, 3, TimeUnit.SECONDS);
    }

    public DefaultListModel<Airport> getStartScreenAirportListModel() {
        return this.JListOfflineAirports;
    }

    public class OpenOfflineClickListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent ev) {
            if (ev.getClickCount() == 2) {
                view.getOpenOfflineBtn().doClick();
            }

        }
    }

    public class OpenOnlineClickListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent ev) {
            if (ev.getClickCount() == 2) {
                view.getOpenOnlineBtn().doClick();
            }

        }
    }

    public class ServiceListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton source = (JButton) ae.getSource();
            switch (source.getName()) {
                case "settings":
                    new SettingsSubController(mainController).setPreviousSubController(getThisController());

                    break;
                case "manual":
                    if (Connection.getInstance().webPageExist(Manual.startScreenURL)) {
                        Manual.openInBowser(Manual.startScreenURL);
                    } else {
                        PredefinedManual.showManual(getManual().getStartScreen());
                    }
                    break;
                case "exit":
                    int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit the application?");
                    if (result == JOptionPane.YES_OPTION) {
                        System.exit(0);
                    }

                    break;
            }
        }

    }

    public CustomListModel<Airport> getOnlineModel() {
        return this.JListOnlineAirportsModel;
    }

    public int onlineListHasAirport(Airport airport) { //check if "airport" is in the model of the online list
        int result = -1;
        Enumeration<Airport> b = getOnlineModel().elements();
        while (b.hasMoreElements()) {
            Airport curr = b.nextElement();
            if (curr.getName().equals(airport.getName())) {
                result = getOnlineModel().indexOf(curr);
            }
        }
        return result;
    }

    //list is the list we just received from the server
    //we check if any of the new "arrival" airports are already in the online list
    //if they are - replace the old one's with the new one - both in the online list and in the main model's online airports
    //if it is not in the online list, add it to the online and main model
    public void updateOnlineList(final ArrayList<Airport> list) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    for (Airport a : list) {
//                        int index_of_main_model = getModel().getIndexOfOnlineAirport(a); //check whether the airport is in the main model and the index in it
//                        if (index_of_main_model != -1) { //we found the airport in the main model so we will update it
//                            getModel().getOnlineAirports().set(index_of_main_model, a);
//                        } else { //just add the airport to the main model(meaning we haven't found it in the main model)
//                            getModel().getOnlineAirports().add(a);
//                        }
                        getModel().setOnlineAirports(list);
                        int selected_index = view.getOnlineList().getSelectedIndex();
                        getOnlineModel().removeAllElements();
                        for (Airport arr : list) {
                            getOnlineModel().addElement(arr, selected_index);
                        }
                    }
                } catch (Exception ex) {
                    Log.addMessage("Trying to get the online airports without being logged in!");
                    System.err.println("Trying to get the online airports without being logged in!");
                }

            }
        });

//        getModel().getOnlineAirports().
    }

    class AirportListsListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            JButton source = (JButton) ae.getSource();
            switch (source.getName()) {
                case "open_online_btn":
                    if (!view.getOnlineList().isSelectionEmpty()) {
                        final Airport selected_online_airport = (Airport) view.getOnlineList().getSelectedValue();
                        java.awt.EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                new AirportSubController(mainController, selected_online_airport).setPreviousSubController(getThisController());
                            }
                        });
                    }
                    break;
                case "open_offline_btn":
                    if (!view.getOfflineList().isSelectionEmpty()) {
                        final Airport selected_airport = (Airport) view.getOfflineList().getSelectedValue();
                        java.awt.EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                new AirportSubController(mainController, selected_airport).setPreviousSubController(getThisController());
                            }
                        });
                    }
                    break;
                case "delete_online_airport_btn":
                    int result = JOptionPane.showConfirmDialog(null, "Are you "
                            + "sure you want to delete this airport? "
                            + "This will remove it from the cloud. If you do not have the airport locally, "
                            + "you won't be able to access it after deleting it from the cloud.");
                    if (result != JOptionPane.OK_OPTION) {
                        break;
                    }
                    System.out.println("deleting onine airport");
                    if (!view.getOnlineList().isSelectionEmpty()) {
                        Airport a = (Airport) view.getOnlineList().getSelectedValue();
                        String airport_name = a.getName();
                        String response = Connection.getInstance().deleteAirportRequest(a);
//                        System.out.println(response);
                        if (response.equals(Connection.RESPONCE_MSG_OK)) { //deleted from server
                            JListOnlineAirportsModel.removeElement(a);
                            getModel().removeAirport(a, false);
                            Log.addMessage("deleting airport " + airport_name + ". Response from server: " + response);
                            new ToastMessage(airport_name + " deleted successfully from the cloud.", false);
                        } else {
                            new ToastMessage(response, true);
                        }
                    }

                    break;
                case "delete_offline_airport_btn":
                    if (!view.getOfflineList().isSelectionEmpty()) {
                        Airport selected_airport = (Airport) view.getOfflineList().getSelectedValue();
                        String name = selected_airport.getName();
                        int delete = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete " + selected_airport.getName() + "?"
                                + "If you don't have the airport stored in the cloud as well, it will be no longer accessible.");
                        if (delete != JOptionPane.OK_OPTION) {
                            break;
                        }
                        JListOfflineAirports.removeElement(selected_airport);
                        getModel().removeAirport(selected_airport, true);
                        new ToastMessage(name + "  is deleted successfully!", false);
                        Log.addMessage("deleting airport " + name);
                    }

                    break;
                case "download_btn":
                    if (!view.getOnlineList().isSelectionEmpty()) {
                        Airport selected_airport = (Airport) view.getOnlineList().getSelectedValue();
                        XStream xstream = Connection.getInstance().getXStream();
                        Airport temp = (Airport) xstream.fromXML(xstream.toXML(selected_airport));
                        temp.setIsOnlineAirport(false);
                        Enumeration<Airport> en = JListOfflineAirports.elements();
                        Airport air;
                        int index = -1;
                        while (en.hasMoreElements()) {
                            air = en.nextElement();
                            if (air.getName().equals(temp.getName())) { //the airport we are downloading is in the offline list as well - we need to update the offline one with the online one
                                index = JListOfflineAirports.indexOf(air);
                            }
                        }
                        if (index != -1) { //update the existing airport
                            System.out.println("Updated");
                            getModel().updateOfflineAirport(temp);
                            Log.addMessage(temp.getName() + " was downloaded locally");
                            JListOfflineAirports.set(index, temp);
                        } else { //the online airport was not in the offline list, so put the online airport in the offline list.
                            JListOfflineAirports.addElement(temp);
                            getModel().addAirport(temp);
                        }
                        PredefinedData.getInstance().saveDefaultAirport(temp, false);

                    }
                    break;
                case "upload_btn":
                    System.out.println("upload to cloud");
                    if (Connection.getInstance().isLoggedIn()) { //we have the neccesery credentials in the Connection singleton
                        if (!view.getOfflineList().isSelectionEmpty()) {
                            Airport selected_airport = (Airport) view.getOfflineList().getSelectedValue();
                            String response = Connection.getInstance().addAirportRequest(selected_airport);
                            if (response.equals(Connection.RESPONSE_MSG_AIRPORT_UPDATED)) {
                                int index = -1;
                                Enumeration<Airport> en = JListOnlineAirportsModel.elements();
                                Airport air;
                                while (en.hasMoreElements()) {
                                    air = en.nextElement();
                                    if (air.getName().equals(selected_airport.getName())) {
                                        index = JListOnlineAirportsModel.indexOf(air);
                                    }
                                }
                                if (index != -1) { //update the online list with the new airport
                                    System.out.println("UPDATED");
                                    XStream xstream = Connection.getInstance().getXStream();
                                    Airport temp = (Airport) xstream.fromXML(xstream.toXML(selected_airport));
                                    temp.setIsOnlineAirport(true);
                                    JListOnlineAirportsModel.set(index, temp);
                                } else {
                                    System.err.println("not updated list");
                                }
                                new ToastMessage("Online airport updated with local copy", true, JOptionPane.INFORMATION_MESSAGE);
                            } else if (response.equals(Connection.RESPONCE_MSG_OK)) {
                                XStream xstream = Connection.getInstance().getXStream();
                                Airport temp = (Airport) xstream.fromXML(xstream.toXML(selected_airport));
                                temp.setIsOnlineAirport(true);
                                JListOnlineAirportsModel.addElement(temp);
                            } else {
                                new ToastMessage(response, true);
                            }

                        }
                    } else { // we are not logged in.
                        new ToastMessage("You are not logged in the cloud!", true);

                    }
                    break;
                case "login_btn":
                    System.out.println("login button");
                    getMainController().launcher.showLoginScreen();
                    if (Connection.getInstance().isLoggedIn()) {
                        JListOnlineAirportsModel = new CustomListModel<Airport>(view.getOnlineList());
                        try {
                            ArrayList<Airport> onlineAirports = getModel().getOnlineAirports();
                            Collections.sort(onlineAirports); //sort alphabetically
                            for (Airport a : onlineAirports) {
                                JListOnlineAirportsModel.addElement(a);
                            }
                            view.getOnlineList().setModel(JListOnlineAirportsModel);
                        } catch (NotLoggedInException ex) {
                            System.out.println("Cannot load online airports.");
                            Log.addMessage("Cannot load online airports.");
                        }
                    }
                    break;
            }

        }

    }

    class CreateAirportListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            //create airport
            new AddAirportSubController(mainController).setPreviousSubController(getThisController());

        }
    }

    class LoadAirportListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            //load airport
//            PredefinedData pr = PredefinedData.getInstance();
            getMainView().switchOff();
            final ArrayList<Airport> airports = PredefinedData.getInstance().getAirportList(true);

            if (airports != null) { //the user has made a selection
                for (Airport a : airports) {
                    JListOfflineAirports.addElement(a);
//                    System.err.println(a);
                    getMainController().getModel().addAirport(a); // add to the model as well
                }
                if (!getMainController().getModel().isAutmaticallyAddedFromXML()) { //no predefined airports had been loaded when the app started
                    view.getPredefinedPanel().setVisible(true);

                }

                getMainView().switchOn();
                if (airports.size() == 1) {
                    java.awt.EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            new AirportSubController(mainController, airports.get(0)).setPreviousSubController(getThisController());
                        }
                    });
                }
                getMainView().packMain();
                view.repaint();

            } else {
                JOptionPane.showMessageDialog(null, "Airport not added!");
                getMainView().switchOn();
            }
//            System.err.println(JComboModel.getSize());
        }

    }

}
