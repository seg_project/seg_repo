package Controller;

/**
 *
 * @author Toby
 */
public class Validate {
    public static boolean airportName(String name) {
        return name.matches("^[a-zA-Z]([a-z \\-A-Z]*[a-zA-Z])?$");
        //airport names can contain letters with spaces or dashes in the middle
    }
    
    public static boolean runwayName(String name) {
        if((name.length() < 2) || (name.length() > 3)) {
            return false;
        } else {
            String numberPart = name.substring(0, 2);
            if(Validate.number(numberPart)) {
                int number = Integer.parseInt(numberPart);
                if((number >=1) && (number <=36)) {
                    if(name.length() == 3) {
                        String letterPart = name.substring(2);
                        return letterPart.matches("L|R|C");
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
    
    public static boolean number(String num) {
        return num.matches("^[0-9]+$");
    }
    
    public static boolean obstacleName(String name) {
        return name.matches("^[a-zA-Z0-9]([a-z \\-_A-Z0-9]*[a-zA-Z0-9])?$");
    }
    
    public static boolean distance(String dist) {
        return dist.matches("^[//-]?[0-9]+$");
    }
    
    public static boolean userName(String name){
    	return name.matches("^[a-zA-Z0-9]([a-z\\-_A-Z0-9]*[a-zA-Z0-9])?$") && name.length()>0;
    }
    
    
    public static boolean userPassword(String pass){
    	return pass.length()>0;
    }
    
    
}
