package Controller;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Model.Obstacle;
import Model.ObstacleFactory;
import Model.PredefinedData;
import Model.Runway;
import View.AddObstaclePanel;

public class AddObstacleSubController extends SubController {
    
    final AddObstaclePanel view;
    Runway runway; // where obstacle is added
    Integer inputOption; // result of user input

    JComboBox<Obstacle> cmboPredefObst;
    JTextField txtName;
    JTextField txtHeight;
    JTextField txtDist09L;
    JTextField txtDist27R;
    JTextField txtDistCentrLine;
    JCheckBox chbAddAsPredef;
    
    ArrayList<String> tooltips = new ArrayList<>();
    
    public AddObstacleSubController(MainController m, Runway runway) {
        super(m);
        
        this.runway = runway;
        this.view = new AddObstaclePanel(runway.getFirstName(),
                runway.getSecondName());
        if (runway == null) {
            JOptionPane.showMessageDialog(null, "Runway is not selected!");
        } else if (runway.isObstacleOnRunway()) { // runway already has obstacle
            JOptionPane.showMessageDialog(null, "Too many obstacles!");
        } else {
            cmboPredefObst = view.getCmboPredefObst();
            ComboboxToolTipRenderer renderer = new ComboboxToolTipRenderer();
            cmboPredefObst.setRenderer(renderer);
            
            txtName = view.getTxtName();
            txtHeight = view.getTxtHeight();
            txtDistCentrLine = view.getTxtDistCentrLine();
            txtDist09L = view.getTxtDist09L();
            txtDist27R = view.getTxtDist27R();
            chbAddAsPredef = view.getChbAddAsPredef();
            
            chbAddAsPredef.setEnabled(true);
            cmboPredefObst.addItem(new Obstacle(" ", 0, 0, 0, 0, -999));// dummy
            tooltips.add("Select obstacle"); // obstacle

//		
//		System.out.println(PredefinedData.getInstance().getObstacleList()
//				.size());
            for (Obstacle obstacle : getMainController().getModel().getObstacles()) {
//            if (!obstacle.getName().equals("")) {
                cmboPredefObst.addItem(obstacle);
                
                tooltips.add("<html>" + "Height:" + obstacle.getHeight()
                        + "<br>Distance from First Threshold: " + obstacle.getDistanceFirst()
                        + "<br>Distance from Second Threshold: " + obstacle.getDistanceSecond()
                        + "<br>Distance from centerline: "
                        + obstacle.getDistanceY() + "</html>");
//            }
            }
            renderer.setTooltips(tooltips);
            cmboPredefObst.addActionListener(new cmboPredefListener(cmboPredefObst,
                    txtName, txtHeight, txtDist09L, txtDist27R, txtDistCentrLine));
            
            display();
        }
        
    }
    
    public void display() {
        
        inputOption = JOptionPane.showConfirmDialog(null, view, "Add Obstacle",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);
        
        if (inputOption == JOptionPane.OK_OPTION) {
            
            Obstacle obstacle;
            String errorMessage = null;
            
            String name = null;
            Integer height = null;
            Integer distance09L = null;
            Integer distance27R = null;
            Integer distanceFromCentrline = null;
            
            if (!Validate.obstacleName(txtName.getText())) {
                errorMessage = "An invalid obstacle name was entered.";
            } else if (!Validate.number(txtHeight.getText())) {
                errorMessage = "An invalid height was entered.";
            } else if (!Validate.distance(txtDistCentrLine.getText())) {
                errorMessage = "An invalid distance from centreline was entered.";
            } else if (!Validate.distance(txtDist09L.getText())) {
                errorMessage = "An invalid distance from the first threshold was entered.";
            } else if (!Validate.distance(txtDist27R.getText())) {
                errorMessage = "An invalid distance from the second threshold was entered.";
            }
            
            if (errorMessage == null) {
//                obstacle = new Obstacle(name, height, distance09L, distance27R,
//                        distanceFromCentrline, -1);
                name = txtName.getText();
                height = Integer.valueOf(txtHeight.getText());
                distanceFromCentrline = Integer.valueOf(txtDistCentrLine.getText());
                distance09L = Integer.valueOf(txtDist09L.getText());
                distance27R = Integer.valueOf(txtDist27R.getText());
                obstacle = ObstacleFactory.createObstacle(name, height, distance09L, distance27R, distanceFromCentrline);
                if (runway != null) {
                    runway.setObstacle(obstacle);
                }
                getModel().addObstacle(obstacle);
                if (chbAddAsPredef.isSelected()) {
                    PredefinedData.getInstance().saveDefaultObstacle(obstacle, false);
                }
            } else {
                new View.ToastMessage(errorMessage, true);
            }
        } else {
            // TODO: handle exit
        }
    }
    
    class cmboPredefListener implements ActionListener {
        
        JComboBox<Obstacle> combo;
        JTextField txtName;
        JTextField txtHeight;
        JTextField txtDist09L;
        JTextField txtDist27R;
        JTextField txtDistCentrLine;
        
        public cmboPredefListener(JComboBox<Obstacle> combo,
                JTextField txtName, JTextField txtHeight,
                JTextField txtDist09L, JTextField txtDist27R,
                JTextField txtDistCentrLine) {
            
            this.combo = combo;
            this.txtName = txtName;
            this.txtHeight = txtHeight;
            this.txtDist09L = txtDist09L;
            this.txtDist27R = txtDist27R;
            this.txtDistCentrLine = txtDistCentrLine;
            this.txtDistCentrLine = txtDistCentrLine;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if (combo.getItemAt(combo.getSelectedIndex()).getID() != Obstacle.DUMMY_ID) {
                Obstacle obst = combo.getItemAt(combo.getSelectedIndex());
                this.txtName.setText(obst.getName());
                this.txtHeight.setText(String.valueOf(obst.getHeight()));
                this.txtDist09L.setText(String.valueOf(obst.getDistanceFirst()));
                this.txtDist27R.setText(String.valueOf(obst.getDistanceSecond()));
                this.txtDistCentrLine.setText(String.valueOf(obst.getDistanceY()));
            }
            
        }
    }
    
    class ComboboxToolTipRenderer extends DefaultListCellRenderer {
        
        ArrayList<String> tooltips;
        
        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            
            JComponent comp = (JComponent) super.getListCellRendererComponent(
                    list, value, index, isSelected, cellHasFocus);
            
            if (-1 < index && null != value && null != tooltips) {
                list.setToolTipText((String) tooltips.get(index));
            }
            return comp;
        }
        
        public ArrayList<String> getTooltips() {
            return tooltips;
        }
        
        public void setTooltips(ArrayList<String> tooltips) {
            this.tooltips = tooltips;
        }
    }
    
}
