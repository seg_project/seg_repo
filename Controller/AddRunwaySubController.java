package Controller;

import Model.Airport;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Model.Runway;
import View.AddRunwayPanel;
import javax.swing.JLabel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class AddRunwaySubController extends SubController {

    AddRunwayPanel view;
    Integer inputOption; // result of user input
    public static String EDIT = "e";
    public static String NEW = "n";
    String title;

    private Runway runway;
    private Airport airport;

    public AddRunwaySubController(MainController m, Runway r) {
        super(m);
        title = "Edit Runway";
        this.runway = r;
        this.view = new AddRunwayPanel(r);
        view.getTxtName1().getDocument().addDocumentListener(new NameListener(view.getLblFirstName(), view.getTxtName1()));
        view.getTxtName2().getDocument().addDocumentListener(new NameListener(view.getLblSecondName(), view.getTxtName2()));
//        display(AddRunwaySubController.EDIT);
    }

    public AddRunwaySubController(MainController m, Airport a) {
        super(m);
        title = "Add Runway";
        this.view = new AddRunwayPanel(a);
        view.getTxtName1().getDocument().addDocumentListener(new NameListener(view.getLblFirstName(), view.getTxtName1()));
        view.getTxtName2().getDocument().addDocumentListener(new NameListener(view.getLblSecondName(), view.getTxtName2()));
//        display(AddRunwaySubController.NEW);
        this.airport = a;
    }
//    public

    public final Runway display(String Mode) {
        inputOption = JOptionPane.showConfirmDialog(null, view, title,
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);

        if (inputOption == JOptionPane.OK_OPTION) {
            Runway r;

            String name1 = null;
            String name2 = null;

            Integer TORAf = null;
            Integer TODAf = null;
            Integer ASDAf = null;
            Integer LDAf = null;
            Integer treshf = null;

            Integer TORAs = null;
            Integer TODAs = null;
            Integer ASDAs = null;
            Integer LDAs = null;
            Integer treshs = null;
            
            String errorMessage = null;
            
            if(!Validate.runwayName(view.getTxtName1().getText())) {
                errorMessage = "An invalid runway name was entered for the first threshold.";
            } else if(!Validate.runwayName(view.getTxtName2().getText())) {
                errorMessage = "An invalid runway name was entered for the second threshold.";

            } else if(!Validate.number(view.getTxtTORA09L().getText())) {
                errorMessage = "An invalid TORA was entered for the first threshold.";
            } else if(!Validate.number(view.getTxtTODA09L().getText())) {
                errorMessage = "An invalid TODA was entered for the first threshold.";
            } else if(!Validate.number(view.getTxtASDA09L().getText())) {
                errorMessage = "An invalid ASDA was entered for the first threshold.";
            } else if(!Validate.number(view.getTxtLDA09L().getText())) {
                errorMessage = "An invalid LDA was entered for the first threshold.";
            } else if(!Validate.number(view.getTxtDisplacedTresh09L().getText())) {
                errorMessage = "An invalid displaced threshold was entered for the first threshold.";

            } else if(!Validate.number(view.getTxtTORA27R().getText())) {
                errorMessage = "An invalid TORA was entered for the second threshold.";
            } else if(!Validate.number(view.getTxtTODA27R().getText())) {
                errorMessage = "An invalid TODA was entered for the second threshold.";
            } else if(!Validate.number(view.getTxtASDA27R().getText())) {
                errorMessage = "An invalid ASDA was entered for the second threshold.";
            } else if(!Validate.number(view.getTxtLDA27R().getText())) {
                errorMessage = "An invalid LDA was entered for the second threshold.";
            } else if(!Validate.number(view.getTxtDisplacedTresh27R().getText())) {
                errorMessage = "An invalid displaced threshold was entered for the second threshold.";
            } else {
                name1 = view.getTxtName1().getText();
                name2 = view.getTxtName2().getText();
                
                int number1 = Integer.parseInt(name1.substring(0, 2));
                int number2 = Integer.parseInt(name2.substring(0,2));

                TORAf = Integer.valueOf(view.getTxtTORA09L().getText());
                TODAf = Integer.valueOf(view.getTxtTODA09L().getText());
                ASDAf = Integer.valueOf(view.getTxtASDA09L().getText());
                LDAf = Integer.valueOf(view.getTxtLDA09L().getText());
                treshf = Integer.valueOf(view.getTxtDisplacedTresh09L().getText());

                TORAs = Integer.valueOf(view.getTxtTORA27R().getText());
                TODAs = Integer.valueOf(view.getTxtTODA27R().getText());
                ASDAs = Integer.valueOf(view.getTxtASDA27R().getText());
                LDAs = Integer.valueOf(view.getTxtLDA27R().getText());
                treshs = Integer.valueOf(view.getTxtDisplacedTresh27R().getText());

                if(number2 != (number1 + 18)) {
                    errorMessage = "Invalid runway names entered. The first name should "
                            + "be the lower threshold and the second name should be different by 18.";
                } else if((TODAf < TORAf) || (TODAs < TORAs)) {
                    errorMessage = "TODA cannot be less than TORA.";
                } else if((ASDAf < TORAf) || (ASDAs < TORAs)) {
                    errorMessage = "ASDA cannot be less than TORA.";
                } else if((LDAf > TORAf) || (LDAs > TORAs)) {
                    errorMessage = "LDA cannot be greater than TORA.";
                } else if((treshf > TORAf) || (treshs > TORAs)) {
                    errorMessage = "Displaced threshold cannot be greater than TORA.";
                }
            }
            
            if(errorMessage == null) {
                if (Mode.equals(AddRunwaySubController.EDIT)) {
                    runway.setFirstName(name1);
                    runway.setSecondName(name2);
                    runway.setfASDA(ASDAf);
                    runway.setfLDA(LDAf);
                    runway.setfTODA(TODAf);
                    runway.setfTORA(TORAf);
                    runway.setfDisplacedThreshold(treshf);
                    runway.setsDisplacedThreshold(treshs);
                    return null;
                } else if (Mode.equals(AddRunwaySubController.NEW)) {
                    r = new Runway(name1, name2, TORAf, TODAf, ASDAf,
                            LDAf, treshf, TORAs, TODAs, ASDAs, LDAs,
                            treshs);
                    return r;
                } else {
                    return null;
                }
            } else {
                JOptionPane.showMessageDialog(null, errorMessage);
                return null;
            }
        } else {
            return null;
        }
    }
    
    class NameListener implements DocumentListener {
        private JLabel label;
        private JTextField textField;
        
        public NameListener(JLabel l, JTextField t) {
            this.label = l;
            this.textField = t;
        }
        
        @Override
        public void changedUpdate(DocumentEvent e) {
            this.updateLabel();
        }
        
        @Override
        public void removeUpdate(DocumentEvent e) {
            this.updateLabel();
        }
        
        @Override
        public void insertUpdate(DocumentEvent e) {
            this.updateLabel();
        }
        
        private void updateLabel() {
            String text = textField.getText();
            if(text.length() > 3) {
                label.setText(text.substring(0, 3));
            } else {
                label.setText(text);
            }
        }
    }

}
