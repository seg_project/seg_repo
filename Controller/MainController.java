package Controller;

import Model.MainModel;
import System.Launcher;
import View.MainView;
import javax.swing.DefaultListModel;
import javax.swing.JList;

public class MainController {

    // Test code
    private StartScreenSubController initial_subController;
    private MainView view;
    private MainModel model;
    private SubController current;
    private SubController previous;
    
    public Launcher launcher;
    
    public static void setFirstElementSelected(JList a) {
        if (a.getModel().getSize() != 0) {
            a.setSelectedIndex(0);
        }
    }
    
    public MainController(MainView v, MainModel m, Launcher l) {
        this(v, m);
        this.launcher = l;
    }
    
    public MainController(MainView v, MainModel m) {
        this.view = v;
        this.model = m;
        // start the initial subview and its subcontroller
        final MainController con = this;
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                initial_subController = new StartScreenSubController(con);
            }
        });
        this.current = initial_subController;
        previous = initial_subController;
    }
    
    public SubController getCurrentSubController() {
        return this.current;
    }
    
    public StartScreenSubController getInitialController() {
        return this.initial_subController;
    }
    
    public void setCurrentSubController(SubController s) {
        this.current = s;
    }
    
    public MainModel getModel() {
        return this.model;
    }
    
    public MainView getMainView() {
        return view;
    }
    
}
