/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Controller;

import Model.MainModel;
import Model.Manual;
import Model.PredefinedSettings;
import View.MainView;
import View.SubView;

//this is the class which all subcontrollers will extend
//each subcontroller is responsible for one subview
public abstract class SubController {

    MainController mainController;
    MainView mainView;
    SubController previousController;
    private SubView s_view;

    public SubController(MainController m) {
        this.mainController = m;
        this.mainView = m.getMainView();
//        setPreviousSubController(m.getCurrentSubController());
        m.setCurrentSubController(this);

    }

    //this method will be used to explicitly set the previous controller
    SubController getThisController() {
        return this;
    }

    public void setView(SubView s) {
        this.s_view = s;
    }

    public SubView getView() {
        return this.s_view;
    }
    /*
     *The idea is that method would be invoked when 
     *the subview is being created and this method will 
     *populate the data required by the subview
     *Also, when the subcontroller wants to update the whole subview,
     *the subconroller adds data, and fires event which will update
     *all the data on the view
     */

    public MainView getMainView() {
        return this.mainView;
    }

    public SubController getPreviousController() {
        return this.previousController;
    }

    protected void setPreviousSubController(SubController s) {
        this.previousController = s;
    }

    public MainController getMainController() {
        return this.mainController;
    }

    public PredefinedSettings getSettings() {
        return this.mainController.getModel().getSettings();
    }

    public Manual getManual() {
        return this.mainController.getModel().getManual();
    }

    public MainModel getModel() {
        return this.mainController.getModel();
    }
}
