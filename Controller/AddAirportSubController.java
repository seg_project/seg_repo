/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Controller;

import Model.Airport;
import Model.PredefinedData;
import View.AddAirportSubView;
import View.ToastMessage;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AddAirportSubController extends SubController {

    AddAirportSubView view;

    public AddAirportSubController(MainController m) {
        super(m);
        view = new AddAirportSubView();
        setView(view);

        view.addListenerAddAirportBtn(new AddAirportListener());
        view.addListenerCancelBtn(new CancelListener());
        view.getNameField().addMouseListener(new SelectText(view.getNameField()));
//        view.getPredefinedCheck().addActionListener(new PredefinedTick(view.getPredefinedCheck()));
        getMainView().changeSubView(view);
    }

    class PredefinedTick implements ActionListener {

        public JCheckBox check;

        public PredefinedTick(JCheckBox p) {
            this.check = p;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (check.isSelected()) {
                JOptionPane.showMessageDialog(null, "When saving the XML file, use the default directory \nso the airport is loaded on the next run of the program.");
            } else {
            }
        }

    }

    class SelectText extends MouseAdapter {

        public JTextField text;

        public SelectText(JTextField c) {
            this.text = c;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            text.selectAll();
        }
    }

    class AddAirportListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (!Validate.airportName(view.getNameField().getText())) {
                JOptionPane.showMessageDialog(null, "An invalid airport name was entered.");
            } else { //airport name validated successfully
                Airport a = new Airport(view.getNameField().getText());
                getMainController().getModel().addAirport(a);
                if (view.getPredefinedCheck().isSelected()) {

                    boolean result = PredefinedData.getInstance().saveDefaultAirport(a, false); //save the airport to XML

                    if (!result) { //saving to xml not successfull
                        JOptionPane.showMessageDialog(view, "The airport was NOT saved to XML.");
                        new ToastMessage(a.getName() + " was Not saved on the disk as XML. Please check the error "
                                + "log file and try to save the airport manually.", true);
                    }
                }
                if (view.getPredefinedCheck().isSelected()) {
                    new ToastMessage(a.getName() + " was successfully created and saved on the disk ", false);
                } else {
                    new ToastMessage(a.getName() + " was successfully created", false);

                }
                new AirportSubController(mainController, a).setPreviousSubController(getMainController().getInitialController());
                getMainController().getInitialController().view.getPredefinedPanel().setVisible(true);
                getMainController().getInitialController().getStartScreenAirportListModel().addElement(a);
                getMainView().changeSubView(getPreviousController().getView());

            }
        }
    }

    class CancelListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            getMainView().changeSubView(getPreviousController().getView());
        }
    }

}
