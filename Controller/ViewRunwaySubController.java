/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Controller;

import Model.Airport;
import Model.Connection;
import Model.Manual;
import Model.Obstacle;
import Model.PredefinedData;
import Model.PredefinedManual;
import Model.RecalculatedRunway;
import Model.Runway;
import View.MainFrame;
import View.SideOnPanel;
import View.ToastMessage;
import View.TopDownPanel;
import View.ViewRunwaySubView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class ViewRunwaySubController extends SubController {

    private ViewRunwaySubView view;
    private Runway runway;
    private boolean controlDown = false;
    Airport airport;

    public ViewRunwaySubController(MainController mainC, Runway r, Airport a) {
        super(mainC);
        this.runway = r;
        this.airport = a;
        view = new ViewRunwaySubView(this.runway, getMainController().getModel().getSettings());
        view.showMenu(getMainView().getMainFrame());

        this.view.addListenerFirstThresholdButton(new SwapThresholds());
        this.view.addListenerSecondThresholdButton(new SwapThresholds());
        this.view.getSideOn().addActionListener(new SwapViews());
        this.view.getTopDown().addActionListener(new SwapViews());
        this.view.getAddObstacleBtn().addActionListener(new MenuListener());
        this.view.getImportObstacleBtn().addActionListener(new MenuListener());
        this.view.addListenerBackBtn(new BackBtnListener());
        view.addListenerViewBreakdownCalculations(new BreakdownCalculationsBtn());
        view.addListenerRemoveObstacle(new MenuListener());
        this.view.addListenerRotateChk(new MenuListener());
        this.view.getTopPane().addMouseWheelListener(new ZoomListener());
        this.view.getSidePane().addMouseWheelListener(new ZoomListener());
        ControlListener l = new ControlListener();
        this.view.getTopPane().addKeyListener(l);
        this.view.getSidePane().addKeyListener(l);
        this.view.getTopPane().addMouseListener(new FocusListener(this.view.getTopPane()));
        this.view.getSidePane().addMouseListener(new FocusListener(this.view.getSidePane()));
        this.view.getHelpBtn().addActionListener(new MenuListener());
        this.view.getObstacleInfoBtn().addActionListener(new MenuListener());
        this.view.getSaveRunwayAsImageBtn().addActionListener(new MenuListener());
        this.view.getPrintCalculationsBtn().addActionListener(new MenuListener());
//        view.getSwapCombo()
//        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>();
//        model.addElement("Top-down view");
//        model.addElement("Side-on view");
//        view.getSwapCombo().setModel(model);
//        this.mainView.getMainFrame().
        super.setView(view);
        this.mainView.changeSubView(view);
    }

    private void hideMenu() {
        MainFrame frame = getMainView().getMainFrame();
        frame.setJMenuBar(null);
    }

    class FocusListener extends MouseAdapter {

        private JScrollPane panel;

        public FocusListener(JScrollPane p) {
            this.panel = p;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            panel.requestFocusInWindow();
        }
    }

    class ZoomListener implements MouseWheelListener {

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (controlDown) {
                if (e.getWheelRotation() < 0) {
                    view.zoomIn();
                } else if (e.getWheelRotation() > 0) {
                    view.zoomOut();
                }
            }
        }
    }

    class ControlListener implements KeyListener {

        @Override
        public void keyReleased(KeyEvent e) {
            if (!e.isControlDown()) {
                controlDown = false;
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.isControlDown()) {
                controlDown = true;
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {
        }
    }

    public void updateAirport() {
        if (airport.isOnlineAirport()) {
            if (Connection.getInstance().isLoggedIn()) {
                Connection.getInstance().addAirportRequest(airport);
            }
        }
    }

    class MenuListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            JMenuItem source = (JMenuItem) ae.getSource();
            switch (source.getName()) {
                case "save_image":
                    view.getSaveImg().doClick();
                    break;
                case "print_calculations":
                    view.printAllCalculations();
                    break;
                case "add_obstacle":
                    if (view.getRunway().isObstacleOnRunway()) {
                        new View.ToastMessage("Too many obstacles", true);
                        return;
                    }
                    new AddObstacleSubController(mainController, runway);
                    if (view.getRunway().isObstacleOnRunway()) {
                        Integer blast = view.getBlastDistance();
                        if (blast == null) { //this will be the case if in the Enter blast JOptionPane the user has pressed Cancel/Esc
                            runway.removeObstacle();
                            return;
                        }
                        view.getRemoveObstacleBtn().setEnabled(true);
                        view.getAddObstacleBtn().setEnabled(false);
                        view.getObstacleParametersPanel().setEnabled(true);

                        view.setRecalculatedRunway(runway.recalculateParameters(blast));
                        RecalculatedRunway rec = view.getRecalculatedRunway();
                        if (rec.getObstacleType().equals(RecalculatedRunway.OBS_TYPE.NO_EFFECT)) {
                            new View.ToastMessage("Obstacle has no effect on the runway", true);
                        }
                        view.getTopDownPanel().updateRecalculatedRunway(rec);
                        view.getSideOnPanel().updateRecalculatedRunway(rec);
                        view.repaintPanes();
                        if (view.getTopDownPanel().isDirectionRight()) {
                            view.getObstacleASDA().setText(String.valueOf(rec.getfASDA()));
                            view.getObstacleLDA().setText(String.valueOf(rec.getfLDA()));
                            view.getObstacleTODA().setText(String.valueOf(rec.getfTODA()));
                            view.getObstacleTORA().setText(String.valueOf(rec.getfTORA()));
                        } else {
                            view.getObstacleASDA().setText(String.valueOf(rec.getsASDA()));
                            view.getObstacleLDA().setText(String.valueOf(rec.getsLDA()));
                            view.getObstacleTODA().setText(String.valueOf(rec.getsTODA()));
                            view.getObstacleTORA().setText(String.valueOf(rec.getsTORA()));
                        }
                        if (!rec.getObstacleType().equals(RecalculatedRunway.OBS_TYPE.NO_EFFECT)) {

                            new View.ToastMessage("Obstacle added successfully", false);
                        }
                    }
                    break;
                case "import_obstacle":
                    ArrayList<Obstacle> arr = PredefinedData.getInstance().getObstacleList();
                    if (arr == null) {
                        new ToastMessage("Importing obstacle FAILED", false);
                        break;
                    }

                    getModel().addObstacles(arr);
                    if (arr.size() == 1) {
                        new ToastMessage("Importing obstacle SUCCEEDED", false);
                    } else {
                        new ToastMessage("Importing obstacles SUCCEEDED", false);
                    }
                    break;
                case "remove_obstacle":
                    int result = JOptionPane.showConfirmDialog(view, "Are you sure you want to remove this obstacle?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (result == JOptionPane.YES_OPTION) {
                        runway.removeObstacle();
                        view.setRecalculatedRunway(null);
                        view.getTopDownPanel().updateRecalculatedRunway(null);
                        view.getSideOnPanel().updateRecalculatedRunway(null);
                        view.repaintPanes();
                        view.getObstacleParametersPanel().setEnabled(false);
                        view.getRemoveObstacleBtn().setEnabled(false);
                        view.getObstacleASDA().setText("0");
                        view.getObstacleTORA().setText("0");
                        view.getObstacleTODA().setText("0");
                        view.getObstacleLDA().setText("0");
                        view.getAddObstacleBtn().setEnabled(true);

                        new View.ToastMessage("Obstacle removed successfully", true, JOptionPane.INFORMATION_MESSAGE);
                    }
                    break;
                case "show_info_obstacle":
                    if (view.getRunway().isObstacleOnRunway()) {
                        String info = "";

                        Obstacle obst = view.getRunway().getObstacle();
                        info += "Name: " + obst.getName() + "\n";
                        info += "Height: " + obst.getHeight() + "\n";
                        info += "Distance from centreline: " + obst.getDistanceY() + "\n";
                        info += "Distance from first threshold: " + obst.getDistanceFirst() + "\n";
                        info += "Distance from second threshold: " + obst.getDistanceSecond();
                        JOptionPane.showMessageDialog(null, info);
                    }
                    break;
                case "rotate":
                    view.rotateTopView();
                    break;
                case "help":
                    if (Connection.getInstance().webPageExist(Manual.mainViewURL)) {
                        Manual.openInBowser(Manual.mainViewURL);
                    } else {
                        PredefinedManual.showManual(getManual().getStartScreen());
                    }
                    break;
            }
        }
    }

    class BackBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            updateAirport();
            hideMenu();
            getMainView().changeSubView(getPreviousController().getView());
        }
    }

    class BreakdownCalculationsBtn implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            view.viewBreakdownCalculations(view.getRunway().isObstacleOnRunway());
        }
    }

    class SwapViews implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ie) {

            if (view.getTopDown().isSelected()) {
                if (!view.isTopDown()) {
                    view.swapViews();
                    new ToastMessage("Top down view activated", false);
                }

            } else { //sideOn 
                if (view.isTopDown()) {
                    view.swapViews();
                    new ToastMessage("Side on view activated", false);
                }
            }
        }
    }

    class SwapThresholds implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (view.getFirstNameRadioButton().isSelected()) { //if we have selected to face left(e.g 27R)
                view.getTopDownPanel().changeDirection(TopDownPanel.FACING_RIGHT);
                view.getSideOnPanel().changeDirection(SideOnPanel.FACING_RIGHT);

                view.getOriginalTORA().setText(String.valueOf(runway.getfTORA()));
                view.getOriginalTODA().setText(String.valueOf(runway.getfTODA()));
                view.getOriginalASDA().setText(String.valueOf(runway.getfASDA()));
                view.getOriginalLDA().setText(String.valueOf(runway.getfLDA()));

                if (view.getRecalculatedRunway() != null) {

                    view.getObstacleASDA().setText(String.valueOf(view.getRecalculatedRunway().getfASDA()));
                    view.getObstacleTODA().setText(String.valueOf(view.getRecalculatedRunway().getfTODA()));
                    view.getObstacleTORA().setText(String.valueOf(view.getRecalculatedRunway().getfTORA()));
                    view.getObstacleLDA().setText(String.valueOf(view.getRecalculatedRunway().getfLDA()));

                }
            } else {
                view.getTopDownPanel().changeDirection(TopDownPanel.FACING_LEFT);
                view.getSideOnPanel().changeDirection(SideOnPanel.FACING_LEFT);
                view.getOriginalTORA().setText(String.valueOf(runway.getsTORA()));
                view.getOriginalTODA().setText(String.valueOf(runway.getsTODA()));
                view.getOriginalASDA().setText(String.valueOf(runway.getsASDA()));
                view.getOriginalLDA().setText(String.valueOf(runway.getsLDA()));
                if (view.getRecalculatedRunway() != null) {
                    view.getObstacleASDA().setText(String.valueOf(view.getRecalculatedRunway().getsASDA()));
                    view.getObstacleTODA().setText(String.valueOf(view.getRecalculatedRunway().getsTODA()));
                    view.getObstacleTORA().setText(String.valueOf(view.getRecalculatedRunway().getsTORA()));
                    view.getObstacleLDA().setText(String.valueOf(view.getRecalculatedRunway().getsLDA()));
                }
            }
            view.repaintPanes();
        }

    }
}
