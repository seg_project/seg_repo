package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Model.Obstacle;
import Model.ObstacleFactory;
import View.ChangeObstacleSubView;

public class ChangeObstacleSubController extends SubController {

    final ChangeObstacleSubView view;
    private Obstacle obstacle; // selected obstacle
    private Integer inputOption; // result of user input

    private JTextField txtName;
    private JTextField txtHeight;
    private JTextField txtDist09L;
    private JTextField txtDist27R;
    private JTextField txtDistCentrLine;

    public ChangeObstacleSubController(MainController m, Obstacle obstacle) {
        super(m);

        view = new ChangeObstacleSubView("", "");
        this.obstacle = obstacle;

        if (obstacle == null) {
            JOptionPane.showMessageDialog(null, "Obstacle is not selected!");
        } else {

            txtName = view.getTxtName();
            txtHeight = view.getTxtHeight();
            txtDistCentrLine = view.getTxtDistCentrLine();
            txtDist09L = view.getTxtDist09L();
            txtDist27R = view.getTxtDist27R();

            txtName.setText(obstacle.getName());
            txtHeight.setText(String.valueOf(obstacle.getHeight()));
            txtDistCentrLine.setText(String.valueOf(obstacle.getDistanceY()));
            txtDist09L.setText(String.valueOf(obstacle.getDistanceFirst()));
            txtDist27R.setText(String.valueOf(obstacle.getDistanceSecond()));

            display();
        }

    }

    public final void display() {

        inputOption = JOptionPane.showConfirmDialog(null, view, "Change Obstacle",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null);

        if (inputOption == JOptionPane.OK_OPTION) {

            Obstacle obstacle;
            String errorMessage = null;

            String name = null;
            Integer height = null;
            Integer distance09L = null;
            Integer distance27R = null;
            Integer distanceFromCentrline = null;

            if(!Validate.obstacleName(txtName.getText())) {
                errorMessage = "An invalid obstacle name was entered.";
            } else if(!Validate.number(txtHeight.getText())) {
                errorMessage = "An invalid height was entered.";
            } else if(!Validate.distance(txtDistCentrLine.getText())) {
                errorMessage = "An invalid distance from centreline was entered.";
            } else if(!Validate.distance(txtDist09L.getText())) {
                errorMessage = "An invalid distance from the first threshold was entered.";
            } else if(!Validate.distance(txtDist27R.getText())) {
                errorMessage = "An invalid distance from the second threshold was entered.";
            }

            if (errorMessage == null) {
                name = txtName.getText();
                height = Integer.valueOf(txtHeight.getText());
                distanceFromCentrline = Integer.valueOf(txtDistCentrLine.getText());
                distance09L = Integer.valueOf(txtDist09L.getText());
                distance27R = Integer.valueOf(txtDist27R.getText());
                obstacle = ObstacleFactory.createObstacle(name, height,
                        distance09L, distance27R, distanceFromCentrline, this.obstacle.getID());

                this.obstacle = obstacle;
                //PredefinedData.getInstance().saveDefaultObstacle(obstacle);

            } else {
                new View.ToastMessage(errorMessage, true);
            }
        } else {
            // TODO: handle exit
        }
    }

    class cmboPredefListener implements ActionListener {

        JComboBox<Obstacle> combo;
        JTextField txtName;
        JTextField txtHeight;
        JTextField txtDist09L;
        JTextField txtDist27R;
        JTextField txtDistCentrLine;

        public cmboPredefListener(JComboBox<Obstacle> combo,
                JTextField txtName, JTextField txtHeight,
                JTextField txtDist09L, JTextField txtDist27R,
                JTextField txtDistCentrLine) {

            this.combo = combo;
            this.txtName = txtName;
            this.txtHeight = txtHeight;
            this.txtDist09L = txtDist09L;
            this.txtDist27R = txtDist27R;
            this.txtDistCentrLine = txtDistCentrLine;
            this.txtDistCentrLine = txtDistCentrLine;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (combo.getItemAt(combo.getSelectedIndex()).getID() != Obstacle.DUMMY_ID) {
                Obstacle obst = combo.getItemAt(combo.getSelectedIndex());
                this.txtName.setText(obst.getName());
                this.txtHeight.setText(String.valueOf(obst.getHeight()));
                this.txtDist09L
                        .setText(String.valueOf(obst.getDistanceFirst()));
                this.txtDist27R
                        .setText(String.valueOf(obst.getDistanceSecond()));
                this.txtDistCentrLine.setText(String.valueOf(obst
                        .getDistanceY()));
            }

        }
    }

    public Obstacle getObstacle() {
        return obstacle;
    }

}
