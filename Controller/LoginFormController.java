package Controller;

import Model.Connection;
import System.Launcher;
import View.LoginFormSubView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JWindow;

public class LoginFormController {

    LoginFormSubView view;

    Launcher launcher;
    JWindow loadingDisplay;

    public LoginFormController(Launcher launcher) {
        this.launcher = launcher;
        this.view = new LoginFormSubView();
        view.getRegisterBtn1().addActionListener(new RegisterListener());
        view.getLoginBtn1().addActionListener(new LoginListener());
        view.getStartOfflineBtn1().addActionListener(new StartOfflineListener());
//        launcher.makeSplash(false, 1);
        view.getUsernameTxt1().addKeyListener(new EnterListener());
        view.getjPasswordField1().addKeyListener(new EnterListener());
//        loadingDisplay = new JWindow();
//        loadingDisplay.setBackground(new Color(255, 0, 0, 0));
//        loadingDisplay.setSize(50, 50);
//        loadingDisplay.setLocationRelativeTo(view);
//        loadingDisplay.getContentPane().add(
//                new JLabel("", new javax.swing.ImageIcon(getClass().getResource("/View/loader.gif")), SwingConstants.CENTER));
//        loadingDisplay.setVisible(true);
    }

    class EnterListener implements KeyListener {

        @Override
        public void keyTyped(KeyEvent ke) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                JTextField a;
                JPasswordField p;
                try {
                    if (ke.getSource() instanceof JTextField) {
                        a = (JTextField) ke.getSource();
                        switch (a.getName()) {
                            case "user_name_field":
                                if (view.getjPasswordField1().getPassword().length != 0) {
                                    view.getLoginBtn1().doClick();
                                } else {
                                    System.out.println("The password field is empty");
                                }
                                break;
                        }
                    }
                    if (ke.getSource() instanceof JPasswordField) {
                        p = (JPasswordField) ke.getSource();
                        switch (p.getName()) {
                            case "password_field":
                                if (view.getUsernameTxt1().getText().length() != 0) {
                                    view.getLoginBtn1().doClick();
                                } else {
                                    System.out.println("The user name field is empty");
                                }
                                break;

                        }
                    }
                } catch (Exception e) {

                }
            }
        }

        @Override
        public void keyReleased(KeyEvent ke) {
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    class LoginListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String username = view.getUsernameTxt1().getText();
            String password = new String(view.getjPasswordField1().getPassword());

            if (Validate.userName(username)) {
                if (Validate.userPassword(password)) {
                    //Send Login request to the server:					
                    Connection connection = Connection.getInstance();
                    String reply = connection.loginRequest(username, password);

                    switch (reply) {
                        case Connection.RESPONCE_MSG_OK:
                            //TODO: Allow login
//                            loadingDisplay.dispose();

                            launcher.showMainProgram();
                            break;
                        case Connection.RESPONCE_MSG_USER_NOT_FOUND:
                            JOptionPane.showMessageDialog(null, "Your credentials do not match any records in our database", "Not registered or wrong username/password", JOptionPane.WARNING_MESSAGE);
                            break;
                        case Connection.RESPONSE_MSG_CONNECTION_ERROR:
                            JOptionPane.showMessageDialog(null, "Connection Error", "Connection Error", JOptionPane.WARNING_MESSAGE);
                            break;
                        default:
                            JOptionPane.showMessageDialog(null, reply, "Error", JOptionPane.ERROR_MESSAGE);
                            break;
                    }

                } else {
                    JOptionPane.showMessageDialog(null,
                            "Password is not in the correct format",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "Username is not in the correct format",
                        "Input error",
                        JOptionPane.ERROR_MESSAGE);
            }

        }

    }

    class RegisterListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String username = view.getUsernameTxt1().getText();
            String password = new String(view.getjPasswordField1().getPassword());

            if (true | Validate.userName(username)) {
                if (true | Validate.userPassword(password)) {
                    //Send register request to the server:					
                    Connection connection = Connection.getInstance();
                    String reply = connection.RegisterRequest(username, password);

                    switch (reply) {
                        case Connection.RESPONCE_MSG_OK:
                            //TODO: Allow login
                            JOptionPane.showMessageDialog(null, "You are now registered!");
//                            loadingDisplay.dispose();
                            break;
                        case Connection.RESPONCE_MSG_USER_NOT_FOUND:
                            JOptionPane.showMessageDialog(null, "You are not registered in the system", "Not registered", JOptionPane.WARNING_MESSAGE);
                            break;
                        case Connection.RESPONSE_MSG_CONNECTION_ERROR:
                            JOptionPane.showMessageDialog(null, "Connection Error", "Connection Error", JOptionPane.WARNING_MESSAGE);
                            break;
                        default:
                            JOptionPane.showMessageDialog(null, reply, "Error", JOptionPane.ERROR_MESSAGE);
                            break;
                    }

                } else {
                    JOptionPane.showMessageDialog(null,
                            "Password is not in the correct format",
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null,
                        "Username is not in the correct format",
                        "Input error",
                        JOptionPane.ERROR_MESSAGE);
            }

        }

    }

    class StartOfflineListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            launcher.showMainProgram();
//            loadingDisplay.setVisible(false);
        }

    }

    public LoginFormSubView getView() {
        return view;
    }

}
