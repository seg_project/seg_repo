/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Controller;

import Model.Airport;
import Model.Connection;
import Model.CustomListModel;
import Model.Manual;
import Model.PredefinedData;
import Model.PredefinedManual;
import Model.Runway;
import View.AirportSubView;
import View.ToastMessage;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;

public class AirportSubController extends SubController {

    AirportSubView view;
    Airport airport;
    boolean edit = false; //if we edit an airport this will be true;
//  AirportSubController current_controller;
    CustomListModel<Runway> model;
    private boolean askToSave;

    public AirportSubController(MainController m, Airport a) {
        super(m);
        this.askToSave = false;
        airport = a;
        this.view = new AirportSubView(a);
        view.addListenerBackBtn(new BackBtnListener());
        view.addListenerDeleteRunwayBtn(new DeleteRunwayListener(view.getJListOfRunways()));
        view.addListenerEditBtn(new EditRunwayListener(view.getJListOfRunways()));
        view.addListenerAddBtn(new AddRunwayListener(view.getJListOfRunways()));
        view.addListenerViewBtn(new ViewRunwayListener(view.getJListOfRunways()));
        view.getSaveAsBtn().addActionListener(new SaveListener());
        view.getSaveToXMLBtn().addActionListener(new SaveListener());
        view.getHelpBtn().addActionListener(new ManualListener());
        view.getJListOfRunways().addMouseListener(new ViewRunwayClickListener());
        view.getAirportNameLabel().setText(a.getName());
//        view.getJListOfRunways()
        model = new CustomListModel<>(view.getJListOfRunways());
        for (Runway r : a.getRunways()) {
            model.addElement(r);
        }

        super.setView(view);
        getMainView().changeSubView(view);
    }

    public CustomListModel<Runway> getRunwayListModel() {
        return this.model;
    }

    public AirportSubView getSubView() {
        return this.view;
    }

    public void updateAirport() {
        if (airport.isOnlineAirport()) {
            if (Connection.getInstance().isLoggedIn()) {
                Connection.getInstance().addAirportRequest(airport);
            }
        }
    }

    class BackBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            updateAirport();//update the airport if it is online airport 
            if ((!airport.isOnlineAirport()) && (askToSave == true)) { //if this is a local airport
                int result = JOptionPane.showConfirmDialog(null, "Do you want to save any changes you have made so they are available on the next program start");

                if (result == JOptionPane.YES_OPTION) {
                    view.getSaveToXMLBtn().doClick();
                    new ToastMessage(airport.getName() + " was updated", false);
                    getMainView().changeSubView(getPreviousController().getView());
                } else if (result == JOptionPane.NO_OPTION) {
                    getMainView().changeSubView(getPreviousController().getView());
                } else {
                    return;
                }
            } else {
                getMainView().changeSubView(getPreviousController().getView());

            }
        }
    }

    class ManualListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {

            if (Connection.getInstance().webPageExist(Manual.airportViewURL)) {
                Manual.openInBowser(Manual.airportViewURL);
            } else {
                PredefinedManual.showManual(getManual().getStartScreen());
            }
        }

    }

    class DeleteRunwayListener implements ActionListener {

        public DeleteRunwayListener(JList l) {
            this.list = l;
        }
        JList<Runway> list;

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (!list.isSelectionEmpty()) {
                int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this runway?");
                System.err.println(result);
                if (result != JOptionPane.YES_OPTION) {
                    new ToastMessage("The runway was NOT deleted", false);
                    return;
                }

                Runway runway_to_be_removed = list.getSelectedValue();
                airport.removeRunway(runway_to_be_removed);
                getRunwayListModel().removeElement(runway_to_be_removed);
                new ToastMessage("The runway was deleted!", true);
                askToSave = true;
                updateAirport();
            }
        }

    }

    class EditRunwayListener implements ActionListener {

        JList<Runway> list;

        public EditRunwayListener(JList<Runway> list) {
            this.list = list;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (!list.isSelectionEmpty()) {
                Runway runway_to_be_edit = list.getSelectedValue();
                new AddRunwaySubController(mainController, runway_to_be_edit).display(AddRunwaySubController.EDIT);
                list.updateUI();
                askToSave = true;
                updateAirport();
//                airport.removeRunway(runway_to_be_removed);
//                view.getListModel().removeElement(runway_to_be_removed);

            }
        }

    }

    class ViewRunwayClickListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent ev) {
            if (ev.getClickCount() == 2) {
                view.getViewBtn().doClick();
            }
        }
    }

    class AddRunwayListener implements ActionListener {

        public AddRunwayListener(JList<Runway> list) {
            this.list = list;
        }
        JList<Runway> list;

        @Override
        public void actionPerformed(ActionEvent ae) {
            Runway r = new AddRunwaySubController(mainController, airport).display(AddRunwaySubController.NEW);
            if (r != null) {
                airport.addRunway(r);
                getRunwayListModel().addElement(r);
                list.updateUI();
                askToSave = true;
                updateAirport();
            }
        }

    }

    class ViewRunwayListener implements ActionListener {

        public ViewRunwayListener(JList<Runway> list) {
            this.list = list;
        }
        JList<Runway> list;

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (!list.isSelectionEmpty()) {
                new ViewRunwaySubController(mainController, list.getSelectedValue(), airport).setPreviousSubController(getThisController());

            }
        }

    }

    class SaveListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            boolean result = false; //whether or not writing of the xml is successfull
            JButton source = (JButton) ae.getSource();
            switch (source.getName()) {
                case "save_to":
                    result = PredefinedData.getInstance().saveDefaultAirport(airport, false);
                    break;
                case "save_as":
                    result = PredefinedData.getInstance().saveDefaultAirport(airport, true);
                    break;
                default:
                    break;
            }
            if (result) {
                new ToastMessage(airport + " successfully saved to the default dircetory(" + new File(PredefinedData.default_airports_directory).getAbsolutePath() + ")", true, JOptionPane.INFORMATION_MESSAGE);
                askToSave = false;
            } else {
                new ToastMessage("The airport could NOT be saved", true);
            }
        }

    }
}
