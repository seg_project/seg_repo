package System;

import Controller.LoginFormController;
import Controller.MainController;
import Model.Airport;
import Model.Connection;
import Model.Log;
import Model.MainModel;
import Model.Manual;
import Model.PredefinedData;
import Model.PredefinedManual;
import Model.PredefinedSettings;
import View.MainView;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.SplashScreen;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JWindow;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

public class Launcher {

    public static boolean developing = false;

    JFrame loginFrame;
    LoginFormController loginController;

    JWindow window;

    public Launcher() {
        loginFrame = new JFrame();
        loginController = new LoginFormController(this);
        java.net.URL url = ClassLoader.getSystemResource("View/img/R.jpg");

        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.createImage(url);
        loginFrame.setIconImage(img);
        loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        loginFrame.setLocationRelativeTo(null);
        loginFrame.setContentPane(loginController.getView());
        loginFrame.setResizable(false);
        loginFrame.pack();
        loginFrame.setLocationRelativeTo(null);

    }

    public void stopSplash() {
        window.setVisible(false);
    }

    public void startSplash() {
        window = new JWindow();
        try {
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    ImageIcon icon = new ImageIcon(getClass().getResource("/View/img/logo.png"));
                    window.getContentPane().add(
                            new JLabel("", icon, SwingConstants.CENTER));
                    window.setSize(360, 245);
                    window.setLocationRelativeTo(null);
                    window.setVisible(true);

                }
            });

//                window.pack();
        } catch (Exception ex) {
            Logger.getLogger(Launcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        //<editor-fold defaultstate="collapsed" desc="Nimbus look and feel initialisation :)">
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception e) {
            // If Nimbus is not available, fall back to cross-platform
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (Exception ex) {
                // not worth my time
            }
        }
//</editor-fold>

        final Launcher launcher = new Launcher();
        launcher.showLoginScreen();
    }

    public void showLoginScreen() {
        loginFrame.setVisible(true);

    }

    public void showMainProgram() {
        loginFrame.setVisible(false);
        loginFrame.dispose();
        makeSplash(developing, 1);
    }

    private void initSettings(MainModel model) {
        //load predefined settings(if available)
        PredefinedSettings set = PredefinedData.getInstance().onLoadCheckForSettings();
        if (set == null) {
            set = new PredefinedSettings("default");
            PredefinedData.getInstance().savePredefinedSettings(set, false);
            System.out.println("A new Settings XML file was created and saved in the default application directory " + new File(PredefinedData.default_settings_directory).getAbsolutePath());
        } else {
            System.out.println("Settings automacilly added from the default directory  " + new File(PredefinedData.default_settings_directory).getAbsolutePath());
        }
        model.setSettings(set);
    }

    private void initObstacles(MainModel model) {
        boolean autamatically_added_obstacles_from_xml = model.getObstacles().addAll(PredefinedData.getInstance().onLoadCheckForObstacles());
        try {
            String ss = new File(PredefinedData.default_obstacles_directory).getCanonicalPath();
            if (autamatically_added_obstacles_from_xml) {
                System.out.println("Obstacles automatically added from the default directory " + ss);
            } else {
                System.out.println("Obstacles were NOT automatically added from the default directory " + ss);
            }

        } catch (IOException ex) {
        }
    }

    private void initOfflineAirports(MainModel model) {
        //load any predefined obstacles and airports now
        ArrayList<Airport> p_a = PredefinedData.getInstance().onLoadCheckForAirports();
        Collections.sort(p_a, new AirportNameSort()); //sort by airport name
        boolean autamatically_added_airports_from_xml = model.getOfflineAirports().addAll(p_a);
        model.setIsAutomaticallyAddedFromXML(autamatically_added_airports_from_xml);
        try {
            String f = new File(PredefinedData.default_airports_directory).getCanonicalPath();
            if (autamatically_added_airports_from_xml) {
                System.out.println("Airports automatically added from the default directory " + f);
            } else {
                System.out.println("Airports were NOT automatically added from the default directory " + f);
            }
        } catch (IOException ex) {
        }
    }

    private void initManual(MainModel model) {

        Manual manual = PredefinedManual.getInstance().initialise(); // load the Manual so it is ready to be used
        model.setManual(manual);

    }

    private void initOnlineAirports(MainModel model) {
        //TO DO -- set the online airports' arraylist in the main model.
        ArrayList<Airport> airports_array = Connection.getInstance().getOnlineAirportsReply();
        model.setOnlineAirports(airports_array);
    }

    private MainModel initModel() {
        final MainModel model = new MainModel();
        PredefinedData.getInstance().createDefaultDirectories();

        ExecutorService executorService = Executors.newFixedThreadPool(10);
//        initManual(model);
//        initSettings(model);
//        initAirports(model);
//        initObstacles(model);

        executorService.execute(new Runnable() {
            public void run() {
                initManual(model);

            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                initSettings(model);

            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                initOfflineAirports(model);

            }
        });
        executorService.execute(new Runnable() {
            public void run() {
                initObstacles(model);

            }
        });
        if (Connection.getInstance().isLoggedIn()) {
            executorService.execute(new Runnable() {
                public void run() {
                    initOnlineAirports(model);
                }
            });
        }
        executorService.shutdown();
        try {
            executorService.awaitTermination(30, TimeUnit.SECONDS); //blocks until all of the tasks above are finished
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Couldn't load resources. Program is terminating now!");
            System.exit(1);
        }
        return model;
    }
    volatile int progress = 0;

    public void makeSplash(boolean testing, int seconds) {
        final Launcher launcher = this;

        SwingWorker worker = new SwingWorker() {

            @Override
            protected Object doInBackground() throws Exception {
                try {
                    // before starting
                    launcher.startSplash();
                    long start = System.currentTimeMillis();
                    MainModel model = initModel();
                    long end = System.currentTimeMillis();
                    System.err.println("loading time is " + (double) (end - start) / 1000.0 + "seconds");
                    Thread.sleep(1000);
                    MainView view;
                    view = MainView.getInstance();

                    MainController a = new MainController(view, model, launcher);
//                    window.setVisible(false);
//            stopSplash();
                } catch (Exception e) {
                    Log.addMessage(e.getMessage());
                }
                return null;
            }

            @Override
            protected void done() {

                window.setVisible(false);

            }

        };
        worker.execute();

//            }
//        });
    }
    static SplashScreen mySplash;
    static Rectangle2D splashTextArea;
    static Rectangle2D splashProgressArea;
    static Graphics2D splashGraphics;
    static Font font;

}

class AirportNameSort implements Comparator<Airport> {

    @Override
    public int compare(Airport t, Airport t1) {
        return t.getName().compareTo(t1.getName());
    }

}
