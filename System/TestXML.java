package System;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import Model.Airport;
import Model.Obstacle;
import Model.ObstacleFactory;
import Model.PredefinedData;

public class TestXML {

    PredefinedData pd = PredefinedData.getInstance();
    @Test
    public void test_getInstance() {
        assertNotEquals(null, pd);
    }
    
    @Test
    public void test_getAirportList() {
        ArrayList<Airport> airports = pd.getAirportList(true);
        assertNotEquals(airports, null);

    }

    
    @Test
    public void test_getObstacleList() {
        ArrayList<Obstacle> obstacles = pd.getObstacleList();
        assertNotEquals(obstacles, null);

    }

    @Test
    public void test_addDefaultObstacle() {
        Obstacle obst = ObstacleFactory.createObstacle("unitest", 20, 30, 40, 10);
        assertNotEquals(obst, null);
        ArrayList<Obstacle> obstacles = pd.getObstacleList();
        int size = obstacles.size();
        assertEquals(obstacles.size(),size++);
    }
    
    
    
    @Test
    public void test_removeDefaultObstacle() {
        ArrayList<Obstacle> obstacles = pd.getObstacleList();
        assertNotEquals(obstacles.get(0), null);
        int size = obstacles.size();
        pd.removeDefaultObstacle(obstacles.get(0));
        assertEquals(obstacles.size(),--size);
    }
    
    
    @Test
    public void test_addDefaultAirport() {

        Airport airport = new Airport("test"); 
        assertNotEquals(airport, null);
        ArrayList<Airport> airports = pd.getAirportList(true);
        int size = airports.size();
        assertEquals(airports.size(),size+1);
    }
    
    @Test
    public void test_removeDefaultAirport() {

        Airport airport = new Airport("test"); 
        assertNotEquals(airport, null);
        ArrayList<Airport> airports = pd.getAirportList(true);
        int size = airports.size();
        assertEquals(airports.size(),--size);
    }
    



}
