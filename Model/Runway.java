package Model;

public class Runway {

    private String firstName; //split the name of the runway into its 2 parts.
    private String secondName;

    private final int CLEARED_FROM_CENTRELINE = 75;
    private final int CLEARED_FROM_RUNWAY = -60;

    //these constants refer to the 2d array indexs which hold the corresponding data
    //EG index [FIRST][TORA] is the TORA for the first half of the runway.
    private final int FIRST = 0;
    private final int SECOND = 1;

    private final int TORA = 0;
    private final int ASDA = 1;
    private final int TODA = 2;
    private final int LDA = 3;
    private final int STOPWAY = 4;
    private final int CLEARWAY = 5;
    private final int DISP_THRESH = 6;

    private int[][] runwayParameters = new int[2][7];

    private boolean obstacleOnRunway;
    private Obstacle obstacle;

    private String airportname;

    public Runway(String fName, String sName, int fTORA, int fTODA, int fASDA, int fLDA, int fDispThresh,
            int sTORA, int sTODA, int sASDA, int sLDA, int sDispThresh) {
        this.firstName = fName;
        this.secondName = sName;
        this.runwayParameters[FIRST][TORA] = fTORA;
        this.runwayParameters[FIRST][TODA] = fTODA;
        this.runwayParameters[FIRST][ASDA] = fASDA;
        this.runwayParameters[FIRST][LDA] = fLDA;
        this.runwayParameters[FIRST][DISP_THRESH] = fDispThresh;

        this.runwayParameters[SECOND][TORA] = sTORA;
        this.runwayParameters[SECOND][TODA] = sTODA;
        this.runwayParameters[SECOND][ASDA] = sASDA;
        this.runwayParameters[SECOND][LDA] = sLDA;
        this.runwayParameters[SECOND][DISP_THRESH] = sDispThresh;

        this.runwayParameters[FIRST][STOPWAY] = this.runwayParameters[FIRST][ASDA] - this.runwayParameters[FIRST][TORA];
        this.runwayParameters[FIRST][CLEARWAY] = this.runwayParameters[FIRST][TODA] - this.runwayParameters[FIRST][TORA];

        this.runwayParameters[SECOND][STOPWAY] = this.runwayParameters[SECOND][ASDA] - this.runwayParameters[SECOND][TORA];
        this.runwayParameters[SECOND][CLEARWAY] = this.runwayParameters[SECOND][TODA] - this.runwayParameters[SECOND][TORA];

        this.obstacleOnRunway = false;

        this.airportname = airportname;
    }

    public String getAirportName() {
        return airportname;

    }

    private boolean isRecalculationNeeded() {
        //first check if the obstacle is too high or low to cause a problem
        if ((Math.abs(this.obstacle.getDistanceY()) > this.CLEARED_FROM_CENTRELINE)
                //now check if it is too far left to be a problem
                || (this.obstacle.getDistanceFirst() + this.getfDisplacedThreshold() + this.getsStopwayLength() < this.CLEARED_FROM_RUNWAY)
                //now check if it is too far right to be a problem
                || (this.obstacle.getDistanceSecond() + this.getsDisplacedThreshold() + this.getfStopwayLength() < this.CLEARED_FROM_RUNWAY)) {
            return false;
        } else {
            return true;
        }
    }

    private void determineObstacleLocation(RecalculatedRunway recal) {
        if ((this.obstacle.getDistanceFirst() + this.getfDisplacedThreshold()) > (this.obstacle.getDistanceSecond() + this.getsDisplacedThreshold())) {
            recal.setObstacleType(RecalculatedRunway.OBS_TYPE.OBSTACLE_AT_SECOND);
        } else {
            recal.setObstacleType(RecalculatedRunway.OBS_TYPE.OBSTACLE_AT_FIRST);
        }
    }

    public RecalculatedRunway recalculateParameters(int blastProtection) {
        RecalculatedRunway recal = new RecalculatedRunway();
        recal.setFirstName(this.getFirstName());
        recal.setSecondName(this.getSecondName());
        if (this.obstacleOnRunway) { //first check there is actually an obstacle
            if (this.isRecalculationNeeded()) {  //now check if that obstacle will mean parameters need to be recalculated
                this.determineObstacleLocation(recal); //first of all we need to calculate which side of the runway the obstacle is on
                //recalculate some parameters!!
                recal.setBlast(blastProtection);
                String[] breakdown = new String[2];
                int index;
                int threshold;
                int distanceFromThreshold;
                int distanceFromOtherThreshold;
                int takeOffOverThresh;
                if (recal.getObstacleType().equals(RecalculatedRunway.OBS_TYPE.OBSTACLE_AT_FIRST)) {
                    breakdown[0] = "Obstacle is at the first half of the runway.\n\n\n";
                    breakdown[1] = "Obstacle is at the first half of the runway.\n\n\n";
                    index = 0;
                    threshold = 0;
                    distanceFromThreshold = this.obstacle.getDistanceFirst();
                    distanceFromOtherThreshold = this.obstacle.getDistanceSecond();
                    takeOffOverThresh = 1;
                } else {
                    breakdown[0] = "Obstacle is at the second half of the runway.\n\n\n";
                    breakdown[1] = "Obstacle is at the second half of the runway.\n\n\n";
                    index = 1;
                    threshold = 1;
                    distanceFromThreshold = this.obstacle.getDistanceSecond();
                    distanceFromOtherThreshold = this.obstacle.getDistanceFirst();
                    takeOffOverThresh = 0;
                }

                //this part works out the parameters for taking off away from obstacle
                breakdown[index] = breakdown[index] + "Calculations of TORA, TODA and ASDA when taking off away from obstacle:\n";
                int takeOffAwayLength;
                if (blastProtection < 240 + 60) {
                    breakdown[index] = breakdown[index] + "Blast protection is less than RESA + Strip End so use RESA + Strip End\n";
                    takeOffAwayLength = 240;
                    recal.setTakeOffAway(RecalculatedRunway.DIST_TYPE.RESA);
                    recal.setTakeOffAwayLength(takeOffAwayLength);
                    takeOffAwayLength = 300;
                } else {
                    breakdown[index] = breakdown[index] + "Blast protection is greater than RESA + Strip End so use blast protection\n";
                    takeOffAwayLength = blastProtection;
                    recal.setTakeOffAway(RecalculatedRunway.DIST_TYPE.BLAST);
                    recal.setTakeOffAwayLength(takeOffAwayLength);
                }
                breakdown[index] = breakdown[index] + "Distance to deduct = " + takeOffAwayLength + " + obstacle distance from threshold + threshold distance from end of runway\n";
                breakdown[index] = breakdown[index] + "Distance to deduct = " + takeOffAwayLength + " + " + distanceFromThreshold + " + " + this.runwayParameters[threshold][DISP_THRESH] + "\n";
                int deductedDistance = takeOffAwayLength + distanceFromThreshold + this.runwayParameters[threshold][DISP_THRESH];
                breakdown[index] = breakdown[index] + "Distance to deduct = " + deductedDistance + "\n";
                if (deductedDistance < 0) {
                    deductedDistance = 0;
                    breakdown[index] = breakdown[index] + "Distance to deduct is less than 0 so don't change TORA, TODA and ASDA\n";
                } else {
                    breakdown[index] = breakdown[index] + "New TORA/TODA/ASDA = old TORA/TODA/ASDA - distance to deduct\n";
                    breakdown[index] = breakdown[index] + "New TORA = " + this.runwayParameters[index][TORA] + " - " + deductedDistance + "\n";
                    breakdown[index] = breakdown[index] + "New TODA = " + this.runwayParameters[index][TODA] + " - " + deductedDistance + "\n";
                    breakdown[index] = breakdown[index] + "New ASDA = " + this.runwayParameters[index][ASDA] + " - " + deductedDistance + "\n";
                }
                recal.setTORA(index, this.runwayParameters[index][TORA] - deductedDistance);
                recal.setTODA(index, this.runwayParameters[index][TODA] - deductedDistance);
                recal.setASDA(index, this.runwayParameters[index][ASDA] - deductedDistance);
                breakdown[index] = breakdown[index] + "TORA = " + recal.getTORA(index) + "\n";
                breakdown[index] = breakdown[index] + "TODA = " + recal.getTODA(index) + "\n";
                breakdown[index] = breakdown[index] + "ASDA = " + recal.getASDA(index) + "\n";
                if (recal.getTORA(index) < 0) {
                    breakdown[index] = breakdown[index] + "TORA less than 0 so no space left on runway for TORA\n";
                    recal.setTORA(index, 0);
                }
                if (recal.getTODA(index) < 0) {
                    breakdown[index] = breakdown[index] + "TODA less than 0 so no space left on runway for TODA\n";
                    recal.setTODA(index, 0);
                }
                if (recal.getASDA(index) < 0) {
                    breakdown[index] = breakdown[index] + "ASDA less than 0 so no space left on runway for ASDA\n";
                    recal.setASDA(index, 0);
                }
                breakdown[index] = breakdown[index] + "\n";
                //this part works out LDA for landing over the obstacle
                breakdown[index] = breakdown[index] + "Calculation of LDA when landing over the obstacle:\n";
                breakdown[index] = breakdown[index] + "1 in 50 Slope = 50 x Obstacle Height\n";
                breakdown[index] = breakdown[index] + "Slope = 50 x " + this.obstacle.getHeight() + "\n";
                int slopeLength = this.obstacle.getHeight() * 50;
                breakdown[index] = breakdown[index] + "Slope = " + slopeLength + "\n";
                int usedLength;
                //need to choose the biggest out of slope, RESA and blast
                //set the type used in RecalculatedRunway
                if (slopeLength > 240) {
                    usedLength = slopeLength;
                    recal.setLandOver(RecalculatedRunway.DIST_TYPE.SLOPE);
                    breakdown[index] = breakdown[index] + "Slope length is greater than a RESA of 240 so use slope length\n";
                } else {
                    usedLength = 240;
                    recal.setLandOver(RecalculatedRunway.DIST_TYPE.RESA);
                    breakdown[index] = breakdown[index] + "RESA of 240 is greater than slope length so use RESA\n";
                }
                if (usedLength + 60 < blastProtection) {
                    usedLength = blastProtection;
                    recal.setLandOver(RecalculatedRunway.DIST_TYPE.BLAST);
                    breakdown[index] = breakdown[index] + "Blast protection is greater than both slope length and RESA + Strip End so use Blast protection\n";
                    breakdown[index] = breakdown[index] + "Distance to deduct from LDA = " + usedLength + " + obstacle distance from threshold\n";
                    breakdown[index] = breakdown[index] + "Distance to deduct from LDA = " + usedLength + " + " + distanceFromThreshold + "\n";
                }
                recal.setLandOverLength(usedLength);
                if (!recal.getLandOver().equals(RecalculatedRunway.DIST_TYPE.BLAST)) {
                    breakdown[index] = breakdown[index] + "Distance to deduct from LDA = " + usedLength + " + Strip End + obstacle distance from threshold\n";
                    breakdown[index] = breakdown[index] + "Distance to deduct from LDA = " + usedLength + " + 60 + " + distanceFromThreshold + "\n";
                    usedLength = usedLength + 60;
                }
                //now work out the actual deduction of the LDA
                int ldaDeduction = distanceFromThreshold + usedLength;
                breakdown[index] = breakdown[index] + "Distance to deduct from LDA = " + ldaDeduction + "\n";
                if (ldaDeduction < 0) {
                    ldaDeduction = 0;
                    breakdown[index] = breakdown[index] + "Distance to deduct is less than 0 so don't change LDA\n";
                } else {
                    breakdown[index] = breakdown[index] + "New LDA = old LDA - distance to deduct\n";
                    breakdown[index] = breakdown[index] + "New LDA = " + this.runwayParameters[index][LDA] + " - " + ldaDeduction + "\n";
                }
                recal.setLDA(index, this.runwayParameters[index][LDA] - ldaDeduction);
                breakdown[index] = breakdown[index] + "LDA = " + recal.getLDA(index) + "\n";
                if (recal.getLDA(index) < 0) {
                    breakdown[index] = breakdown[index] + "LDA less than 0 so no space left on runway for LDA\n";
                    recal.setLDA(index, 0);
                }
                breakdown[index] = breakdown[index] + "\n";

                //this part works out LDA for landing towards the obstacle
                //need to toggle index
                if (index == 0) {
                    index = 1;
                } else {
                    index = 0;
                }

                breakdown[index] = breakdown[index] + "Calculation of LDA when landing towards the obstacle:\n";
                breakdown[index] = breakdown[index] + "Use a fixed RESA of 240\n";
                breakdown[index] = breakdown[index] + "New LDA = obstacle distance from this threshold - RESA - Strip End\n";
                breakdown[index] = breakdown[index] + "New LDA = " + distanceFromOtherThreshold + " - 240 - 60\n";
                recal.setLandingTowardsLength(240); //use a fixed RESA
                recal.setLDA(index, distanceFromOtherThreshold - 240 - 60);
                breakdown[index] = breakdown[index] + "LDA = " + recal.getLDA(index) + "\n";
                if (recal.getLDA(index) < 0) {
                    breakdown[index] = breakdown[index] + "LDA less than 0 so no space left on runway for LDA\n";
                    recal.setLDA(index, 0);
                }
                breakdown[index] = breakdown[index] + "\n";
                //this part works out parameters for taking off over obstacle
                //choose biggest out of slope and RESA like before but no need to consider blast
                breakdown[index] = breakdown[index] + "Calculations of TORA, TODA and ASDA when taking off over the obstacle:\n";
                breakdown[index] = breakdown[index] + "1 in 50 Slope = 50 x Obstacle Height\n";
                breakdown[index] = breakdown[index] + "Slope = 50 x " + this.obstacle.getHeight() + "\n";
                breakdown[index] = breakdown[index] + "Slope = " + slopeLength + "\n";
                if (slopeLength > 240) {
                    usedLength = slopeLength;
                    recal.setTakeoffOver(RecalculatedRunway.DIST_TYPE.SLOPE);
                    breakdown[index] = breakdown[index] + "Slope length is greater than a RESA of 240 so use slope length\n";
                } else {
                    usedLength = 240;
                    recal.setTakeoffOver(RecalculatedRunway.DIST_TYPE.RESA);
                    breakdown[index] = breakdown[index] + "RESA of 240 is greater than slope length so use RESA\n";
                }
                recal.setTakeoffOverLength(usedLength);
                //now work out the new TORA
                breakdown[index] = breakdown[index] + "New TORA = obstacle distance from this threshold + this displaced threshold - " + usedLength + " - Strip End\n";
                breakdown[index] = breakdown[index] + "New TORA = " + distanceFromOtherThreshold + " + " + this.runwayParameters[takeOffOverThresh][DISP_THRESH] + " - " + usedLength + " -60\n";
                recal.setTORA(index, distanceFromOtherThreshold + this.runwayParameters[takeOffOverThresh][DISP_THRESH] - usedLength - 60);
                breakdown[index] = breakdown[index] + "TORA = " + recal.getTORA(index) + "\n";
                breakdown[index] = breakdown[index] + "TORA = ASDA = TODA when taking off over obstacle\n";
                if (recal.getTORA(index) < 0) {
                    breakdown[index] = breakdown[index] + "TORA less than 0 so no space left on runway for TORA, ASDA and TODA\n";
                    recal.setTORA(index, 0);
                    recal.setASDA(index, 0);
                    recal.setTODA(index, 0);
                } else {
                    breakdown[index] = breakdown[index] + "ASDA = " + recal.getTORA(index) + "\n";
                    breakdown[index] = breakdown[index] + "TODA = " + recal.getTORA(index) + "\n";
                    //ASDA and TODA will also = this TORA
                    recal.setASDA(index, recal.getTORA(index));
                    recal.setTODA(index, recal.getTORA(index));
                }
                recal.setFirstRecalculation(breakdown[0]);
                recal.setSecondRecalculation(breakdown[1]);
            } else {
                recal.setObstacleType(RecalculatedRunway.OBS_TYPE.NO_EFFECT);
            }
        } else {
            recal.setObstacleType(RecalculatedRunway.OBS_TYPE.NO_OBSTACLE);
        }
        return recal;
    }

    public void setObstacle(Obstacle o) {
        this.obstacle = o;
        this.obstacleOnRunway = true;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String n) {
        this.firstName = n;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String n) {
        this.secondName = n;
    }

    public int getfTORA() {
        return this.runwayParameters[FIRST][TORA];
    }

    public void setfTORA(int a) {
        this.runwayParameters[FIRST][TORA] = a;
    }

    public int getfASDA() {
        return this.runwayParameters[FIRST][ASDA];
    }

    public void setfASDA(int a) {
        this.runwayParameters[FIRST][ASDA] = a;
    }

    public int getfTODA() {
        return this.runwayParameters[FIRST][TODA];
    }

    public void setfTODA(int a) {
        this.runwayParameters[FIRST][TODA] = a;
    }

    public int getfLDA() {
        return this.runwayParameters[FIRST][LDA];
    }

    public void setfLDA(int a) {
        this.runwayParameters[FIRST][LDA] = a;
    }

    public int getfStopwayLength() {
        return this.runwayParameters[FIRST][STOPWAY];
    }

    public void setfStopwayLength(int a) {
        this.runwayParameters[FIRST][STOPWAY] = a;
    }

    public int getfClearwayLength() {
        return this.runwayParameters[FIRST][CLEARWAY];
    }

    public void setfClearwayLength(int a) {
        this.runwayParameters[FIRST][CLEARWAY] = a;
    }

    public int getfDisplacedThreshold() {
        return this.runwayParameters[FIRST][DISP_THRESH];
    }

    public void setfDisplacedThreshold(int a) {
        this.runwayParameters[FIRST][DISP_THRESH] = a;
    }

    public int getsTORA() {
        return this.runwayParameters[SECOND][TORA];
    }

    public void setsTORA(int a) {
        this.runwayParameters[SECOND][TORA] = a;
    }

    public int getsASDA() {
        return this.runwayParameters[SECOND][ASDA];
    }

    public void setsASDA(int a) {
        this.runwayParameters[SECOND][ASDA] = a;
    }

    public int getsTODA() {
        return this.runwayParameters[SECOND][TODA];
    }

    public void setsTODA(int a) {
        this.runwayParameters[SECOND][TODA] = a;
    }

    public int getsLDA() {
        return this.runwayParameters[SECOND][LDA];
    }

    public void setsLDA(int a) {
        this.runwayParameters[SECOND][LDA] = a;
    }

    public int getsStopwayLength() {
        return this.runwayParameters[SECOND][STOPWAY];
    }

    public void setsStopwayLength(int a) {
        this.runwayParameters[SECOND][STOPWAY] = a;
    }

    public int getsClearwayLength() {
        return this.runwayParameters[SECOND][CLEARWAY];
    }

    public void setsClearwayLength(int a) {
        this.runwayParameters[SECOND][CLEARWAY] = a;
    }

    public int getsDisplacedThreshold() {
        return this.runwayParameters[SECOND][DISP_THRESH];
    }

    public void setsDisplacedThreshold(int a) {
        this.runwayParameters[SECOND][DISP_THRESH] = a;
    }

    public boolean isObstacleOnRunway() {
        return obstacleOnRunway;
    }

    public Obstacle getObstacle() {
        return obstacle;
    }

    public void removeObstacle() {
        this.obstacle = null;//is this sufficient for removing an obstacle
        obstacleOnRunway = false;
    }

    @Override
    public String toString() {
        return this.getFirstName() + " " + this.getSecondName();
    }
}
