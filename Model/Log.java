package Model;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

//this class will write to a log file all errors which appear
public class Log {

    public static void addMessage(String message) {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Calendar cal = Calendar.getInstance();
            out.println("[" + dateFormat.format(cal.getTime()) + "]::" + message);
        } catch (IOException ex) {
            System.err.println("Cannot write to   log.   message:");
            System.err.println(ex.getMessage());
        }
    }

}
