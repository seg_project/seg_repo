package Model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Connection {

	
	
    public static String HOME_DIRECTORY = "http://e.alarmsyst-bg.com/seg/home";
    private static final String ADD_AIRPORT_DIRECTORY = "http://e.alarmsyst-bg.com/seg/add_airport";
    private static final String DELETE_AIRPORT_DIRECTORY = "http://e.alarmsyst-bg.com/seg/delete_airport";
    private static final String GET_AIRPORTS_DIRECTORY = "http://e.alarmsyst-bg.com/seg/get_airports";
    private static final String REGISTER_DIRECTORY = "http://e.alarmsyst-bg.com/seg/register";
    private static final String LOGIN_DIRECTORY = "http://e.alarmsyst-bg.com/seg/login";

    public static final String RESPONCE_MSG_OK = "200";
    public static final String RESPONSE_MSG_CONNECTION_ERROR = "Connection Error! Check your internet connection!";
    public static final String RESPONCE_MSG_USER_NOT_FOUND = "user_not_found";
    public static final String RESPONSE_MSG_AIRPORT_UPDATED = "airport_updated";

    private static final String SALT = "SEG_PROJECT_GROUP_9";
    
    private boolean logged_in = false;
    private static Connection instance;
    private String user_name = "";
    private String password = "";
    private XStream xstream;

    public XStream getXStream() {
        return this.xstream;
    }

    public static void main(String[] a) {
    	
    	/*
    	
        XStream xstream = new XStream(new DomDriver());
        Airport first = new Airport("Heathrow");
        Airport temp = (Airport) xstream.fromXML(xstream.toXML(first));

        // Airport a = first.
        System.out.println(first.isOnlineAirport());
        Airport second = temp;
        second.setIsOnlineAirport(true);

        System.out.println(first.isOnlineAirport());
        */
    	
    		
				System.out.println(crypt("hello"));
				
			
    }

    public void setUserName(String user) {
        this.user_name = user;
    }

    public void setUserPassword(String pass) {
        this.password = pass;
    }

    public String getUserPassword() {
        if (this.password == "" || this.logged_in == false) {
            System.err
                    .println("You are doin something wrong...You try to get the password, when no one had logged in!");
        }
        return this.password;
    }

    public String getUserName() {
        if (this.password == "" || this.logged_in == false) {
            System.err
                    .println("You are doin something wrong...You try to get the user name, when no one had logged in!");
        }
        return this.user_name;
    }

    private Connection() {
        xstream = new XStream(new DomDriver());
    }

    public void setLoggedIn(boolean userLogedIn) {
        logged_in = userLogedIn;
    }

    public boolean isLoggedIn() {
        return this.logged_in;
    }

    public static Connection getInstance() {
        if (instance == null) {
            instance = new Connection();
        }
        return instance;
    }

    public boolean webPageExist(URL url) {
        try {
            HttpURLConnection huc;
            huc = (HttpURLConnection) url.openConnection();
            huc.setRequestMethod("GET"); // OR huc.setRequestMethod ("HEAD");
            huc.connect();
            return huc.getResponseCode() == 200; // code 200 - connection
            // established
        } catch (IOException e) {
            return false;
        }
    }

    public boolean webPageExist(String url) {
        try {
            return webPageExist(new URL(url));
        } catch (MalformedURLException ex) {
            return false;
        }

    }

    public String RegisterRequest(String userName, String password) {
        try {
        	password = crypt(password);
            URL siteUrl = new URL(REGISTER_DIRECTORY);
            Map<String, String> data = new HashMap<String, String>();
            data.put("user_name", userName);
            data.put("password", password);
            String reply = makeRequest(data, siteUrl);
            if (reply == null) {
                return RESPONSE_MSG_CONNECTION_ERROR;
            }
            return reply;
        } catch (MalformedURLException e) {
            return RESPONSE_MSG_CONNECTION_ERROR;
        }
    }

    public String loginRequest(String userName, String password) {
        try {
        	password = crypt(password);
            URL siteUrl = new URL(LOGIN_DIRECTORY);
            Map<String, String> data = new HashMap<String, String>();
            data.put("user_name", userName);
            data.put("password", password);

            String reply = makeRequest(data, siteUrl);

            if (reply == null) {
                return RESPONSE_MSG_CONNECTION_ERROR;
            }

            if (reply.equals(RESPONCE_MSG_OK)) {
                setUserName(userName);
                setUserPassword(password);
                setLoggedIn(true);
                return RESPONCE_MSG_OK;
            } else {
                return reply;
            }
        } catch (MalformedURLException e) {
            return RESPONSE_MSG_CONNECTION_ERROR;
        }
    }

    public String addAirportRequest(Airport airport) {
        try {
            Airport temp = (Airport) xstream.fromXML(xstream.toXML(airport));

            temp.setIsOnlineAirport(true);

            URL siteUrl = new URL(ADD_AIRPORT_DIRECTORY);
            Map<String, String> data = new HashMap<String, String>();
            data.put("user_name", getUserName());
            data.put("password", getUserPassword());
            data.put("airport_xml", toXML(temp));

            String reply = makeRequest(data, siteUrl);
            if (reply == null) {
                return RESPONSE_MSG_CONNECTION_ERROR;
            }
            return reply;
        } catch (MalformedURLException e) {
            return RESPONSE_MSG_CONNECTION_ERROR;
        }
    }

    public String deleteAirportRequest(Airport airport) {
        try {
            URL siteUrl = new URL(DELETE_AIRPORT_DIRECTORY);
            Map<String, String> data = new HashMap<String, String>();
            data.put("user_name", getUserName());
            data.put("password", getUserPassword());
            data.put("airport_name", airport.getName());

            String reply = makeRequest(data, siteUrl);
            if (reply == null) {
                return RESPONSE_MSG_CONNECTION_ERROR;
            }
            return reply;
        } catch (MalformedURLException e) {
            return RESPONSE_MSG_CONNECTION_ERROR;
        }
    }

    public ArrayList<Airport> getOnlineAirportsReply() {
        try {
            URL siteUrl = new URL(GET_AIRPORTS_DIRECTORY);
            Map<String, String> data = new HashMap<String, String>();
            data.put("user_name", getUserName());
            data.put("password", getUserPassword());

            String reply = makeRequest(data, siteUrl);
            OnlineCollectionAirport collection = (OnlineCollectionAirport) xstream
                    .fromXML(reply);
            ArrayList<Airport> result = collection.getAirports();
            return result;
        } catch (MalformedURLException e) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private String makeRequest(Map<String, String> data, URL siteUrl) {
        try {
            HttpURLConnection conn = (HttpURLConnection) siteUrl
                    .openConnection();
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);

            DataOutputStream out = new DataOutputStream(conn.getOutputStream());
            Set<String> keys = data.keySet();
            Iterator<String> keyIter = keys.iterator();
            String content = "";
            for (int i = 0; keyIter.hasNext(); i++) {
                Object key = keyIter.next();
                if (i != 0) {
                    content += "&";
                }
                content += key + "="
                        + URLEncoder.encode(data.get(key), "UTF-8");
            }
//            System.out.println(content);

            out.writeBytes(content);
            out.flush();
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String line = "";
            StringBuffer result = new StringBuffer();
            while ((line = in.readLine()) != null) {
                result.append(line);
            }
            in.close();
            return result.toString();
        } catch (IOException e) {
            return null;
        }

    }

    public String toXML(Object o) {
        return xstream.toXML(o);
    }
    
    public static String crypt(String str)  {
 	   try {
 	        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
 	        byte[] array = md.digest(str.getBytes("UTF-8"));
 	        StringBuffer sb = new StringBuffer();
 	        for (int i = 0; i < array.length; ++i) {
 	          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
 	       }
 	        return md5(sb.toString()+SALT);
 	    } catch (java.security.NoSuchAlgorithmException | UnsupportedEncodingException e) {
 	    	return str;
 	    }    	    
 	}

    
    public static String md5(String str)  {
    	   try {
    	        java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
    	        byte[] array = md.digest(str.getBytes("UTF-8"));
    	        StringBuffer sb = new StringBuffer();
    	        for (int i = 0; i < array.length; ++i) {
    	          sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
    	       }
    	        return sb.toString();
    	    } catch (java.security.NoSuchAlgorithmException | UnsupportedEncodingException e) {
    	    	return str;
    	    }    	    
    	}

}
