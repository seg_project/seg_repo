package Model;

import java.io.*;

public class ObstacleFactory {

    private static Integer latestID = 0; // set to one not to trigger loadLatestID() 

    protected static void setLatestID(int id) {
        latestID = id;
    }

    public static Obstacle createObstacle(String name, int height,
            int distanceF, int distanceS, int distanceY) {
        if (latestID.equals(0)) {
            latestID = loadLatestID();
        }

        Obstacle obs = new Obstacle(name, height, distanceF, distanceS, distanceY, latestID);
        latestID++;
        saveLatestID();

        return obs;
    }

    public static Obstacle createObstacle(String name, int height,
            int distanceF, int distanceS, int distanceY, Integer id) {
        return new Obstacle(name, height, distanceF, distanceS, distanceY, id);
    }

    private static void saveLatestID() {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter("latestid.txt"));

            bw.write(latestID.toString());

            bw.close();
        } catch (IOException e) {

        }
    }

    private static Integer loadLatestID() {
        Integer returnValue = null;

        try {
            BufferedReader br = new BufferedReader(new FileReader("latestid.txt"));

            String line = br.readLine();
            returnValue = Integer.valueOf(line);

            br.close();
        } catch (IOException e) {

        }

        return returnValue;
    }
}
