package Model;

import Exceptions.NotLoggedInException;
import View.ToastMessage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class MainModel {

    private boolean autamatically_added_airports_from_xml = false;
    private boolean autamatically_added_obstacles_from_xml = false;
    private ArrayList<Airport> offline_airports;
    private ArrayList<Airport> online_airports;
    private ArrayList<Obstacle> obstacles;
    private PredefinedSettings settings;
    private Manual manual;

    public MainModel() {
        offline_airports = new ArrayList<>();
        obstacles = new ArrayList<>();
        online_airports = new ArrayList<>();

    }

    public synchronized void setManual(Manual m) {
        this.manual = m;
    }

    public synchronized void setOfflineAirports(ArrayList<Airport> a) {
        this.offline_airports = a;
    }

    public synchronized void setOnlineAirports(ArrayList<Airport> a) {
        this.online_airports = a;
    }

    public synchronized void setObstacles(ArrayList<Obstacle> o) {
        this.obstacles = o;
    }

    public synchronized void setSettings(PredefinedSettings a) {
        this.settings = a;
    }

    public synchronized Manual getManual() {
        if (this.manual == null) {
            this.manual = PredefinedManual.getInstance().initialise();
            System.err.println("Manual not initialised properly");
            return this.manual;
        }
        return this.manual;
    }

    public PredefinedSettings getSettings() {
        return this.settings;
    }

    public void setIsAutomaticallyAddedFromXML(boolean b) {
        this.autamatically_added_airports_from_xml = b;
    }

    public boolean isAutmaticallyAddedFromXML() {
        return this.autamatically_added_airports_from_xml;
    }

    public ArrayList<Airport> getOfflineAirports() {
        return this.offline_airports;
    }

    public ArrayList<Airport> getOnlineAirports() throws NotLoggedInException {
        if (!Connection.getInstance().isLoggedIn()) {
            throw new NotLoggedInException("Not logged in!");
        }
        return this.online_airports;
    }

    public ArrayList<Obstacle> getObstacles() {
        return this.obstacles;
    }

    public void addAirport(Airport a) {
        this.offline_airports.add(a);
    }

    public void addAirports(Collection a) {
        this.offline_airports.addAll(a);
    }

    public void addObstacles(ArrayList<Obstacle> o) {
        for (Obstacle a : o) {
            obstacles.add(a);
        }

    }

    public void addObstacle(Obstacle o) {
        this.obstacles.add(o);
    }

    //find an airport with the same name and replace it with *newAirport*

    public boolean updateOfflineAirport(Airport newAirport) {
        boolean result = false;
        Iterator<Airport> it = this.getOfflineAirports().iterator();
        while (it.hasNext()) {
            Airport old = it.next();
            if (old.getName().equals(newAirport.getName())) {
                old = newAirport;
                return true;
            }

        }
        return result;
    }

    public int getIndexOfOnlineAirport(Airport a) {
        int result = -1;
        try {
            for (Airport b : getOnlineAirports()) {
                if (a.getName().equals(b.getName())) {
                    return getOnlineAirports().indexOf(b);
                }
            }
        } catch (NotLoggedInException ex) {
            Log.addMessage("trying to get the online airports without being logged in");
            System.out.println("trying to get the online airports without being logged in");
        }
        return result;
    }

    public boolean removeAirport(Airport a, boolean removeXMLFileToo) {
        String name = a.getName();
        boolean res = this.offline_airports.remove(a);
        if (removeXMLFileToo) {
            if (!PredefinedData.getInstance().removeDefaultAirport(a)) {

                JOptionPane.showMessageDialog(null, "The XML file of that airport was not found in the default directory(" + new File(PredefinedData.default_airports_directory).getAbsolutePath() + ")\n"
                        + "). Hence, this airport won't be loaded when the program starts next time because it is not in the default directory.");
            } else {
//                file removed from default directory
                new ToastMessage(name + " removed from the default directory", false);
            }
        }
        return res;
    }

    public boolean removeObstacle(Obstacle a, boolean removeXMLFileToo) {
        String name = a.getName();
        if (removeXMLFileToo) {
            PredefinedData.getInstance().removeDefaultObstacle(a);
            if (!PredefinedData.getInstance().removeDefaultObstacle(a)) {

                JOptionPane.showMessageDialog(null, "The XML file of that obstacle was not found in the default directory(" + new File(PredefinedData.default_obstacles_directory).getAbsolutePath() + ")\n"
                        + "). This file won't be loaded when the program starts next time because it is not in the default directory.");
            } else {
//                file removed from default directory
                new ToastMessage(name + " removed from the default directory", false);
            }
        }
        return this.obstacles.remove(a);
    }

}
