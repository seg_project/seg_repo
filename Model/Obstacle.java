package Model;

public class Obstacle {

    private String obstacle_name; //The name of he object - not unique
    private int height; //The height of the object that will affect the displaced theshold

    private int distanceFirstThreshold; //Distance from the first displaced threshold
    private int distanceSecondThreshold; //Distance from second threshold
        /* (negative values denote obstacle is before threshold
     * positive threshold means obstacle is after the threshold) */

    private int distanceY; //Distance from the centerline (positive = obstacle above, negative = obstacle below)

    private Integer id; //Unique ID of obstacle used in XML
    public static int DUMMY_ID = -1;

    public Obstacle(String name, int height, int distanceF, int distanceS, int distanceY, Integer id) {
        this.obstacle_name = name;
        this.height = height;
        this.id = id;

        this.distanceFirstThreshold = distanceF;
        this.distanceSecondThreshold = distanceS;
        this.distanceY = distanceY;
    }

    //Set where the obstacle is on the runway
    public void setDistance(int first, int second, int y) {
        distanceFirstThreshold = first;
        distanceSecondThreshold = second;
        distanceY = y;
    }

    public String getName() {
        return obstacle_name;
    }

    public int getHeight() {
        return height;
    }

    public int getDistanceFirst() {
        return distanceFirstThreshold;
    }

    public int getDistanceSecond() {
        return distanceSecondThreshold;
    }

    public int getDistanceY() {
        return distanceY;
    }

    public Integer getID() {
        return id;
    }

    @Override
    public String toString() {
        return getName();
    }
}
