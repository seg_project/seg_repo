/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Model;

import Controller.MainController;
import javax.swing.DefaultListModel;
import javax.swing.JList;

public class CustomListModel<T> extends DefaultListModel<T> {

    private JList<T> list;

    public CustomListModel(JList<T> l) {
        super();
        this.list = l;
        list.setModel(this);
        MainController.setFirstElementSelected(l);

    }

    public void addElement(T a, int index) {
        super.addElement(a);
        list.setSelectedIndex(index);
//        MainController.setFirstElementSelected(this.list);
    }

}
