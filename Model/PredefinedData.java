package Model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class PredefinedData {

    private static PredefinedData instance = null;
    private ArrayList<Obstacle> obstacleList = null;
    private XStream xstream = null;
    public static String default_airports_directory = "./predefined/airports";
    public static String default_obstacles_directory = "./predefined/obstacles";
    public static String default_settings_directory = "./predefined/settings";

    //get
    //Initially the 'get' methods will just return the static lists
    //later those methods will be responsible for reading the XML files
    //and including them in the result.
    protected PredefinedData() {
        xstream = new XStream(new DomDriver()); //initialise the XML reader/writer
        obstacleList = new ArrayList<>();
    }

    public XStream getX() {
        return this.xstream;
    }

    public static PredefinedData getInstance() {
        if (instance == null) {
            instance = new PredefinedData();
//            System.out.println(getX());
        }
        return instance;
    }

    public void createDefaultDirectories() {
        File airports = new File(PredefinedData.default_airports_directory);
        if (!airports.isDirectory()) {
            airports.mkdirs();
        }
        File obstacles = new File(PredefinedData.default_obstacles_directory);
        if (!obstacles.isDirectory()) {
            obstacles.mkdirs();
        }

    }

    public final ArrayList<Airport> getAirportList(boolean allowMultiply) {
        FileDialog fd = new FileDialog(new Frame(), "Choose a file", FileDialog.LOAD);
        fd.setLocationRelativeTo(null);
        fd.setMultipleMode(allowMultiply);
        fd.setDirectory(new File(default_airports_directory).getAbsolutePath());
        fd.setFile("*.xml");
        fd.pack();
        fd.setVisible(true);
        ArrayList<Airport> result = new ArrayList<>();

        File[] res = fd.getFiles();
//        System.err.println(res.length + " lenthg awt");
        if (res.length == 0) {
            Log.addMessage("No file selected while saving obstacle. Usually caused by the user pressing the Cancel button");
            return null;
        }
        try {
            for (File f : res) {
                System.out.println("Airport's path: " + f.getPath());
                Airport temp = (Airport) this.xstream.fromXML(f);
                result.add(temp);
            }
            if (result.isEmpty()) {
                return null;
            }
            return result;
        } catch (Exception e) {
            System.err.println(e);
            return null;
        }
    }

    public final ArrayList<Obstacle> getObstacleList() {
        FileDialog fd = new FileDialog(new Frame(), "Choose a file/s", FileDialog.LOAD);
        fd.setMultipleMode(true);
        fd.setDirectory(new File(default_obstacles_directory).getAbsolutePath());

        fd.setFile("*.xml");
        fd.setVisible(true);
        ArrayList<Obstacle> result = new ArrayList<>();

        File[] res = fd.getFiles();
        if (res.length == 0) {
            Log.addMessage("No file selected while loading obstacle. Usually caused by the user pressing the Cancel button");

            return null;
        }
        try {
            for (File f : res) {
                System.out.println("Obstacle's path " + f.getPath());
                Obstacle temp = (Obstacle) this.xstream.fromXML(f);
                result.add(temp);
            }
            return result;
        } catch (Exception e) {
            Log.addMessage(e.getMessage());
            return null;
        }
    }

    //
    //add
    /* if savingEditedObstacle==true then we try to find the obstacle, 
     delete it and save the edited one at the same place  
     also, it is assumed that no to obstacles have same id's and are in the same folder
     */
    public boolean saveDefaultObstacle(Obstacle o, boolean promptForName) {
        File f;
        File direcotry = new File(PredefinedData.default_obstacles_directory);

        if (!promptForName && direcotry.isDirectory()) {
            // always keep one
            File[] fls = direcotry.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isFile() && file.getName().contains(".xml");
                }
            });
//            if (fls.length != 0) {
            for (File file : fls) {
                try {
                    Obstacle temp = (Obstacle) xstream.fromXML(file);
                    if (temp.getID().equals(o.getID())) {
                        System.out.println("deleting" + file.getCanonicalPath());
                        file.delete(); // clean the old obstacle
                    }
                } catch (Exception e) { //this would be caused if we try to read a xml which is not PredefinedSettings object
                }
//                }
            }
            f = new File(PredefinedData.default_obstacles_directory + "/" + o.getID() + "_" + o.getName() + ".xml");
        } else {
            FileDialog fd = new FileDialog(new Frame(), "Save as...", FileDialog.SAVE);
            fd.setMultipleMode(false);
            fd.setDirectory(new File(default_obstacles_directory).getAbsolutePath());
            fd.setFile(o.getID() + "_" + o.getName() + ".xml");
            fd.setVisible(true);
            File[] fls = fd.getFiles();
            if (fls.length == 0) {
                Log.addMessage("No file selected while saving obstacle. Usually caused by the user pressing the Cancel button");
                return false;
            }
            f = fd.getFiles()[0];
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f, false);
            PrintWriter out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(fos, "UTF-8")));
            // do what you want to do
            xstream.toXML(o, fos);
            out.flush();
            out.close();
            return true;
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Log.addMessage(ex.getLocalizedMessage());
            return false;
        } finally {
            // quietly close the FileOutputStream (see Jakarta Commons IOUtils)
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Log.addMessage(ex.getLocalizedMessage());
                }
            }
        }

    }

    public boolean savePredefinedSettings(PredefinedSettings settings, boolean promptForName) {
        File toBeWritten;
        File direcotry = new File(PredefinedData.default_settings_directory);
        if (!direcotry.isDirectory()) {
            direcotry.mkdirs();
        }
        if ((!promptForName) && direcotry.isDirectory()) {
//            if () { // always keep one
            File[] f = direcotry.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isFile() && file.getName().contains(".xml");
                }
            });
            if (f.length != 0) {
                for (File file : f) {
                    try {
                        PredefinedSettings temp = (PredefinedSettings) xstream.fromXML(file);
                        if (temp.getName().equals(settings.getName())) {
                            file.delete(); // clean the old settings
                        }
                    } catch (Exception e) { //this would be caused if we try to read a xml which is not PredefinedSettings object
                    }
                }
            }
            toBeWritten = new File(PredefinedData.default_settings_directory + "/" + settings.getName() + ".xml");

//            }
        } else {
//            toBeWritten = new File(PredefinedData.default_settings_directory + "/" + settings.getName() + ".xml");
            FileDialog fd = new FileDialog(new Frame(), "Choose a file/s", FileDialog.SAVE);
            fd.setMultipleMode(false);
            fd.setFile(settings.getName() + ".xml");
            fd.setVisible(true);
            File[] res = fd.getFiles();
            if (res.length == 0) {
                return false;
            }
            toBeWritten = res[0];
        }
        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(toBeWritten, false);

            PrintWriter out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(fos, "UTF-8")));
            xstream.toXML(settings, fos);
            out.flush();
            out.close();
            return true;
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Log.addMessage(ex.getLocalizedMessage());
            return false;
        } finally {
            // quietly close the FileOutputStream (see Jakarta Commons IOUtils)
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Log.addMessage(ex.getLocalizedMessage());
                }
            }
        }
    }

    public boolean saveDefaultAirport(Airport a, boolean promptForName) {
        if (a == null) {
            return false;
        }
        File toWriteTo = null;
        File directory = new File(PredefinedData.default_airports_directory);
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        if (!promptForName && directory.isDirectory()) {
            toWriteTo = new File(PredefinedData.default_airports_directory + "/" + a.getName() + ".xml");
        } else {
            FileDialog fd = new FileDialog(new Frame(), "Choose a file/s", FileDialog.SAVE);
            fd.setMultipleMode(false);
            fd.setDirectory(new File(default_airports_directory).getAbsolutePath());
            fd.setFile(a.getName() + ".xml");
            fd.setVisible(true);
            File[] files = fd.getFiles();
            if (files.length != 1) {
                return false;
            }
            toWriteTo = fd.getFiles()[0];
        }
        FileOutputStream fos = null;
        try {
//            toWriteTo = new File(PredefinedData.default_airports_directory + "/" + a.getName() + ".xml");
            fos = new FileOutputStream(toWriteTo, false);
            PrintWriter out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(fos, "UTF-8")));
            xstream.toXML(a, fos);
            out.flush();
            out.close();
            Log.addMessage("Airport exported as XML:" + a.getName());
            return true;
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Log.addMessage(ex.getLocalizedMessage());
            return false;
        } finally {
            // quietly close the FileOutputStream (see Jakarta Commons IOUtils)
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Log.addMessage(ex.getLocalizedMessage());
                }
            }
        }
    }
    //
    //remove

    public boolean removeDefaultObstacle(Obstacle ref) {

        boolean result = false;
        File directory = new File(PredefinedData.default_obstacles_directory);
        if (directory.isDirectory()) {
            File[] files = directory.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isFile() && file.getName().contains(".xml");
                }
            });
            File toBeDeleted = null;
            for (File f : files) {
                try {
                    Obstacle a = (Obstacle) xstream.fromXML(f);
                    if (a.getID().equals(ref.getID())) {
                        toBeDeleted = f;
                    }
                } catch (Exception ex) {
                }
            }
            if (toBeDeleted == null) {
                JOptionPane.showMessageDialog(null, "The obstacle's XML file was not found in the default obstacle directory.\n"
                        + PredefinedData.default_obstacles_directory + "\nDelete the file ");
            }
            if (toBeDeleted != null) { // the airprot file WAS in the folder
                result = toBeDeleted.delete();
            } else {
                result = false;
            }
        }
        return result;
    }

    //this method will try to remove airport xml file from the default 
    //predefined/airport directory.
    //if the airport is not in there this method will return with false
    //asking the user to delete the file manually
    public boolean removeDefaultAirport(Airport ref) {
        boolean result = false;
        File directory = new File(PredefinedData.default_airports_directory);
        if (directory.isDirectory()) {
            File[] files = directory.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isFile() && file.getName().contains(".xml");
                }
            });
            File toBeDeleted = null;
            for (File f : files) {
                try {
                    Airport a = (Airport) xstream.fromXML(f);
                    if (a.getName().equals(ref.getName())) {
                        toBeDeleted = f;
                    }
                } catch (Exception ex) {
                }
            }
            if (toBeDeleted != null) { // the airprot file WAS in the folder
                result = toBeDeleted.delete();
            } else {
                result = false;
            }
        }
        return result;
    }

    //
    public ArrayList<Airport> onLoadCheckForAirports() {
        ArrayList<Airport> result = new ArrayList<>();

        File directory = new File(PredefinedData.default_airports_directory);
        if (directory.isDirectory()) {
            File[] files = directory.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isFile() && file.getName().contains(".xml");
                }
            });
            for (File f : files) {
                try {
                    Airport a = (Airport) xstream.fromXML(f);
                    result.add(a);
                } catch (Exception ex) {

                }
            }
        }
        if (result.isEmpty()) {
            Airport ar = new Airport("Heathrow");
            Runway r = new Runway("09L", "27R", 3902, 3902, 3902, 3595, 306, 3884, 3962, 3884, 3884, 0);
            ar.addRunway(r);
            this.saveDefaultAirport(ar, false);
            result.add(ar);
        }
        return result;
    }

    public ArrayList<Obstacle> onLoadCheckForObstacles() {
        int last_index = 0;
        ArrayList<Obstacle> result = new ArrayList<>();
        File directory = new File(PredefinedData.default_obstacles_directory);
        if (directory.isDirectory()) {
            File[] files = directory.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isFile() && file.getName().contains(".xml");
                }
            });
            for (File f : files) {
                try {
                    Obstacle a = (Obstacle) xstream.fromXML(f);
                    result.add(a);
                    if (a.getID() > last_index) {
                        last_index = a.getID();
                    }
                } catch (Exception ex) {
                    System.err.println(ex);
                }
            }
            ObstacleFactory.setLatestID(last_index + 1);
        }
        if (result.isEmpty()) {
            Obstacle ob1 = ObstacleFactory.createObstacle("Boeing 767", 17, 0, 3353, 0);
            Obstacle ob2 = ObstacleFactory.createObstacle("Boeing 747", 20, 0, 3353, 0);
            Obstacle ob3 = ObstacleFactory.createObstacle("Boeing 737", 13, 0, 3353, 0);
            Obstacle ob4 = ObstacleFactory.createObstacle("Boeing 777", 19, 0, 3353, 0);
            Obstacle ob5 = ObstacleFactory.createObstacle("Boeing 787", 17, 0, 3353, 0);
            result.add(ob1);
            result.add(ob2);
            result.add(ob3);
            result.add(ob4);
            result.add(ob5);
            this.saveDefaultObstacle(ob5, false);
            this.saveDefaultObstacle(ob4, false);
            this.saveDefaultObstacle(ob3, false);
            this.saveDefaultObstacle(ob2, false);
            this.saveDefaultObstacle(ob1, false);
        }
        return result;

    }

    public PredefinedSettings onLoadCheckForSettings() {
        PredefinedSettings result = null;
        File directory = new File(PredefinedData.default_settings_directory);
        if (directory.isDirectory()) {
            File[] files = directory.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    return file.isFile() && file.getName().contains(".xml");
                }
            });
            for (File f : files) {
                try {
                    PredefinedSettings a = (PredefinedSettings) xstream.fromXML(f);
                    result = a;
                    break; //load only one settings file
                } catch (Exception ex) {

                }
            }
        }
        return result;
    }
}
