package Model;

import java.util.ArrayList;
import java.util.List;

public class Airport implements Comparable<Airport> {

    //The list of all runways in this airport
    private ArrayList<Runway> runwayList;
    private String airport_name;
    private boolean onlineAirport = false;

    //A new airport contains no runways (empty list)
    public Airport(String name) {
        this.airport_name = name;
        this.runwayList = new ArrayList<>();
    }

    public String getName() {
        return this.airport_name;
    }

    public void setIsOnlineAirport(boolean online) {
        this.onlineAirport = online;
    }

    public boolean isOnlineAirport() {
        return this.onlineAirport;
    }

    //Used when loading from XML file, construct a list of the runways and set that as the list
    public void setRunwayList(List<Runway> newRunwayList) {
        runwayList = new ArrayList<>(newRunwayList);
    }

    //Add all runways, used when importing from XML file mid-program
    public void addRunwayList(List<Runway> newRunwayList) {
        runwayList.addAll(newRunwayList);
    }

    //Get a runway based on its name(threshold indicators)
    public Runway getRunway(String name) {
        Runway returnRunway = null;

        for (Runway r : runwayList) {
            String first = r.getFirstName();
            String last = r.getSecondName();
            String totalName = first + "/" + last;
            if (totalName.equals(name)) {
                returnRunway = r;
            }
        }
        return returnRunway;
    }

    public ArrayList<Runway> getRunways() {
        return this.runwayList;
    }

    //Add a new runway to the list
    public void addRunway(Runway runwayToAdd) {
        runwayList.add(runwayToAdd);
    }

    public boolean removeRunway(Runway r) {
        return this.runwayList.remove(r); //true if the collection contained the elelement
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public int compareTo(Airport t) {
        return this.airport_name.compareTo(t.airport_name);
    }
}
