/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Model;

import java.awt.Color;

public class PredefinedSettings {

    private String name;
    private Color runway_concrete;
    private Color back_ground;
    private Color CGA;
    private Color clearway;
    private Color stopway;
    private Color arrow;
    private Color other;

    //default settings which will be used if we load the app for the first time or
    //we didn't find the setting.xml in the default directory
    private Color default_runway_concrete = Color.GRAY;
    private Color default_back_ground = Color.WHITE;
    private Color default_CGA = Color.RED;
    private Color default_clearway = Color.BLUE;
    private Color default_stopway = Color.GREEN;
    private Color default_arrow = Color.ORANGE;
    private Color default_other = Color.BLACK;

    public PredefinedSettings(String name) {
        this.name = name;

        other = default_other;
        runway_concrete = default_runway_concrete;
        back_ground = default_back_ground;
        CGA = default_CGA;
        clearway = default_clearway;
        stopway = default_stopway;
        arrow = default_arrow;
    }

    public String getName() {
        return this.name;
    }

    public Color getBack_ground() {
        return back_ground;
    }

    public void setBack_ground(Color back_ground) {
        this.back_ground = back_ground;
    }

    public Color getCGA() {
        return CGA;
    }

    public void setCGA(Color CGA) {
        this.CGA = CGA;
    }

    public Color getClearway() {
        return clearway;
    }

    public void setClearway(Color clearway) {
        this.clearway = clearway;
    }

    public Color getStopway() {
        return stopway;
    }

    public void setStopway(Color stopway) {
        this.stopway = stopway;
    }

    public Color getArrow() {
        return arrow;
    }

    public void setArrow(Color arrow) {
        this.arrow = arrow;
    }

    public Color getOther() {
        return other;
    }

    public void setOther(Color other) {
        this.other = other;
    }

    public Color getRunwayColor() {
        return this.runway_concrete;
    }

    public void setRunwayConcrete(Color c) {
        //set the color in the object and write it to the XML for future use
        this.runway_concrete = c;
    }

    public boolean saveSettings() {
        return PredefinedData.getInstance().savePredefinedSettings(this, false);
    }

    public boolean exportSettings() {
        return PredefinedData.getInstance().savePredefinedSettings(this, true);
    }
}
