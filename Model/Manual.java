/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Model;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Manual {

	public final static String startScreenURL = "http://e.alarmsyst-bg.com/seg/manual";
	public final static String airportViewURL = "http://e.alarmsyst-bg.com/seg/manual/airportview";
	public final static String introductionURL = "http://e.alarmsyst-bg.com/seg/manual/introduction";
	public final static String mainViewURL = "http://e.alarmsyst-bg.com/seg/manual/mainview";

	String airport_view = "";
	String introduction = "";
	String main_view = "";
	String start_screen = "";

	boolean tryXML = false; // this would be true if retrieving the HTML has
							// failed and we will try to read xml

	// this method is passed an URL and "safely" returns the HTML content of the
	// URL as a String
	// "safety" refers to the fact that the method will return "" if the
	// retrieving doesn't complete in more than 1 second
	// this is done because if the server which we try to access is down, the
	// method will block for undefined period of time
	// therefore we use the ExecutorService to limit the time the method can try
	// to get the html.
	public final String readURL(final String S) {
		String result = "";
		Callable<Object> complicatedCalculation = new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				// this is the code which actually attempts to retrieve the HTML
				String result = "";
				try {
					URL u = new URL(S);
					URLConnection yc = u.openConnection();
					BufferedReader in;
					try {
						in = new BufferedReader(new InputStreamReader(
								yc.getInputStream()));
						String inputLine;
						while ((inputLine = in.readLine()) != null) {
							result = result + inputLine;
						}
						in.close();
					} catch (IOException ex) {
						return "";
					}
				} catch (MalformedURLException ex) {
					return "";
				} catch (IOException ex) {
					return "";
				}
				return result;
			}
		};
		final ExecutorService service = Executors.newSingleThreadExecutor();
		try {
			final Future<Object> f = service.submit(complicatedCalculation); // this
																				// attempts
																				// to
																				// retrieve
																				// the
																				// HTML
																				// for
																				// no
																				// more
																				// than
																				// 1
																				// second
																				// time
																				// limit.
			return (String) f.get(2, TimeUnit.SECONDS);
			// f.;
			// System.out.println(f.get(2, TimeUnit.SECONDS));
		} catch (final TimeoutException e) {
			System.err.println("Retrieving HTML took too long");
		} catch (final Exception e) {
			System.err.println(e);
		} finally {
			service.shutdown();
		}
		return result;

	}

	public Manual() {
		airport_view = readURL("http://seg_project.bitbucket.org/Airportview.html");
		if (airport_view == "") {
			tryXML = true;
			return;
		}
		introduction = readURL("http://seg_project.bitbucket.org/Introduction.html");
		if (introduction == "") {
			tryXML = true;
			return;
		}
		main_view = readURL("http://seg_project.bitbucket.org/MainView.html");
		if (main_view == "") {
			tryXML = true;
			return;
		}
		start_screen = readURL("http://seg_project.bitbucket.org/Startscreen.html");
		if (start_screen == "") {
			tryXML = true;
		}

	}

	public String getAirportView() {
		return airport_view;
	}

	public String getIntroduction() {
		return this.introduction;
	}

	public String getMainView() {
		return this.main_view;
	}

	public String getStartScreen() {
		return this.start_screen;
	}

	public void setAirportView(String a) {
		this.airport_view = a;
	}

	public void setIntroduction(String s) {
		this.introduction = s;
	}

	public void setMainView(String s) {
		this.main_view = s;
	}

	public void setStartScreen(String s) {
		this.start_screen = s;
	}

	public static void openInBowser(String strUrl) {

		if (Desktop.isDesktopSupported()) {
			Desktop desktop = Desktop.getDesktop();
			try {
				desktop.browse(new URI(strUrl));
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		} else {
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec("xdg-open " + strUrl);
			} catch (IOException e) {
				e.printStackTrace();

			}
		}
	}

}
