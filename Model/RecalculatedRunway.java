package Model;

public class RecalculatedRunway {

    public enum OBS_TYPE {

        NO_OBSTACLE, NO_EFFECT, OBSTACLE_AT_FIRST, OBSTACLE_AT_SECOND;
    }

    public enum DIST_TYPE {

        RESA, SLOPE, BLAST;
    }

    private String firstRecalculation;
    private String secondRecalculation;

    private OBS_TYPE obstacleType;

    private final int FIRST = 0;
    private final int SECOND = 1;
    private final int TORA = 0;
    private final int ASDA = 1;
    private final int TODA = 2;
    private final int LDA = 3;

    private int[][] runwayParameters = new int[2][4];

    private DIST_TYPE landOver;
    private int landOverLength;
    /*
     * when landing over the buffer distance between the obstacle and start of
     * LDA is either blast, slope or RESA (which ever is largest). RESA and slope
     * will also have a strip end
     */

    private DIST_TYPE takeoffOver;
    private int takeoffOverLength;
    /*
     * when taking off over an obstacle the buffer distance between the obstacle
     * and end of TORA, TODA and ASDA is either slope or RESA (which ever is largest)
     * RESA and slope will also have a strip end.
     */

    private int landingTowardsLength;
    /*
     * when landing towards an obstacle the buffer distance between the obstacle
     * and the end of LDA will always be the RESA length (plus the strip end)
     */

    int blast;

    private DIST_TYPE takeOffAway;
    private int takeOffAwayLength;
    /*
     * when taking off away from the obstacle it might be the blast distance or RESA used 
     * as a buffer between the obstacle and start of TORA, TODA and ASDA (which ever is largest)
     */
    private String firstName;
    private String secondName;

    public String getFirstName() {
        return this.firstName;
    }

    public String getSecondName() {
        return this.secondName;
    }

    public void setFirstName(String s) {
        this.firstName = s;
    }

    public void setSecondName(String s) {
        this.secondName = s;
    }

    public void setTORA(int index, int value) {
        this.runwayParameters[index][TORA] = value;
    }

    public void setTODA(int index, int value) {
        this.runwayParameters[index][TODA] = value;
    }

    public void setASDA(int index, int value) {
        this.runwayParameters[index][ASDA] = value;
    }

    public void setLDA(int index, int value) {
        this.runwayParameters[index][LDA] = value;
    }

    public int getTORA(int index) {
        return this.runwayParameters[index][TORA];
    }

    public int getTODA(int index) {
        return this.runwayParameters[index][TODA];
    }

    public int getASDA(int index) {
        return this.runwayParameters[index][ASDA];
    }

    public int getLDA(int index) {
        return this.runwayParameters[index][LDA];
    }

    public int getfTORA() {
        return this.runwayParameters[FIRST][TORA];
    }

    public int getfASDA() {
        return this.runwayParameters[FIRST][ASDA];
    }

    public int getfTODA() {
        return this.runwayParameters[FIRST][TODA];
    }

    public int getfLDA() {
        return this.runwayParameters[FIRST][LDA];
    }

    public int getsTORA() {
        return this.runwayParameters[SECOND][TORA];
    }

    public int getsASDA() {
        return this.runwayParameters[SECOND][ASDA];
    }

    public int getsTODA() {
        return this.runwayParameters[SECOND][TODA];
    }

    public int getsLDA() {
        return this.runwayParameters[SECOND][LDA];
    }

    public void setfTORA(int t) {
        this.runwayParameters[FIRST][TORA] = t;
    }

    public void setfASDA(int a) {
        this.runwayParameters[FIRST][ASDA] = a;
    }

    public void setfTODA(int t) {
        this.runwayParameters[FIRST][TODA] = t;
    }

    public void setfLDA(int l) {
        this.runwayParameters[FIRST][LDA] = l;
    }

    public void setsTORA(int t) {
        this.runwayParameters[SECOND][TORA] = t;
    }

    public void setsASDA(int a) {
        this.runwayParameters[SECOND][ASDA] = a;
    }

    public void setsTODA(int t) {
        this.runwayParameters[SECOND][TODA] = t;
    }

    public void setsLDA(int l) {
        this.runwayParameters[SECOND][LDA] = l;
    }

    public String getSecondRecalculation() {
        return secondRecalculation;
    }

    public void setSecondRecalculation(String secondRecalculation) {
        this.secondRecalculation = secondRecalculation;
    }

    public String getFirstRecalculation() {
        return firstRecalculation;
    }

    public void setFirstRecalculation(String recalculation) {
        this.firstRecalculation = recalculation;
    }

    public OBS_TYPE getObstacleType() {
        return obstacleType;
    }

    public DIST_TYPE getLandOver() {
        return landOver;
    }

    public int getLandOverLength() {
        return landOverLength;
    }

    public DIST_TYPE getTakeoffOver() {
        return takeoffOver;
    }

    public int getTakeoffOverLength() {
        return takeoffOverLength;
    }

    public int getLandingTowardsLength() {
        return landingTowardsLength;
    }

    public DIST_TYPE getTakeOffAway() {
        return this.takeOffAway;
    }

    public int getTakeOffAwayLength() {
        return this.takeOffAwayLength;
    }

    public int getBlast() {
        return blast;
    }

    public void setObstacleType(OBS_TYPE obstacleType) {
        this.obstacleType = obstacleType;
    }

    public void setLandOver(DIST_TYPE landOver) {
        this.landOver = landOver;
    }

    public void setLandOverLength(int landOverLength) {
        this.landOverLength = landOverLength;
    }

    public void setTakeoffOver(DIST_TYPE takeoffOver) {
        this.takeoffOver = takeoffOver;
    }

    public void setTakeoffOverLength(int takeoffOverLength) {
        this.takeoffOverLength = takeoffOverLength;
    }

    public void setLandingTowardsLength(int landingTowardsLength) {
        this.landingTowardsLength = landingTowardsLength;
    }

    public void setTakeOffAway(DIST_TYPE takeOffAway) {
        this.takeOffAway = takeOffAway;
    }

    public void setTakeOffAwayLength(int takeOffAwayLength) {
        this.takeOffAwayLength = takeOffAwayLength;
    }

    public void setBlast(int blast) {
        this.blast = blast;
    }
}
