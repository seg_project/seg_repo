/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Model;

//this class is respnsible for reading/qriting the manual xml files and load them
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

//in the model
//it tries to load the manual xml file from the default folder
//if nothing is find, load the default manual.
public class PredefinedManual {

    public static void main(String[] a) {
        try {
            PredefinedManual.getInstance().testXML();
        } catch (IOException ex) {
            Logger.getLogger(PredefinedManual.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    private Manual manual;
    private static PredefinedManual instance;
    private XStream xstream;
    public static String default_manual_directory = "./predefined/manual";

    private PredefinedManual() {
        xstream = new XStream(new DomDriver());
    }
    static String default_a = "<html><head><title>Airport view</title></head><body class=\"c3\"><p class=\"c1\"><span>When you have selected an airport, you are presented with a list of runways.</span></p><p class=\"c0\"><span></span></p><p class=\"c1\"><span class=\"c4\">Runways</span></p><p class=\"c1\"><span>There are 3 options you can do with existing runways, but </span><span class=\"c2\">please note </span><span>that you must highlight one first to do anything with them.</span></p><p class=\"c1\"><span class=\"c2\">Deletion</span><span>&nbsp;is self explanatory, it deletes the runway from the system entirely. You cannot retrieve the runway once it&rsquo;s deleted.</span></p><p class=\"c1\"><span class=\"c2\">Edit</span><span>&nbsp;lets you change some of the default parameters of the runway, such as its name and thresholds.</span></p><p class=\"c1\"><span class=\"c2\">View</span><span>&nbsp;takes you the main screen where the visualisation of the runway is shown, along with the option of adding/removing obstacles.</span></p><p class=\"c0\"><span></span></p><p class=\"c1\"><span>An option to </span><span class=\"c2\">add</span><img src=\"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPEOsFC2tci90_yQTxEfD5gXWOBzI3Rz2wIchkmm2VdNBN57RttD4kiUk\"><span>&nbsp;runways is also present, and allows you to add a completely new runway.</span></p><p class=\"c1\"><span>There are additionally two saving options. One to save to the airport file that it was loaded from, and the second to save to a completely new file.</span></p></body></html>";
    static String default_i = "<html><head><title>Introduction</title></head><body class=\"c6\"><p class=\"c0 c4\"><span class=\"c2\">Runway Check</span></p><p class=\"c0 c5\"><span></span></p><p class=\"c0\"><span class=\"c1\">What is the application?</span></p><p class=\"c0\"><span class=\"c3\">Runway Check</span><span>&nbsp;is a simple to use yet powerful tool to perform the calculation of runway parameters. When obstacles block parts of the runway, an official redeclaration of safe parameters to use must be undertaken.</span></p><p class=\"c5 c0\"><span></span></p><p class=\"c0\"><span class=\"c1\">Who is this application aimed at?</span></p><p class=\"c0\"><span>Primarily, this application is aimed at trainees who are learning the different steps in the process, and all the intricacies is holds. This tool is purely a reference guide and is not intended to replace the manual recalculations. To that end, trainees can use this to cross-check with scenarios they are given to make sure the are doing things correctly.</span></p><p class=\"c0\"><span>Additionally, it is to be used by official re-evaluators for them to cross-check and make sure their calculations line up with the tool, so any errors can be easily spotted and rectified in minimal time.</span></p></body></html>";
    static String default_m = "<html><head><title>Main View </title><body class=\"c4\"><p class=\"c2\"><span class=\"c0\">Runway View is the main view of the program. There is a visual representation of the selected runway in the centre of the screen to give clear understanding of the location of the obstacle and how recalculated runway looks like. Apart from runway itself, runway parameters like TODA, ASDA,TORA,LDA and clearway are also shown on alongside with the values they correspond to.</span></p><p class=\"c1\"><span class=\"c0\"></span></p><p class=\"c2\"><span class=\"c0 c3\">Runway name</span></p><p class=\"c2\"><span class=\"c0\">The name of the displayed runway is shown in the top left corner of the application (09L/27R). </span></p><p class=\"c1\"><span class=\"c0\"></span></p><p class=\"c2\"><span class=\"c3 c0\">Changing runway View</span></p><p class=\"c2\"><span class=\"c0\">There are 2 different way of how the runway can be displayed: Top-down and side-on. By default the runway is displayed in the top-down view. The view can be changed to side-on and back by pressing &ldquo;Top-down/side-on&rdquo; button which is located right under the runway name in the top left corner of the program.</span></p><p class=\"c1\"><span class=\"c0\"></span></p><p class=\"c2\"><span class=\"c3 c0\">Threshold</span><span class=\"c0\">&nbsp;- Threshold can be changed by clicking the radio buttons that the located on the top central side of the application labeled with the threshold it represents.</span></p><p class=\"c1\"><span class=\"c0\"></span></p><p class=\"c2\"><span class=\"c3 c0\">Recalculated &amp; Original parameters </span><span class=\"c0\">- shown on the right side of the application including TORA, TODA, ASDA, LDA.</span></p><p class=\"c1\"><span class=\"c0\"></span></p><p class=\"c2\"><span class=\"c3 c0\">Add &amp; Remove obstacle</span></p><p class=\"c2\"><span class=\"c0\">Remove obstacle button is disabled if there is no obstacle on the runway.</span></p><p class=\"c1\"><span class=\"c3 c0\"></span></p><p class=\"c2\"><span class=\"c3 c0\">Breakdown of Calculations - </span><span class=\"c0\">can be accessed by clicking button in the bottom of the screen.</span></p><p class=\"c1\"><span class=\"c0\"></span></p><p class=\"c1\"><span class=\"c0\"></span></p></body></html>";
    static String default_s = "<html><head><title>Start screen</title></head><body class=\"c3\"><p class=\"c0\"><span>When the application first loads, you have 3 options.</span></p><p class=\"c0 c2\"><span></span></p><p class=\"c0\"><span class=\"c1\">Create airport</span></p><p class=\"c0\"><span>This allows you to create an airport from scratch. The purpose of creating an airport is purely to group runways together. It provides no additional functionality.</span></p><p class=\"c0 c2\"><span></span></p><p class=\"c0\"><span class=\"c1\">Load airport</span></p><p class=\"c0\"><span>Loads an airport from a previously created file. This is useful when moving between systems a lot and need to take the data with you.</span></p><p class=\"c0 c2\"><span></span></p><p class=\"c0\"><span class=\"c1\">Settings</span></p><p class=\"c0\"><span>From here, you can add/remove sets of predefined data that the system can use as well as change colour schemes to suit your needs.</span></p><p class=\"c0 c2\"><span></span></p><p class=\"c0\"><span>If you have added predefined runways (such as ones used from a previous session) these will appear in a drop down menu at the top of the screen for you to choose from.</span></p></body></html>";

    private void testXML() throws IOException {
        Map<String, String> data = new HashMap<String, String>();
        data.put("user_name", "chipsan");
        data.put("password", "my_pass");
        URL siteUrl = new URL("http://e.alarmsyst-bg.com/seg/get_airports");
        HttpURLConnection conn = (HttpURLConnection) siteUrl.openConnection();
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);

        DataOutputStream out = new DataOutputStream(conn.getOutputStream());
        Set keys = data.keySet();
        Iterator keyIter = keys.iterator();
        String content = "";
        for (int i = 0; keyIter.hasNext(); i++) {
            Object key = keyIter.next();
            if (i != 0) {
                content += "&";
            }
            content += key + "=" + URLEncoder.encode(data.get(key), "UTF-8");
        }
        System.out.println(content);
        out.writeBytes(content);
        out.flush();
        out.close();
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line = "";
        StringBuffer result = new StringBuffer();
        while ((line = in.readLine()) != null) {
            System.out.println(line);
            result.append(line);
        }
        OnlineCollectionAirport online = (OnlineCollectionAirport) xstream.fromXML(result.toString());
        System.out.println("==========================================");
        System.out.println(online.getAirports().get(0).getName());
        in.close();

    }

    public synchronized static PredefinedManual getInstance() {
        if (instance == null) {
            instance = new PredefinedManual();
            return instance;
        }
        return instance;
    }

    public static void showManual(String s) {

        JFrame frame = new JFrame();
        frame.setMinimumSize(new Dimension(400, 500));
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 1));
        frame.setContentPane(panel);
        JEditorPane area;
        area = new JEditorPane("text/html", s) {
            @Override
            public boolean getScrollableTracksViewportWidth() {
                return true;
            }
        };
        area.setEditable(false);
        JScrollPane pane = new JScrollPane(area);
        panel.add(pane);

        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

    }

//    public Manual getManual() {
//        return MainModel
//    }
    public Manual initialise() { // this method would try to get the html file, if it doesnt succeed it would use a default xml manual
        Manual result = new Manual();
        if (result.tryXML) { //we failed to retrieve from the repo, hence try to read from XML
            Manual temp;
            temp = attemptReadDefault();
            if (temp == null) { // if we can't read the xml
                result.setAirportView(default_a);
                result.setIntroduction(default_i);
                result.setMainView(default_m);
                result.setStartScreen(default_s);
                this.wrtieManual(result);
                System.out.println("A new XML file with the manual was creat����");
            } else { //we succeeded to read the xml ->return it
                result = temp;
                System.out.println("XML file with the manual was loaded");
            }
        } else {
            System.out.println("HTML successfully downloaded from the repo");
        }
        return result;

    }

    private Manual attemptReadDefault() {
        Manual loaded;
        try {
            loaded = (Manual) xstream.fromXML(default_manual_directory + "/manual.xml");
        } catch (Exception e) {
            loaded = null;
        }
        return loaded;
    }

    private Manual readFromFile(File f) {
        Manual loaded;
        try {
            loaded = (Manual) xstream.fromXML(f);
        } catch (Exception e) {
            loaded = null;
        }
        return loaded;
    }

    private boolean wrtieManual(Manual m) {
        FileOutputStream fos = null;
        try {
            File dir = new File(default_manual_directory);
            if (!dir.isDirectory()) {
                dir.mkdirs();
            }
            File write_to = new File(default_manual_directory + "/manual.xml");
            fos = new FileOutputStream(write_to, false);

            PrintWriter out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(fos, "UTF-8")));
            xstream.toXML(m, fos);
            out.flush();
            out.close();
            return true;
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Log.addMessage(ex.getLocalizedMessage());
            return false;
        } finally {
            // quietly close the FileOutputStream (see Jakarta Commons IOUtils)
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException ex) {
                    Log.addMessage(ex.getLocalizedMessage());
                }
            }
        }
    }
}
