/*
 * The following code is written in a collaboration between
 * Georgi Tenev <gtt1g12@soton.ac.uk>
 * Toby Finch <tlf1g12@soton.ac.uk>
 * Peter Fox <tf3g12@soton.ac.uk>
 * Denny Demchenko <dd3g12@soton.ac.uk>
 */
package Model;

import java.util.ArrayList;

public class OnlineCollectionAirport {

    ArrayList<Airport> airports;

    public OnlineCollectionAirport() {
        airports = new ArrayList<>();
    }

    public void setAirports(ArrayList<Airport> a) {
        airports = a;
    }

    public void addAirport(Airport a) {
        this.airports.add(a);
    }

    public ArrayList<Airport> getAirports() {
        return this.airports;
    }
}
